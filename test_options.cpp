#include "test.h"
#include "options.h"
#include "util.h"
#include "global_config.h"
#include "config.h"
#include "tim_options.h"

using namespace std;

bool tryParse(string failMsg, int argc, const char **argv, options &opt ) { 
  bool ret = true;
  try {
    opt.parse( argc, argv );
  } catch (options::parseException &ex) {
    cerr << failMsg << ex.what() << endl;
    ret = false;
  }
  return ret;
}

bool testDefault() { 
  bool ret = true;
  options opt;
  const char *argv[] = { "" };
  int argc = 1;
  opt.addOption("numINT", "i", option::number,"",false,"123");
  
  ret = tryParse("testDefault(): Failed parsing: ", argc, argv, opt);
  
  TESTCOND( opt.parsedOption("numINT") && FP_EQ(opt.getNum("numINT"),123), "testDefault(): Failed to find option or wrong value" );

  return ret;
}


bool testNum() {
  bool ret = true;
  options opt;
  const char *argv[] = { "", "--numINT", "120", "--numNEG", "-34", "--numFLOAT", "0.231" };
  int argc = 7;
  
  opt.addOption("numINT","i",option::number,"");
  opt.addOption("numNEG","n",option::number,"");
  opt.addOption("numFLOAT","f",option::number,"");
  
  ret = tryParse("testNum(): Failed parsing: ", argc, argv, opt);
  
  if ( ! opt.parsedOption("numINT") || ! opt.parsedOption("numNEG") || ! opt.parsedOption("numFLOAT") ) { 
    cerr << "testNum(): Failed to find all options" << endl;
    ret = false;
  } else if ( ! FP_EQ(opt.getNum("numINT"), 120) || ! FP_EQ(opt.getNum("numNEG"), -34) || ! FP_EQ(opt.getNum("numFLOAT"),0.231,0.00000001) ) { 
    cerr << "testNum(): Failed to parse options correctly (numINT="  <<opt.getNum("numINT")<<
                                                          ", numNEG="  <<opt.getNum("numNEG")<<
                                                          ", numFLOAT="<<opt.getNum("numFLOAT")<<")" << endl;
    ret = false;
  }
  return ret;
}

bool testStr() {
  bool ret = true;
  options opt;
  const char *argv[] = { "", "--str", "string"};
  int argc = 3;
  
  opt.addOption("str","s",option::string, "");

  ret = tryParse("testStr(): Failed parsing: ", argc, argv, opt);
  
  if ( ! opt.parsedOption("str") ) { 
    cerr << "testStr(): Failed to find all options" << endl;
    ret = false;
  } else if ( opt.getStr("str") != "string" ) { 
    cerr << "testStr(): Failed to parse options correctly (str="  <<opt.getStr("str")<< endl;
    ret = false;
  }
  return ret;
}

bool testBool() {
  bool ret = true;
  options opt;
  const char *argv[] = { "", "--bool" };
  int argc = 2;
  
  opt.addOption("bool","b",option::boolean,"");

  ret = tryParse("testBool(): Failed parsing: ", argc, argv, opt);
  
  if ( ! opt.parsedOption("bool") ) { 
    cerr << "testBool(): Failed to find all options" << endl;
    ret = false;
  } else if ( opt.getBool("bool") != true ) { 
    cerr << "testBool(): Failed to parse options correctly "<< endl;
    ret = false;
  }
  return ret;
}

bool testShort() { 
  bool ret = true;
  options opt;
  const char *argvA[] = { "", "--test", "ahoj", "-test", "cau" };
  int argcA = 5;
  
  opt.addOption("test","t",option::string,"");
  opt.addOption("test2","test",option::string,"");
  
  
  ret = tryParse("testShort(): Failed parsing: ", argcA, argvA, opt);
  
  if ( ! opt.parsedOption("test") || ! opt.parsedOption("test2") ) {
    cerr << "testShort(): Failed to find all options" << endl;
    ret = false;
  } else if ( opt.getStr("test") != "ahoj" || opt.getStr("test2") != "cau" ) { 
    cerr << "testShort(): Failed to parse options correctly (test="<<opt.getStr("test")<<", test2="<<opt.getStr("test2")<<")" << endl;
    ret = false;
  }
  return ret;
}

bool testIgnoreNonOptions() { 
  bool ret = true;
  options opt;
  const char *argv[] = { "", "init" };
  int argc = 2;
  
  opt.addOption("init","i",option::number,"",false);
  
  ret = tryParse("testIgnoreNonOptions(): Failed parsing: ", argc, argv, opt);
  
  if ( ret && opt.parsedOption("init") ) { 
    cerr << "testIgnoreNonOptions(): Parsed option init which is not present.";
    ret = false;
  }
  return ret;
  
}
  

bool testRequired() { 
  bool ret = true;
  options opt, optb;
  const char *argvA[] = { "", "--testb", "--tests", "string", "--testn", "182" };
  const char *argvB[] = { "", "--tests", "string"};
  int argcA = 6;
  int argcB = 3;
  opt.addOption("testb","tb",option::boolean, "", true);
  opt.addOption("tests","ts",option::string, "", true);
  opt.addOption("testn","ts",option::number, "", true);
  
  optb.addOption("testb","tb",option::boolean, "", true);
  optb.addOption("tests","ts",option::string, "", true);
  optb.addOption("testn","ts",option::number, "", true);
  
  
  ret = tryParse("testRequired(): Failed parsing in step 1: ", argcA, argvA, opt);
  if ( ret && ( ! opt.parsedOption("testb") || ! opt.parsedOption("tests") || ! opt.parsedOption("testn") ) ) {
    cerr << "testRequired(): Failed in step 1 (required options not parsed)" << endl;
    ret = false;
  }
  
  try { 
    optb.parse( argcB, argvB );
    cerr << "testRequired(): Failed in step 2 (no exception but missing required option)" << endl;
    ret = false;
  } catch ( options::parseException &ex ) {
    if ( ex.err != options::parseException::missing_required_option ) {
      cerr << "testRequired(): Failed in step 2 (wrong exception: "<< ex.what() <<")" << endl;
      ret = false;
    }
  }
  
  return ret;
}

bool testSaveToConfig() { 
  bool ret = true;
  tim_options optb;
  
  const char *argvb[] = {"tim.exe","-p",PHASE_COMPUTE,"-d", "data\\uk-init-my_run", "-o", "data\\uk-results-my_run", "-i", "1000000000", "-e", "1000", "--down", "-f", "uk_dump_" };  
  int argcb = 14;
  
  
  TESTCOND( optb.parse( argcb, argvb ), "testSaveToConfig(): Failed parsing options." );
  if ( ret ) {
    TESTCOND( optb.command == PHASE_COMPUTE, "testSaveToConfig(): Failed saving phase." );
    TESTCOND( global_config.getStr(DATA_FILE_OPTION) == "data\\uk-init-my_run", "testSaveToConfig(): Failed saving data." );
    TESTCOND( FP_EQ(global_config.getNum(DUMP_INTERVAL_OPTION), 1000000000), "testSaveToConfig(): Failed saving dump-interval.");
    TESTCOND( FP_EQ(global_config.getNum(EPSILON_OPTION), 1000), "testSaveToConfig(): Failed saving epsilon.");
    TESTCOND( global_config.getBool(ALLOW_DOWN_OPTION), "testSaveToConfig(): Failed saving down.");
    TESTCOND( global_config.getStr(DUMP_FILE_PREFIX_OPTION) == "uk_dump_","testSaveToConfig(): Failed saving dump-file-base.");
  }
  
  return ret;
}
  
bool testRunBatOptions() {
  bool ret = true;
  tim_options opta, optb;
  const char *argva[] = {"tim.exe","-p","init","-d", "data\\uk-measured-net", "-z", "data\\uk-zakazy", "-o", "data\\uk-init-my_run" };
  int argca = 9;
  
  TESTCOND( opta.parse( argca, argva ), "testRunBatOptions()[ init phase ]: Failed parsing." );
  if ( ret ) { 
    TESTCOND( opta.command == PHASE_INIT, "testRunBatOptions() [ init phase ]: Failed parsing phase." );
    TESTCOND( global_config.getStr(DATA_FILE_OPTION) == "data\\uk-measured-net", "testRunBatOptions() [ init phase ]: Failed parsing data." );
    TESTCOND( global_config.getStr(ZAKAZY_FILE_OPTION) == "data\\uk-zakazy", "testRunBatOptions() [ init phase ]: Failed parsing zakazy." );
    TESTCOND( global_config.getStr(OUTPUT_FILE_OPTION) == "data\\uk-init-my_run", "testRunBatOptions() [ init phase ]: Failed parsing output." );
  }
  
  const char *argvb[] = {"tim.exe","-p","compute","-d", "data\\uk-init-my_run", "-o", "data\\uk-results-my_run", "-i", "1000000000", "-e", "1000", "--down", "-f", "uk_dump_" };
  int argcb = 14;
  
  TESTCOND( optb.parse( argcb, argvb ), "testRunBatOptions()[ compute phase ]: Failed parsing." );
  if ( ret ) { 
    TESTCOND( optb.command == PHASE_COMPUTE, "testRunBatOptions()[ compute phase ]: Failed parsing phase." );
    TESTCOND( global_config.getStr(DATA_FILE_OPTION) == "data\\uk-init-my_run", "testRunBatOptions()[ compute phase ]: Failed parsing data." );
    TESTCOND( global_config.getStr(OUTPUT_FILE_OPTION) == "data\\uk-results-my_run", "testRunBatOptions()[ compute phase ]: Failed parsing output." );
    TESTCOND( FP_EQ(global_config.getNum(DUMP_INTERVAL_OPTION),1000000000), "testRunBatOptions()[ compute phase ]: Failed parsing dump-interval." );
    TESTCOND( FP_EQ(global_config.getNum(EPSILON_OPTION),1000), "testRunBatOptions()[ compute phase ]: Failed parsing epsilon." );    
    TESTCOND( global_config.getBool(ALLOW_DOWN_OPTION), "testRunBatOptions()[ compute phase ]: Failed parsing down." );
    TESTCOND( global_config.getStr(DUMP_FILE_PREFIX_OPTION) == "uk_dump_", "testRunBatOptions()[ compute phase ]: Failed parsing output." );
  }
  
  return ret;
}


int main(int, char **) { 
  bool ret = true;
  TEST(testRequired)
  TEST(testShort)
  TEST(testNum)
  TEST(testBool)
  TEST(testStr)
  TEST(testDefault)
  TEST(testIgnoreNonOptions)
  TEST(testRunBatOptions)
  TEST(testSaveToConfig)
  if ( ! ret ) return 1;
  return 0;
}
