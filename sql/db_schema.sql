begin transaction;

set search_path to tim_template;
drop table if exists network;
drop table if exists restrictions;
drop table if exists aadf;

create table network (
  link_id int8,
  nodeid_from int4,
  nodeid_to int4,
  func_class int4,
  dir_of_travel varchar(1),
  residential_traffic int default 0,
/*  access_restriction_allowed varchar(1) default 'Y',
  residential bool default false, */
  include_in_model bool default true,
  treshold int4 default 0/*,
  version int default 0 */
);

comment on table network IS 'A table with the network topology';
comment on column network.dir_of_travel IS 'The direction of the link: T: nodeid_from=>nodeid_to; F: nodeid_to=>nodeid_from; B: both directions';
comment on column network.link_id IS 'Unique positive id of the link';
comment on column network.func_class IS 'A number >= 1 and <=5, where 1 are most important, 5 least important links';
comment on column network.residential_traffic IS 'The cumulated amount of residential traffic along the link';
comment on column network.include_in_model IS 'Whether to include in the computation (i.e. the road is not residential and is not closed to traffic)';
comment on column network.treshold IS 'Guesstimate of the amount of traffic (including residential traffic, ...)';
/*comment on column tim.network.access_restriction_allowed IS 'N --- closed to traffic, Y --- open for traffic';
comment on column tim.network.residential IS 'False --- through link or a residential link entering the network with residential traffic, True --- a residential link not to be included in the computation';*/
/*comment on column tim.network.version IS 'The version of the data'; */


create table restrictions (
  linkid_from int8,
  linkid_to int8
);
comment on table restrictions IS 'A table describing turn restrictions. Must include a row of the form link_id, link_id, for each link! (to disable U-turns)';


create table aadf (
  link_id int8,
  intensity int/*,
  version int default 0 */
);

comment on table aadf IS 'The table holding the aadf_counts';
/*comment on column tim.aadf_sets.version IS 'The version of the aadf data'; */



end transaction;