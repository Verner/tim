-- Negative values (bad)
select count(distinct(link_id)) as Edges from tim.var_results;
select count(*) as Negative_Edges from tim.var_results where computed_f < 0 OR computed_b < 0;

-- Surplus
select count(distinct(link_id)) as Nodes from tim.var_results;
select count(distinct(link_id)) as Surplus_Nodes from tim.var_results where surplus_src_f > 0 OR surplus_src_b > 0;

-- Assymetry
select count(*) AS Rel_25 from tim.var_results where rel_f * rel_b > 0.25;
select count(*) AS Asym9 from tim.asymmetry where fraction >= 0.9 and rel_f * rel_b > 0.25;
select count(*) AS Asym7 from tim.asymmetry where fraction >= 0.7 and rel_f * rel_b > 0.25;
select count(*) AS Asym5 from tim.asymmetry where fraction >= 0.5 and rel_f * rel_b > 0.25;
select count(*) AS Asym3 from tim.asymmetry where fraction >= 0.5 and rel_f * rel_b > 0.25;
select count(*) AS Asym2 from tim.asymmetry where fraction >= 0.5 and rel_f * rel_b > 0.25;


-- Fclass5 
select count(*) AS FC5 from tim.var_results where fclass = 5;
select count(*) AS FC5_1000 from tim.var_results where computed_f > 1000 and fclass = 5;
select count(*) AS FC5_2000 from tim.var_results where computed_f > 2000 and fclass = 5;
select count(*) AS FC5_5000 from tim.var_results where computed_f > 5000 and fclass = 5;
select count(*) AS FC5_10000 from tim.var_results where computed_f > 10000 and fclass = 5;


-- Print negative vals
select * from tim.var_results where computed_f < 0 OR computed_b < 0
order by computed_f asc
limit 90;

-- Print surplus vals
select * from tim.var_results where surplus_src_f > 0
order by surplus_src_f desc
limit 90;


