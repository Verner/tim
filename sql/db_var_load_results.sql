drop table if exists tim.var_results cascade;
create table tim.var_results_2 (
  link_id int4,
  measured_f integer default null,
  measured_b integer default null,
  init_f integer default null,
  init_b integer default null,
  computed_f integer default 0,
  computed_b integer default null,
  rel_f float default 0,
  rel_b float default null,
  lbound_f integer default 0,
  lbound_b integer default null,
  surplus_src_f integer default 0,
  surplus_src_b integer default null,
  surplus_tgt_f integer default 0,
  surplus_tgt_b integer default null,
  fclass int2,
  str_info varchar(100) default '',
  primary key( link_id )
);


begin transaction;
create temporary table temp_results (
  link_id int4,
  measured_f integer default null,
  measured_b integer default null,
  init_f integer default null,
  init_b integer default null,
  computed_f integer default 0,
  computed_b integer default null,
  rel_f float default 0,
  rel_b float default null,
  lbound_f integer default 0,
  lbound_b integer default null,
  surplus_src_f integer default 0,
  surplus_src_b integer default null,
  surplus_tgt_f integer default 0,
  surplus_tgt_b integer default null,
  fclass int2,
  str_info varchar(100) default ''
);

delete from tim.var_results;
copy temp_results from 'h:/Data/verner/gcc/table-splus9M' USING DELIMITERS ' ' WITH NULL as '-1';
insert into tim.var_results
select distinct on (link_id) * from temp_results;
end transaction;

begin transaction;
drop index if exists tim.i_var_results_fclass;
--delete from tim.var_results_2;
--insert into tim.var_results_2
--select * from tim.var_results;
delete from tim.var_results;
copy tim.var_results from 'h:/Data/verner/gcc/new6-table-splus12M' USING DELIMITERS ' ' WITH NULL as '-1';
create index i_var_results_fclass on tim.var_results( fclass );
end transaction;

update tim.var_results set rel_f =rel_f*0.2 where computed_f < 0;
update tim.var_results set rel_f =rel_f*0.2 where computed_b < 0;
update tim.var_results set computed_b = init_b where computed_b < 0;


select * from tim.var_results as r where r.computed_b is not null and abs(r.computed_f-r.computed_b) > 1000 limit 100;

create index i_var_results_link_id on tim.var_results(link_id);

drop view if exists tim.var_results_map;
create view tim.var_results_map as
select COALESCE(r.computed_f + r.computed_b, r.computed_f) as total_flow, r.*, t.the_geom from tim.var_results as r,topo.navteq_streets_geom_en as t
where r.link_id = t.link_id;

drop view if exists tim.var_results_map_2;
create view tim.var_results_map_2 as
select COALESCE(r.computed_f + r.computed_b,r.computed_f) as total_flow, r.*, t.the_geom from tim.var_results_2 as r,topo.navteq_streets_geom_en as t
where r.link_id = t.link_id;

select r.computed_f+r.computed_b                      as total_flow, 
       abs(r.computed_b-r.computed_f)                 as delta, 
       CAST(abs(r.computed_b-r.computed_f) as float)/CAST(r.computed_f+r.computed_b as float) as fraction,
       fclass
from   tim.var_results as r
where  r.computed_b is not null and ( r.computed_f + r.computed_b > 0)
order by fraction desc, total_flow desc
limit 100;


create view tim.asymmetry as
select *, CAST(abs(r.computed_b-r.computed_f) as float)/CAST(r.computed_f+r.computed_b as float) as fraction
from   tim.var_results as r
where  r.computed_b is not null and ( r.computed_f + r.computed_b > 50) and CAST(abs(r.computed_b-r.computed_f) as float)/CAST(r.computed_f+r.computed_b as float) > 0.3;


drop view if exists tim.asymmetry_map;
create view tim.asymmetry_map as 
select r.computed_f + r.computed_b as total_flow, r.*, t.the_geom from tim.asymmetry as r,topo.navteq_streets_geom_en as t
where r.link_id = t.link_id;

select count(*) from tim.asymmetry_map;


select * from tim.asymmetry as r
order by rel_f*rel_b desc, fraction desc, r.computed_f + r.computed_b desc


select r.measured_f, r.computed_f, r.surplus_tgt_f, r.computed_f/r.surplus_tgt_f as ratio from tim.var_results as r where r.surplus_tgt_f > 100 order by r.surplus_tgt_f desc limit 100;

select count(*) from tim.var_results where computed_b is not null;
select count(*) from tim.measurements where directed = 'B';

select count(distinct(link_id)) from tim.var_results;
select count(distinct(link_id)) from tim.results_import where link_id > 0;