set search_path to tim;
select distinct(version) from aadf_sets;

set search_path to tim;
select avg(version) from aadf_sets;

set search_path to tim;
select n.func_class, avg(a.intensity), stddev(a.intensity), min(a.intensity), max(a.intensity) from mod_aadf2008 as a,  mod_uk_links as n where a.link_id = n.link_id group by n.func_class order by n.func_class;


set search_path to tim;
create temp view aadf as select n.link_id, n.func_class, a.intensity from mod_aadf2008 as a, mod_uk_links as n where n.link_id = a.link_id;

select func_class, count(link_id) from aadf group by func_class order by func_class;

select avg(n2.intensity/n1.intensity), stddev(n2.intensity/n1.intensity) from aadf as n1, aadf as n2 where n1.func_class = 2 and n2.func_class=4;


drop view one_to_two;
create or replace temp view one_to_two as select n2.intensity/n1.intensity as decay from aadf as n1, aadf as n2 where n1.func_class = 1 and n2.func_class=2;
create or replace temp view one_to_three as select n2.intensity/n1.intensity as decay from aadf as n1, aadf as n2 where n1.func_class = 1 and n2.func_class=3;
create or replace temp view one_to_four as select n2.intensity/n1.intensity as decay from aadf as n1, aadf as n2 where n1.func_class = 1 and n2.func_class=4;
create or replace temp view one_to_five as select n2.intensity/n1.intensity as decay from aadf as n1, aadf as n2 where n1.func_class = 1 and n2.func_class=5;
create or replace temp view two_to_three as select n2.intensity/n1.intensity as decay from aadf as n1, aadf as n2 where n1.func_class = 2 and n2.func_class=3;
create or replace temp view two_to_four as select n2.intensity/n1.intensity as decay from aadf as n1, aadf as n2 where n1.func_class = 2 and n2.func_class=4;
create or replace temp view two_to_five as select n2.intensity/n1.intensity as decay from aadf as n1, aadf as n2 where n1.func_class = 2 and n2.func_class=5;
create or replace temp view three_to_four as select n2.intensity/n1.intensity as decay from aadf as n1, aadf as n2 where n1.func_class = 3 and n2.func_class=4;
create or replace temp view three_to_five as select n2.intensity/n1.intensity as decay from aadf as n1, aadf as n2 where n1.func_class = 3 and n2.func_class=5;
create or replace temp view four_to_five as select n2.intensity/n1.intensity as decay from aadf as n1, aadf as n2 where n1.func_class = 4 and n2.func_class=5;


create or replace temp view onetwolim as select decay from one_to_two order by decay desc limit 10000000;
create or replace temp view twothreelim as select decay from one_to_two order by decay desc limit 18000000;
select (decay) from twothreelim;

select * from topo. where link_id < 0;

set search_path to tim;
select distinct(year) from aadf;