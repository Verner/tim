
insert into tim.network ( link_id, nodeid_from, nodeid_to, func_class, dir_of_travel, access_restriction_allowed )
select link_id, nodeidfrom, nodeidto, func_class, dir_of_travel, ar_auto from routing.connections_en
where link_id > 0 and func_class > 0 and func_class < 6;

update tim.network set residential_traffic = r.cumulative_sumcars from tim.links_en as r where r.cumulative_sumcars >= r.sumcarsi and network.link_id = r.link_id;
update tim.network set residential_traffic = r.sumcarsi from tim.links_en as r where r.cumulative_sumcars < r.sumcarsi and network.link_id = r.link_id;
update tim.network set residential = true from tim.links_en as r where r.link_id = network.link_id and r.deadend > 0 and ( r.deadendrecipient > 0 OR network.residential_traffic = 0);

insert into tim.aadf_sets
select link_id, intensity from tim.mod_aadf2008;

copy tim.restrictions from 'h:/Data/verner/data/uk-zakazy-old' using delimiters ' ';
