/*CREATE TABLE tim.results_import (
	id INTEGER DEFAULT 0,
	link_id INT8,
	fClass INT2,
	typ INT2 DEFAULT 0, /* 0 ... REGULAR, 
                               1 ... SOURCE, 
                               2 ... SINK,
                               3 ... ARTIFICIAL
                               4 ... INVALID
                               5 ... ESTIMATED (i.e. deadendorder > 0 and deadendrecipient = 0) */
/*	multiplicity INTEGER,
	fromID INTEGER,
	toID INTEGER,
	lbound INTEGER DEFAULT NULL,
	ubound INTEGER DEFAULT NULL,
        measured INTEGER DEFAULT NULL,
	init INTEGER DEFAULT NULL,
	computed INTEGER DEFAULT NULL,
	reliability FLOAT DEFAULT NULL,
	fclassBACK INT2 DEFAULT NULL
);*/



/*CREATE TABLE tim.results_map (
	link_id INT4,
        id_list VARCHAR(100) DEFAULT NULL,
        computed_list VARCHAR(100) DEFAULT NULL,
        init_list VARCHAR(100) DEFAULT NULL,
        rel_list VARCHAR(100) DEFAULT NULL,
	fClass INT2,
	lbound INTEGER DEFAULT NULL,
	ubound INTEGER DEFAULT NULL,
        measured INTEGER DEFAULT NULL,
	init INTEGER DEFAULT NULL,
	computed INTEGER DEFAULT NULL,
	reliability FLOAT DEFAULT NULL,
        the_geom geometry,
        PRIMARY KEY( link_id )
);*/

--alter table tim.results_map alter column link_id type int4;
--alter table tim.results_map drop column fClassBACK;

/*
CREATE OR REPLACE FUNCTION catenate(text,text) RETURNS text AS '
      SELECT COALESCE($1 || $2,$1,$2,NULL)
   ' LANGUAGE SQL;

CREATE AGGREGATE concatenate (
      sfunc = catenate,
      basetype = text,
      stype = text,
      initcond = ''
   );*/

--create index i_results_import_link_id on tim.results_import(link_id);

begin transaction;
drop index if exists tim.i_results_import_link_id;
drop index if exists i_results_import_typ on tim.results_import( typ );
delete from tim.results_import;
copy tim.results_import from 'c:/Data/verner/gcc/var-uk-partial-4' USING DELIMITERS ' ' WITH NULL as '-1';
create index i_results_import_link_id on tim.results_import( link_id );
create index i_results_import_typ on tim.results_import( typ );
end transaction;

begin transaction;

create table tim.temp_direction_from (
  link_id int4,
  id int8,
  measured integer default null,
  init integer default null,
  computed integer default null,
  reliability float default 0,
  PRIMARY KEY (link_id)
);

create table tim.temp_direction_to (
  link_id int4,
  id int8,
  measured integer default null,
  init integer default null,
  computed integer default null,
  reliability float default 0,
  PRIMARY KEY (link_id)
);


delete from tim.temp_direction_to;
delete from tim.temp_direction_from;

insert into tim.temp_direction_from
select link_id, min(id) from tim.results_import
where typ = 0 OR typ = 1
group by link_id;

insert into tim.temp_direction_to
select link_id, max(id) from tim.results_import
where typ = 1 OR typ = 1
group by link_id;

create index i_temp_direction_from_id on tim.temp_direction_from( id );
create index i_temp_direction_to_id on tim.temp_direction_to( id );

/*drop table tim.results;
CREATE TABLE tim.results (
	link_id INT4,
        id_from int4 default 0,
        id_to int4 default NULL,
        computed_from integer default 0,
        computed_to integer default null,
        init_from integer default 0,
        init_to integer default null,
        measured_from integer default null,
        measured_to integer default null,
        rel_from float default 0,
        rel_to float default null,
	fClass INT2,
	lbound INTEGER DEFAULT NULL,
	ubound INTEGER DEFAULT NULL,
        PRIMARY KEY( link_id )
);*/

insert into tim.results (link_id, id_from, computed_from, init_from, measured_from, rel_from, fclass, lbound, ubound)
select distinct(i.link_id), i.id, r.computed, r.init, r.measured, r.reliability, r.fclass, r.lbound, r.ubound
from temp_direction_from as i join tim.results_import as r
on i.id=r.id;

create table temp_vals_to (
  link_id int4 default null,
  computed integer default null,
  init integer default null,
  measured integer default null,

update tim.results set computed_to = r.computed, init_to = r.init, measured_to = r.measured, rel_to = r.reliability
from 

 
insert into tim.results (link_id, id_to, computed_to, init_to, measured_to, rel_from, fclass, lbound, ubound)
select distinct(i.link_id), i.id, r.computed, r.init, r.measured, r.reliability, r.fclass, r.lbound, r.ubound
from temp_direction_from as i join tim.results_import as r
on i.id=r.id;

end transaction;





begin transaction;

drop index if exists tim.i_results_map_the_geom;
drop index if exists tim.i_results_map_link_id;
drop index if exists i_results_map_computed on tim.results_map( computed );
delete from tim.results_map;


insert into tim.results_map
select r.link_id,
       concatenate( r.id || ',' ) AS id_list,
       concatenate( r.computed || ',' ) AS computed_list,
       concatenate( r.init || ',' ) AS init_list,
       concatenate( r.reliability || ',' ) AS rel_list,
       max(r.fClass ) AS fClass,
       max(r.lbound) AS lbound,
       min(r.ubound) AS ubound,
       avg(r.measured) AS measured,
       avg(r.init) AS init,
       round( sum(r.computed) / max(r.multiplicity) ) AS computed,
       avg(r.reliability) AS reliability,
       g.the_geom
FROM tim.results_import as r, topo.navteq_streets_geom_en as g
WHERE g.link_id = r.link_id
GROUP BY r.link_id, g.the_geom;

create index i_results_map_link_id on tim.results_map( link_id );
create index i_results_map_computed on tim.results_map( computed );
create index i_results_map_the_geom on tim.results_map using gist( the_geom );

end transaction;

