begin transaction;
	drop index if exists tim.i_var_results_fclass;
	--delete from tim.var_results_2;
	--insert into tim.var_results_2
	--select * from tim.var_results;
	delete from tim.var_results;
	copy tim.var_results from 'h:/Data/verner/my_summary_table' USING DELIMITERS ' ' WITH NULL as '-1';
	create index i_var_results_fclass on tim.var_results( fclass );
end transaction;
