drop table if exists tim.classified_results_II;
create table tim.classified_results_II (
	link_id int4,
	intensity_class int2,
	rel_class int2,
	primary key( link_id )
);

create temporary table res (
	link_id int4,
	class int2,
	rel float,
	primary key(link_id)
);

insert into res select link_id, 1, COALESCE((rel_f+rel_b)/2,rel_f) from tim.var_results_map where total_flow >= 0 AND total_flow < 50;
insert into res select link_id, 2, COALESCE((rel_f+rel_b)/2,rel_f) from tim.var_results_map where total_flow >= 50 AND total_flow < 150;
insert into res select link_id, 3, COALESCE((rel_f+rel_b)/2,rel_f) from tim.var_results_map where total_flow >= 150 AND total_flow < 300;
insert into res select link_id, 4, COALESCE((rel_f+rel_b)/2,rel_f) from tim.var_results_map where total_flow >= 300 AND total_flow < 500;
insert into res select link_id, 5, COALESCE((rel_f+rel_b)/2,rel_f) from tim.var_results_map where total_flow >= 500 AND total_flow < 1000;
insert into res select link_id, 6, COALESCE((rel_f+rel_b)/2,rel_f) from tim.var_results_map where total_flow >= 1000 AND total_flow < 1150;
insert into res select link_id, 7, COALESCE((rel_f+rel_b)/2,rel_f) from tim.var_results_map where total_flow >= 1150 AND total_flow < 1300;
insert into res select link_id, 8, COALESCE((rel_f+rel_b)/2,rel_f) from tim.var_results_map where total_flow >= 1300 AND total_flow < 1500;
insert into res select link_id, 9, COALESCE((rel_f+rel_b)/2,rel_f) from tim.var_results_map where total_flow >= 1500 AND total_flow < 1800;
insert into res select link_id, 10, COALESCE((rel_f+rel_b)/2,rel_f) from tim.var_results_map where total_flow >= 1800 AND total_flow < 2150;
insert into res select link_id, 11, COALESCE((rel_f+rel_b)/2,rel_f) from tim.var_results_map where total_flow >= 2150 AND total_flow < 2850;
insert into res select link_id, 12, COALESCE((rel_f+rel_b)/2,rel_f) from tim.var_results_map where total_flow >= 2850 AND total_flow < 4100;
insert into res select link_id, 13, COALESCE((rel_f+rel_b)/2,rel_f) from tim.var_results_map where total_flow >= 4100 AND total_flow < 5900;
insert into res select link_id, 14, COALESCE((rel_f+rel_b)/2,rel_f) from tim.var_results_map where total_flow >= 5900 AND total_flow < 7950;
insert into res select link_id, 15, COALESCE((rel_f+rel_b)/2,rel_f) from tim.var_results_map where total_flow >= 7950 AND total_flow <= 10000;
insert into res select link_id, 16, COALESCE((rel_f+rel_b)/2,rel_f) from tim.var_results_map where total_flow > 10000 AND total_flow < 11850;
insert into res select link_id, 17, COALESCE((rel_f+rel_b)/2,rel_f) from tim.var_results_map where total_flow >= 11850 AND total_flow < 13900;
insert into res select link_id, 18, COALESCE((rel_f+rel_b)/2,rel_f) from tim.var_results_map where total_flow >= 13900 AND total_flow < 16550;
insert into res select link_id, 19, COALESCE((rel_f+rel_b)/2,rel_f) from tim.var_results_map where total_flow >= 16550 AND total_flow < 20450;
insert into res select link_id, 20, COALESCE((rel_f+rel_b)/2,rel_f) from tim.var_results_map where total_flow >= 20450 AND total_flow < 30000;
insert into res select link_id, 21, COALESCE((rel_f+rel_b)/2,rel_f) from tim.var_results_map where total_flow >= 30000 AND total_flow < 36650;
insert into res select link_id, 22, COALESCE((rel_f+rel_b)/2,rel_f) from tim.var_results_map where total_flow >= 36650 AND total_flow < 49300;
insert into res select link_id, 23, COALESCE((rel_f+rel_b)/2,rel_f) from tim.var_results_map where total_flow >= 49300 AND total_flow < 120000;
insert into res select link_id, 24, COALESCE((rel_f+rel_b)/2,rel_f) from tim.var_results_map where total_flow >= 120000;


create index i_res_rel on res( rel );

insert into tim.classified_results_II select link_id, class, 0 from res where rel < 0.002;
insert into tim.classified_results_II select link_id, class, 1 from res where rel >= 0.002 AND rel < 0.01;
insert into tim.classified_results_II select link_id, class, 2 from res where rel = 0.01;
insert into tim.classified_results_II select link_id, class, 3 from res where rel > 0.01 AND rel < 0.641842;
insert into tim.classified_results_II select link_id, class, 4 from res where rel >= 0.641842 AND rel < 0.849869;
insert into tim.classified_results_II select link_id, class, 5 from res where rel >= 0.84987 AND rel < 0.912026;
insert into tim.classified_results_II select link_id, class, 6 from res where rel >= 0.912026 AND rel < 0.947892;
insert into tim.classified_results_II select link_id, class, 7 from res where rel >= 0.947892 AND rel < 0.963395;
insert into tim.classified_results_II select link_id, class, 8 from res where rel >= 0.963395 AND rel < 1;
insert into tim.classified_results_II select link_id, class, 9 from res where rel = 1;

