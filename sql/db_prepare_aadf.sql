--- fill the aadf table in schema __tim__ from the original aadf table __AADFSOURCE__
begin transaction;
set search_path to __tim__;
delete from aadf;
insert into aadf;
SELECT link_ida, avg(all_mv) FROM __AADFSOURCE__ WHERE link_idb = 0
GROUP BY link_ida
UNION  
SELECT link_ida, avg(all_mv/2) FROM __AADFSOURCE__ WHERE link_idb > 0
GROUP BY link_ida
UNION
SELECT link_idb, avg(all_mv/2) FROM __AADFSOURCE__ WHERE link_idb > 0
GROUP BY link_idb;

-- delete duplicite values
delete from aadf
where link_id in ( select link_id from __AADFSOURCE__ group by link_id having count(*) > 1 );

-- delete fclass 5 with large intensities
delete from aadf
where link_id in ( select m.link_id from aadf as m, network as n where m.link_id = n.link_id and n.func_class = 5 and m.intensity > 5000 );
end transaction;


-- create function 
create or replace function tim_template.prepareAADF(sourceTable TEXT, targetTable TEXT) returns void AS $$
declare
  query TEXT;
begin

query := 'delete from ' || targetTable || ';';
execute query;

query := 'insert into ' || targetTable || ' ' || 
'SELECT link_ida, avg(all_mv) FROM ' || sourceTable || ' WHERE link_idb = 0
GROUP BY link_ida
UNION  
SELECT link_ida, avg(all_mv/2) FROM ' || sourceTable || ' WHERE link_idb > 0
GROUP BY link_ida
UNION
SELECT link_idb, avg(all_mv/2) FROM ' || sourceTable || ' WHERE link_idb > 0
GROUP BY link_idb;';
execute query;

-- delete duplicite values
query := 'delete from ' || targetTable || ' 
where link_id in ( select link_id from ' || sourceTable || ' group by link_id having count(*) > 1 );';
execute query;

-- delete fclass 5 with large intensities
query := 'delete from ' || targetTable || '
where link_id in ( select m.link_id from ' || targetTable || ' as m, network as n where m.link_id = n.link_id and n.func_class = 5 and m.intensity > 5000 );';
end
$$ language plpgsql;





-- select count(*) from tim.mod_aadf2008 as m, routing.connections_en as r where m.link_id = r.link_id and r.func_class = 5 and m.intensity > 800;
-- 
-- 
-- select link_ida, link_idb, link_dist_meters, all_mv, link_name, namematch 
-- from tim.aadf2008
-- where link_ida in ( select link_id from tim.mod_aadf2008 group by link_id having count(*) > 1 )
-- order by link_ida;
-- 
-- select link_id from tim.mod_aadf2008
-- where link_id in ( select link_id from tim.mod_aadf2008 group by link_id having count(*) > 1 );
-- 
-- select count(*) from tim.mod_aadf2008
-- where link_id in ( select link_id from tim.mod_aadf2008 group by link_id having count(*) > 1 );

-- delete from tim.mod_aadf2008
-- where link_id in ( select link_id from tim.mod_aadf2008 group by link_id having count(*) > 1 );

-- select count(*) from tim.mod_aadf2008;