#include "zakazy.h"
#include "dataSet.h"

#include <fstream>

using namespace std;

void zakazy::load( const string &fName, dataSet *DS ) { 
  dS = DS;
  ifstream in( fName.c_str() );
  edge from, to;
  while( ! in.eof() ) {
    in >> from >> to;
    if ( in.eof() ) break;
    rules[from].insert(to);
  }
}

bool zakazy::isAllowed( edge From, edge To ) { 
  edge fCat = (*dS)[From].cat,
       tCat = (*dS)[To].cat;
  if ( rules.find( fCat ) == rules.end() ) return true;
  return ( rules[fCat].find(tCat) == rules[fCat].end() );
}
