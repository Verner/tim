/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include "graphscene.h"

#include "graph.h"

#include <QtGui/QGraphicsItem>
#include <QtGui/QPainter>
#include <QtGui/QToolTip>
#include <QtGui/QLabel>

#include <fstream>

class gItem : public QGraphicsItem {
  protected:
    QColor col;
    QString text;
    QLabel label;
    int width;

  protected:
    gItem(): col(Qt::black), width(10) {};
    gItem( QColor c, real w, std::string txt ):
            col(c), width((int)w), text(txt.c_str()) {};

  public:
    void setColor( QColor c ) {
      col = c;
      update();
    }
    void setText( std::string txt ) {
      prepareGeometryChange();
      text = QString(txt.c_str());
      label.setText( text );
      setToolTip( text );
    }
    void setText( QString txt ) {
      text = txt;
      prepareGeometryChange();
      label.setText( text );
      setToolTip( text );
    }
    void setWidth( real w ) {
      prepareGeometryChange();
      width = (int) w+5;
    }
};

class vertexItem : public gItem {
  public:
    vertexItem( real x, real y ): gItem( Qt::red, 10, "" ) {
      setPos( QPointF( x, y ) );
    }
    
    virtual QRectF boundingRect() const { 
      return QRectF(-width/2-10,-width/2-10,width+10,width+10 ).united(label.geometry().translated(20,20));
    }
    virtual void paint( QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget ) {
      painter->setPen( col );
      painter->setBrush( QBrush( col ) );
      painter->drawEllipse(-width/2,-width/2,width,width);
      label.render( painter, QPoint(20,20) );
    }
};


class edgeItem : public gItem {
  private:
    QPointF From, To;

    void getArrowCoords(qreal &x1, qreal &y1, qreal &x2, qreal &y2 ) const {
        //qreal slope = To.y()/To.x();
        qreal delta=10;
        if ( To.x() > 0 ) {
            x1 = To.x()-delta;
            x2 = To.x()-delta;
            y1 = To.y()-delta/3;
            y2 = To.y()+delta/3;
        } else if ( To.x() == 0 ) {
            x1 = To.x() + delta/3;
            x2 = To.x() + delta/3;
                  if (To.y() > 0) {
                     y1 = To.y() -delta;
                     y2 = To.y() -delta;
                 } else {
                     y1 = To.y() + delta;
                     y2 = To.y() + delta;
                 }
        } else { x1 = To.x()+delta;
                 x2 = To.x()+delta;
                 y1 = To.y()-delta/3;
                 y2 = To.y()+delta/3;

        }
    }

    QRectF getArrowRect() const {
        qreal x1,y1,x2,y2;
        getArrowCoords(x1,y1,x2,y2);
        return QRectF(QPointF(x1,y1),To).united(QRectF(QPointF(x2,y2),To));
    }
    void drawArrow(QPainter *p) const {
        qreal x1,y1,x2,y2;
        getArrowCoords(x1,y1,x2,y2);
        QPen pen( col );
        pen.setWidth( 3 );
        p->setPen( pen );
        p->drawLine(QPointF(x1,y1),To);
        p->drawLine(QPointF(x2,y2),To);
    }



  public:
    edgeItem( vertexItem *from, vertexItem *to ): gItem( Qt::blue, 1, "" ), From( from->pos() ), To( to->pos() ) {
      setPos(From);
      To -= From;
    }

    virtual QRectF boundingRect() const { 
      return (QRectF( QPointF(-width-10,-width-10), To+QPointF(width+10,width+10) ).united( label.geometry() )).united(getArrowRect());
    }

    virtual void paint( QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget ) {
      QPen pen( col );
      pen.setWidth( width );
      painter->setPen( pen );
      painter->drawLine( QPointF(0,0), To );
      drawArrow(painter);
      //painter->drawLine(To-QPointF(10,3), To );
      //painter->drawLine(To-QPointF(10,-3), To );
      //label.render( painter, QPoint(30,30) );
    }
};

void graphScene::setEdgeColor(edge E, QColor col) {
  if ( E < edges.size() ) edges[E]->setColor(col);
}

void graphScene::setEdgeDiam(edge E, real diam) {
  if ( E < edges.size() ) edges[E]->setWidth(diam);
}

void graphScene::setEdgeText(edge E, std::string text) {
  if ( E < edges.size() ) edges[E]->setText(text);
}

void graphScene::setEdgeText(edge E, QString text) {
  if ( E < edges.size() ) edges[E]->setText(text);
}

void graphScene::setVertexColor(node V, QColor col) {
  if ( V < vertices.size() ) vertices[V]->setColor(col);
}

void graphScene::setVertexDiam(node V, real diam) {
  if ( V < vertices.size() ) vertices[V]->setWidth(diam);
}

void graphScene::setVertexText(node V, std::string text) {
  if ( V < vertices.size() ) vertices[V]->setText(text);
}

void graphScene::setVertexText(node V, QString text) {
  if ( V < vertices.size() ) vertices[V]->setText(text);
}

void graphScene::addGraph(edgeTable* g, std::string path) {
  graph = g;
  vertices.resize( g->numOfNodes() );
  edges.resize( g->numOfEdges() );
  edge numOfEdgesShown=0;
  std::ifstream coordsFile( path.c_str() );
  node vtex;
  real x,y;
  vertexItem *vItem;
  edgeItem *eItem;
  
  while ( ! coordsFile.eof() ) { 
    coordsFile >> vtex >> x >> y;
    if ( coordsFile.eof() ) break;
    vItem =  new vertexItem( x, y );
    vertices[vtex] = vItem;
    addItem(vItem);
  }
  for(edge i = graph->numOfEdges()-1; i>=0; --i ) {
    node From = graph->fromNode(i), To = graph->toNode(i);
    if ( From < vertices.size() && To < vertices.size() ) {
      eItem = new edgeItem( vertices[graph->fromNode(i)], vertices[graph->toNode(i)] );
      edges[i]=eItem;
      addItem(eItem);
      numOfEdgesShown++;
    }
  }
  edges.resize( numOfEdgesShown );
}







