#!/bin/bash
# computation_diff.sh -- created 2009-08-18, Jonathan Verner
# @Last Change: 24-Dec-2008.
# @Revision:    0.0

aID=$1;
bID=$2;

SQL="
select v.fclass,
       round(avg(abs(a.intensity-b.intensity))) as avg_diff, 
       round(stddev(abs(a.intensity-b.intensity))) as dev_diff, 
       round(max(abs(a.intensity-b.intensity))) as max_diff,
       round(avg(abs(a.intensity-b.intensity)*a.reliability*b.reliability)) as avg_rel_diff,
       round(stddev(abs(a.intensity-b.intensity)*a.reliability*b.reliability)) as dev_rel_diff,
       round(max(abs(a.intensity-b.intensity)*a.reliability*b.reliability)) as max_rel_diff,
       round(sum(abs(a.intensity-b.intensity))) as sum_diff,
       round(sum(abs(a.intensity-b.intensity)*a.reliability*b.reliability)) as sum_rel_diff
from computation_data as a, computation_data as b, vzorek as v
where a.cat = b.cat and (a.intensity > 0 OR b.intensity > 0) and
      a.run_id = $1 and b.run_id = $2 and
      v.cat=a.cat
group by v.fclass
"

echo $SQL | psql mge;



# vi: 
