/* PARAMETERS
    #RUN_LENGTH
    #COMMAND
    #CODE_VERSION
    #MGRAPH_ID
    #MEASUREMENT_SET
    #CONNECTION_COND
    #COMMENTS
*/

begin transaction;
create temporary table init (
  nodeid integer,
  intensity float,
  reliability float
);
copy init from '/tmp/init' USING DELIMITERS ' ';

INSERT INTO computation_runs ( run_length, command_line, code_version, measurement_set, data_subset_CONDITION, run_type, comments )
VALUES( INTERVAL '#RUN_LENGTH', '#COMMAND', '#CODE_VERSION', #MEASUREMENT_SET, '#CONNECTION_COND', 'init', '#COMMENTS' );

insert into edge_run_data
select nodeid,
       intensity,
       reliability,
       currval('computation_seq'),
       #MGRAPH_ID
from init;

delete from map_layer_cur_init_data;
insert into map_layer_cur_init_data
SELECT v.gid,
       v.cat,
       v.fclass,
       v.directed,
       m.measured AS measured,
       avg( i.reliability ) as avgReliability,
       concatenate( i.reliability || ', ' ) AS partReliability,
       round(sum ( i.intensity )/ max(g.multiplicity)) AS intensity,
       concatenate(g.id || ', ') AS edgeIDs,
       concatenate(g.toid || ', ' ) AS toNodeIDs,
       concatenate(g.fromID || ', ' ) AS fromNodeIDs
  FROM vzorek v,
       init i,
       mgraph_edges g,
       measurements as m
 WHERE(  ( g.id = i.nodeid ) AND 
         ( g.cat = v.cat )   AND 
         ( m.cat = g.cat )   AND 
         ( m.measurement_set = #MEASUREMENT_SET) AND
         ( g.mgraph_id = #MGRAPH_ID ) )
 GROUP BY v.the_geom,
        v.gid,
        v.cat,
        v.fclass,
        v.directed,
        v.measured_i,
	m.measured;

end transaction;

vacuum analyze init;




