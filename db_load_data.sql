DROP VIEW IF EXISTS measured_data CASCADE;
DROP TABLE IF EXISTS connections CASCADE;
DROP TABLE IF EXISTS import_graph CASCADE;
DROP TABLE IF EXISTS aadf CASCADE;

CREATE TEMPORARY TABLE import_graph (
	cat INT8 DEFAULT 0,
	fClass INT2,
	directed CHAR(1),
	fromCAT INT8,
	toCAT INT8
);

COPY import_graph FROM '/home/joni/zdroj/mge/data/connections.txt';


CREATE TABLE connections (
	cat INT8,
	fromCAT INT8,
	toCAT INT8,
	directed CHAR(1),
        orig_direction CHAR(1),
	fClass INT2
);

drop index if exists i_connections_cat;
create index i_connections_cat on connections ( cat );

INSERT INTO connections
SELECT cat, fromCAT, toCAT, directed, directed, fClass
FROM import_graph WHERE directed= 'F' OR directed='B';

INSERT INTO connections
SELECT cat, toCAT, fromCAT, 'F', 'T', fClass
FROM import_graph WHERE directed= 'T';


DROP TABLE import_graph;

DROP TABLE IF EXISTS aadf;
CREATE TABLE aadf (
	sid INTEGER,
	Rname VARCHAR(255),
	LACode VARCHAR(10),
	LName VARCHAR(255),
	CP INTEGER,
	Road VARCHAR(10),
	RdSeq INTEGER,
	Street VARCHAR(255),
	RCatName VARCHAR(500),
	LenNet FLOAT,
	dOpened VARCHAR(8),
	dClosed VARCHAR(8),
	SRefE INTEGER,
	SRefN INTEGER,
	Year INTEGER,
	PC INTEGER,
	WMV2 INTEGER,
	CAR INTEGER,
	BUS INTEGER,
	LGV INTEGER,
	HGVR2 INTEGER,
	HGVR3 INTEGER,
	HGVR4 INTEGER,
	HGVA3 INTEGER,
	HGVA5 INTEGER,
	HGVA6 INTEGER,
	HGV INTEGER,
	All_MV INTEGER,
	link_id INT8,
	link_dist_meters INTEGER,
	navteq_ver VARCHAR(20),
	xwgs84 FLOAT,
	ywgs84 FLOAT,
	link_name VARCHAR(255),
	name_match INTEGER
);

drop index if exists i_aadf_link_id_navteq_ver;
create index i_aadf_link_id_navteq_ver on aadf ( link_id, navteq_ver );
drop index if exists i_aadf_link_id;
create index i_aadf_link_id on aadf ( link_id );

drop index if exists i_vzorek_cat;
create index i_vzorek_cat on vzorek ( cat );


COPY aadf FROM '/home/joni/zdroj/mge/data/aadf-data.txt';

cluster i_aadf_link_id_navteq_ver on aadf;
vacuum analyze aadf;


CREATE VIEW measured_data AS
SELECT l.cat AS cat, l.fClass AS fClass, l.directed AS directed, l.fromCAT AS fromCAT, l.toCAT AS toCAT, m.All_MV AS measured
FROM connections AS l 
LEFT OUTER JOIN (
	SELECT * from aadf WHERE navteq_ver='2008Q4'
) AS m ON l.cat = m.link_id;


