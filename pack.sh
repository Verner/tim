#!/bin/bash
# pack.sh -- created 2009-05-25, Jonathan Verner
# @Last Change: 24-Dec-2008.
# @Revision:    0.0

git-ls-tree -r HEAD | sed -e's/[^ ]* [^ ]* \S*//g' | sed -e's/win\/.*//g' | xargs tar -cv | bzip2 > mge.tar.bz2

# vi: 
