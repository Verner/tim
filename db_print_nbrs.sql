SELECT sink.id as id, nxt.id as nbid
FROM graph_nodes as sink, 
     graph_edges as e_tonbr, 
     graph_edges as e_tosink,
     graph_nodes as nxt,
     graph_nodes as prv
WHERE 
        ( sink.ntype='sink' OR sink.ntype='snkF' OR sink.ntype='snkT' )
      AND 
        ( e_tonbr.cat = sink.edge_cat AND e_tosink.cat = sink.edge_cat)
      AND 
        ( e_tosink.toid = sink.id AND e_tonbr.toid = nxt.id )
      AND 
        ( prv.id = e_tosink.fromid )
      AND 
        nxt.ntype='regular' AND prv.ntype = 'regular'
      AND 
        NOT e_tonbr.toid = prv.id
UNION ALL 
SELECT src.id as id , prv.id as nbid
FROM graph_nodes as src,
     graph_edges as e_fromprv,
     graph_edges as e_tonxt,
     graph_nodes as prv,
     graph_nodes as nxt
WHERE
         ( src.ntype='source' OR src.ntype='srcF' OR src.ntype = 'srcT' )
     AND
	 ( e_tonxt.cat = src.edge_cat AND e_fromprv.cat = src.edge_cat )
     AND
         ( e_tonxt.fromid = src.id AND e_fromprv.fromid = prv.id )
     AND 
         ( nxt.id = e_tonxt.toid )
     AND
         ( nxt.ntype = 'regular' AND prv.ntype = 'regular' )
     AND
         NOT e_fromprv.fromid = nxt.id
ORDER BY
        id;