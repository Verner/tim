INPUT SPECIFICATION:
====================


Input data for the model shall be given in the following three tables:
(see sql/db_schema.sql)

1) network 		--- the network topology & residential traffic
2) aadf		--- aadf counts
3) restrictions	--- traffic restrictions

The table network shall have the following columns

link_id			... a unique positive id of the link (negative values allowed, but such links will be ignored)
nodeid_from		... the unique id of the from junction
nodeid_to		... the unique id of the to junction
func_class		... an integer >= 1 and <= 5 (other values allowed, but such links will be ignored)
dir_of_travel		... either B, F, T, where B means the link is a both-way link
		    		T means the traffic goes in the direction from_junction ==> to_junction
                    		F means the traffic goes in the direction to_junction ==> from_junction
residential_traffic	... either NULL, or the minimal amount of traffic that should be traveling along
			    	this link (for both-way links, this amount of traffic is ensured in both directions)
include_in_model	... either FALSE (do not include) or TRUE (include). A link with value FALSE will be completely ignored
				in the computation. The reason for having FALSE can either be that the road is closed to
				the relevant traffic (ar_auto == N) or that the road is a residential road 
				(dead_end > 0 AND dead_end_recipient != 0) or some other reason. 
				[IMPLEMENTATION NOTE: This column may be computed from acccess_restrictions_allowed and 
				 residential by, e.g. the following sql: 
				    UPDATE network SET include_in_model = ( access_restriction_allowed = 'Y' AND residential)]


treshold		... a guesstimate of expected traffic on this road (if available, NULL otherwise)

The table aadf shall have the following columns

link_id			... the id of the link
intensity		... the aadf count connected to this link or NULL if no such count is available.
				If the link is a both-way link, this number shall be interpreted as the
				sum of traffic in both directions and will be divided among the two directions.

Note that this table is required to contain all links from the tim.network table. Also note that this
table should be checked not to contain blatantly invalid aadf counts (i.e. unusually high aadf counts for
fclass 5/4 links or unusually low aadf counts for fclass 1/2 links).

The table restrictions shall have the following columns

linkid_from
linkid_to

where each row in the table corresponds to a traffic restrictions disallowing traffic to pass from
the link with id linkid_from to the link with id linkid_to. Note that this table must contain
a row of the form linkid,linkid for each link in the tim.network table to disable U-turns.

[IMPLEMENTATION NOTE: This last condition may be met, e.g. by executing the following sql:

      INSERT linkid,linkid INTO tim.restrictions FROM SELECT DISTINCT linkid FROM tim.network;
]

NOTE: All of the tables will live in a schema corresponding to the TIM version;
The schema will have the following format:

timcountrycode_netYEAR_adfYEAR

e.g. 

timuk_net2008_adf2012

will be a schema containing the network data in UK from 2008 and aadf counts
from 2012.




OUTPUT SPECIFICATION:
=====================

The output of the computation will be stored in a table chosen by the user which will live in the tim namespace.
If a table of the chosen name exists, it will first be dropped. The table shall have the following columns

link_id			... the id of the link
measured_f		... an aadf count for the FROM direction or NULL if not available
measured_b		... an aadf count for the TO direction or NULL if not available
init_f			... the initialized traffic intensity for the FROM direction or NULL if not available
init_b			... the initialized traffic intensity for the TO direction or NULL if not available
computed_f		... the computed traffic intensity for the FROM direction
computed_b		... the computed traffic intensity for the TO direction or NULL if the link is a one-way link
rel_f			... the reliability of the computed_f value (a floating point number between 0 and 1, with 1 being highest)
rel_b			... the reliability of the computed_b value (a floating point number between 0 and 1, with 1 being highest) OR NULL if not available
lbound_f		... the lower bound (e.g. given by residential traffic) for the FROM direction or NULL if not available
lbound_b		... the lower bound (e.g. given by residential traffic) for the TO direction or NULL if not available
surplus_src_f		... the surplus at the origin junction of the FROM direction
surplus_src_b		... the surplus at the origin junction of the TO direction or NULL if not available
surplus_tgt_f		... the surplus at the destination junction of the FROM direction
surplus_tgt_b		... the surplus at the destination junction of the TO direction or NULL if not available
fclass			... the function class of the link (a number >=1 and <= 5)
str_info		... a string containing internal info about the computation at the given link

Note that the _b columns will be not null only for both-way links in which case they will contain info about the backward direction
(i.e. the direction to_node ==> from_node). For one-way links, only the _f values will be relevant regardeless of the dir_of_travel!!!


EXECUTING THE PROCESS
=====================

The program files will reside in the h:\Data\verner\gcc directory. Three batch files will be provided in the h:\Data\verner directory.
The h:\Data\verner\SQL directory will contain auxiliary sql batches and the h:\Data\verner\data directory will contain data exported
from the database (which will be safe to remove once the computation is finished). The h:\Data\verner directory will also contain
some temporary files during the computation.

1) run.bat 

This batch file will export data from the database and start the computation. The progress of the computation
shall be logged in the file h:\Data\verner\progress_report.

2) get_partial_results.bat

This batch file will ask the user to give a name of a table and then will import into this table the partially computed results of
the computation (if the computation did not yet finish). If a table of the name exists, it will first be dropped.

3) get_results.bat

Once the computation finished, this batch file may be run to load the results into a database table of the users choice.
The table will live in the tim namespace and the batch file will ask the user for its name. If a table of the name exists,
it will first be dropped.


If desired a database procedure to classify the computed flow into bands may be provided once the bands are specified.
