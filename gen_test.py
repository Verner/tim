# 1-2 { f:1 }
# 1=3 { f:1 }
# 1=2x3-4

def load( fname ):
    f = open( fname, 'r' );
    edges  = {};
    zakazy = [];
    for line in f:
        if '{' in line:
            link_name, data = line.split('{')
            link_name = link_name.strip()
            data = data.strip().strip('}').strip().split(',');
            link_data = {}
            for d in data:
                name,value=d.strip().split(':')
                try:
                    link_data[name]=float(value)
                except:
                    link_data[name]=value
            edges[link_name]=link_data;
        elif 'x' in line:
            lfrom,lto = line.split('x')
            lfrom = lfrom.strip()
            lto = lto.strip()
            zakazy.append([lfrom,lto])
    return edges, zakazy

def save( edges, zakazy, fname = '' ):
    if fname == '':
        f = None
    else:
        f = open( fname, 'w' )
    for (name,data) in edges.items():
        data_str = ', '.join(map(lambda (k,v): str(k)+':'+str(v), data.items()))         
        print >> f, name, '{', data_str, '}'
    for z in zakazy:
        print >> f, ' x '.join(z)

    if f != None:
        f.close()

def loadResult():
    return None

def getCat( name ):
    if '-' in name:
        fromN, toN = name.split('-')
    fromN, toN = fromN.strip(), toN.strip()
    return int(fromN + '0' + toN)


def prepareInit( fname, zakazyFname, edges, zakazy ):
    eID=0
    f = open( fname, 'w' )
    for (name,data) in edges.items():
        cat = getCat( name )
        directed = ('-' in name)
        if 'm' in data: # Need a source & sink
            if directed:
                print >> f, eID, cat, data['f'], 1, 2, fromN, toN, 0, 0, data['m'], 0, 0, 0, 0
                eID += 1
                print >> f, eID, cat, data['f'], 2, 2, fromN, toN, 0, 0, data['m'], 0, 0, 0, 0
            else:
                print >> f, eID, cat, data['f'], 1, 2, fromN, toN, 0, 0, data['m'], 0, 0, 0, 0
                eID += 1
                print >> f, eID, cat, data['f'], 2, 2, fromN, toN, 0, 0, data['m'], 0, 0, 0, 0
                eID += 1
                print >> f, eID, cat, data['f'], 1, 2, toN, fromN, 0, 0, data['m'], 0, 0, 0, 0
                eID += 1
                print >> f, eID, cat, data['f'], 2, 2, toN, fromN, 0, 0, data['m'], 0, 0, 0, 0
        else:
            if directed:
                print >> f, eID, cat, data['f'], 0, 1, fromN, toN, 0, 0, data['m'], 0, 0, 0, 0, 0
            else:
                print >> f, eID, cat, data['f'], 0, 1, fromN, toN, 0, 0, data['m'], 0, 0, 0, 0, 0
                eID += 1
                print >> f, eID, cat, data['f'], 0, 1, toN, fromN, 0, 0, data['m'], 0, 0, 0, 0, 0
        eID += 1
    f.close()
    f = open( zakazyFname, 'w' )
    for (fromL, toL) in zakazy.items():
        print >> f, getCat(fromL), getCat(toL)
    f.close()

