#ifndef DEFS_H
#define DEFS_H

#include <list>
#include <vector>

#include <iosfwd>
#include <stdio.h>
#include <assert.h>


#ifndef ND_ECS
#ifdef D_ALG
#define D_ECS
#endif
#endif

#ifdef UNIX
#include <sys/types.h>
typedef int32_t integer32;
#else
typedef __int32 integer32;
typedef unsigned int uint;
#endif




typedef double bigreal;
typedef double real;
typedef float smallreal;

#include "reliability.h"

typedef enum dirT { FORWARD = 0, OUTGOING=0, BACKWARD=1, INCOMING=1 } dirT;

inline dirT invert( dirT direction ) { 
  if ( direction == FORWARD ) return BACKWARD;
  else return FORWARD;
}

#ifndef D_EDGE
typedef integer32 node;
typedef integer32 edge;
typedef integer32 node_data_type;
typedef integer32 edge_data_type;

inline bool valid_node( const node N ) {
  return (N>=0);
}
inline bool valid_edge( const edge E ) {
  return (E>=0);
}


#else // D_EDGE

typedef integer32 node_data_type;
typedef integer32 edge_data_type;

class node {
  private:
    integer32 nID;
    
  public:
    
    node_data_type maxNode() const { return 268435455; }
    

  node(): nID(-1) {};
  node(integer32 N): nID(N){ assert( valid() );};
  
  bool valid() const { return (nID >= 0);};
  node &operator--() {nID--; return *this;};
  node &operator++() {assert( nID < maxNode() ); nID++; return *this;};
  node &operator++(int) {assert( nID < maxNode() ); nID++; return *this;};
  bool operator>=( const integer32 N ) const { return nID >= N; };
  bool operator<(const integer32 N ) const { return nID < N; };
  bool operator==(const node N) const { return (nID == N.nID); };
  friend std::ostream &operator<<( std::ostream &, const node );
  friend std::istream &operator>>( std::istream &, node &);
  operator integer32() const {
    assert( valid() );
    return nID;
  }
};

class edge {
  private:
    integer32 nID;
    
  public:
    
    edge_data_type maxEdge() const { return 268435455; }
    

  edge(): nID(-1) {};
  edge(integer32 N): nID(N){ assert( valid() );};
  
  bool valid() const { return (nID >= 0);};
  edge &operator--() {nID--; return *this;};
  edge &operator++() {assert( nID < maxEdge() ); nID++; return *this;};
  edge &operator++(int) {assert( nID < maxEdge() ); nID++; return *this;};
  bool operator>=( const integer32 N ) const { return nID >= N; };
  bool operator<(const integer32 N ) const { return nID < N; };
  bool operator==(const edge N) const { return (nID == N.nID); };
  friend std::ostream &operator<<( std::ostream &, const edge);
  friend std::istream &operator>>( std::istream &, edge &);
  operator integer32() const {
    assert( valid() );
    return nID;
  }
};

inline bool valid_node( const node N ) {
  return N.valid();
}
inline bool valid_edge( const edge E ) {
  return E.valid();
}

#endif // D_EDGE

class edgeList : public std::vector<edge> {
  public:
    
    /* Appends the contents of lst to edgeList and
     * empties lst */
    void append( edgeList &lst );
};

//typedef std::vector<edge> edgeList;
typedef std::list<edge> eList;

#endif // DEFS_H
  
