#include "tim_options.h"
#include <iostream>

#include "global_config.h"


using namespace std;

tim_options::tim_options():
  options(),
  command(""), data_file(""), update_file(""), zakazy_file(""), dump_prefix(""), output_file(""), checkpoint(""), control_file(""), cfg_file(""),
  epsilon(100), 
  dumpInterval(-1), zerofactor(100000), 
  allowDown(false)
{
  addOption("help","h",option::boolean,"produce a help message");
  addOption("config","cfg",option::string,"configuration file",false,"default_configuration");
  addOption("phase","p",option::string,"one of "PHASE_INIT", "PHASE_COMPUTE", "PHASE_SUMMARY", "PHASE_ID2CAT,true);
  addOption("update","u",option::string,"update computation saved in the given file");
  addOption("checkpoint","c",option::string,"restart from a checkpoint found in file",false);
  
  addOption(DATA_FILE_OPTION,"d",option::string,"input file");
  addOption(OUTPUT_FILE_OPTION,"o",option::string,"output file");
  addOption(ZAKAZY_FILE_OPTION,"z",option::string,"a file containing zakazy odboceni (each zakaz on a line, in the form fromID, toID)");
  addOption(EPSILON_OPTION,"e",option::number,"epsilon",false);
  addOption(AVOID_ZERO_OPTION,"az",option::number,"avoid zero factor",false);
  addOption(DUMP_INTERVAL_OPTION,"i",option::number,"dump intermediate results after arg iterations",false);
  addOption(DUMP_FILE_PREFIX_OPTION,"f",option::string,"base filename for the dumps of intermediate results",false);
  addOption(ALLOW_DOWN_OPTION,"down",option::boolean,"allow down operations during the compute phase",false);
  addOption(CONTROL_FILE_OPTION,"control",option::string,"control file used to control the running computation",false);
  
}

bool tim_options::parse(int argc, const char *argv[] ) {
  try {
    options::parse( argc, argv);
  } catch( options::parseException &ex ) { 
    cout << ex.what() << endl;
    cout << endl;
    cout << helpString();
    return false;
  }

  /* UNKNOWN COMMAND GIVEN --> parsing failure */
  command = getStr("phase");
  if ( command != PHASE_INIT && command != PHASE_COMPUTE && command != PHASE_SUMMARY && command != PHASE_ID2CAT ) return false;
  
  if (parsedOption("checkpoint")) checkpoint = getStr("checkpoint");
  if (parsedOption("update")) update_file = getStr("update");


  if (parsedOption(ALLOW_DOWN_OPTION) && getBool(ALLOW_DOWN_OPTION)) allowDown = true;
  if (parsedOption(DATA_FILE_OPTION)) data_file = getStr(DATA_FILE_OPTION);
  if (parsedOption(ZAKAZY_FILE_OPTION)) zakazy_file = getStr(ZAKAZY_FILE_OPTION);
  if (parsedOption(OUTPUT_FILE_OPTION)) output_file = getStr(OUTPUT_FILE_OPTION);
  if (parsedOption(DUMP_FILE_PREFIX_OPTION)) dump_prefix = getStr(DUMP_FILE_PREFIX_OPTION);

  
  if (parsedOption("config")) {
    cfg_file = getStr("config");
    global_config.load( cfg_file );
  }
  saveToConfig( &global_config );
  
   /* Have default values for these */
  dumpInterval = (int) global_config.getNum(DUMP_INTERVAL_OPTION);
  epsilon = (int) global_config.getNum(EPSILON_OPTION);
  zerofactor = (int) global_config.getNum(AVOID_ZERO_OPTION);
  control_file = global_config.getStr(CONTROL_FILE_OPTION);
  
  return true;
}