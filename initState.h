#ifndef _initState_H
#define _initState_H

#include "defs.h"
#include "flow.h"
#include "graph.h"

#include <string>
#include <iosfwd>


#ifdef VERSION_1
  #define RELIABILITY_TRESHOLD 0.01
  #define CROSS_NETWORK 0
#else
  #define RELIABILITY_TRESHOLD 0.05
  #define CROSS_NETWORK 2
#endif

class sourceSinkList;
class dataSet;

class initState {
  private:
    graph *G;
    edgeList sideEdges;
    int minFlow; // The minimal flow, below which we don't bother.
    real relDecay; // The reliability decay when passing an intersection 
    
#ifndef VERSION_1
    /* The guesstimated flow on edges. 
     *  - Used only in places of low reliability
     *  - edges E for which it is unavailable have
     *    guesstimate_flow[E] == 0; 
     *  - guesstimate_flow is always >= 0
     */
    std::vector<real> guesstimate_flow; 
#endif

    relFlowVector flVector;

#ifdef VERSION_1    
    static real decayFClass(real flow, unsigned short fromClass, unsigned short toClass) {
      return (flow * EXP<real>( 0.3, toClass-fromClass ))/2;
    }
#else
    real decayMatrix[5];
    real decayFClass(real flow, unsigned short fromClass, unsigned short toClass) const;
#endif
    
    /* Returns the (decreased) reliability when splitting
     * flow between edges in @splitEdges. @rel is the original
     * reliability */
    reliability decayReliability( reliability rel, const edgeList &splitEdges, node N, dirT direction ) const { 
      if ( G->nbrEdges(N, direction ).size() < 2 ) return rel;
      real sz = splitEdges.size();
      return rel*MIN<real>((relDecay+(((real)1)/(sz*sz*sz*sz))),1);
    }

    /* Returns a list of proportions assigned to each of
     * the edges in @edges list */
    std::list<real> getProportions( const edgeList &edges ) const {
      std::list<real> ret;
      int sz = edges.size();
      for( edgeList::const_iterator it = edges.begin(); it != edges.end(); ++it )
	ret.push_back( ((real)1)/sz );
      return ret;
    }
    

    // edgeList needMinimumForward, needMinimumBackward;
    /* Add the edgeList @edges to the list of edges
     * where we should ensure a minimal amount of
     * traffic
     * NOTE: Will empty the @edges list.
     * NOTE: Currently not used !! */
    void needMinimalFlow( edgeList &edges, dirT direction ) {
      edges.clear();
      return;
      /*if ( direction == FORWARD ) needMinimumForward.append( edges );
      else needMinimumBackward.append(  edges );*/
    }
    
    /* Returns true if edge E needs ensuring minimal flow
     * in the direction */
    bool wantMinimalFlow( edge E, dirT direction, reliability relTreshold ) const;

    /* Recursively distributes flow @flow along edge @E in the direction @direction,
     * with reliability @rel. The recursion will have depth at most @depth */
    void recursive_distribute_flow( edge source, std::ofstream &outSource, edge E, dirT direction, real flow, reliability rel, int depth, int cross_network = CROSS_NETWORK);

    /* Recursively ensure minimal flow @lBound along edge @E in the direction @direction,
     * not updating edges with reliability > relTrehshold and not recursing below @dept */
    void recursive_ensure_minimum_flow( edge E, dirT direction, real lBound, reliability relTreshold, int depth );

    /* Calculate a lower bound on the flow along edge @E
     * in direction @direction based on the flow in the
     * neibourghood. There are two cases:

     *  A) If the edge is the most important class
     *     road going through the source node, then
     *	   the lower bound is the average of traffic
     *	   on same class roads through the source
     *
     *  B) A certain proportion of the total amount of
     *     traffic going along the most important 
     *     roads through the source node
     *
     *  where the source node is either the to node 
     *  (in case direction == BACKWARD) or the from node
     *  (in case direction == FORWARD )
     */
    real calculateLBound( edge E, dirT direction ) const;

    void distributeFlow( const sourceSinkList *data, dirT direction, int depth = 100 );
    
    /* Returns the number of links where initialized flow significantly differs from the measured flow */
    long testMeasuredSS( const sourceSinkList* data, dirT direction, const dataSet &dSet ) const;

  public:
    initState( graph *G, int mFlow = 300, real relDec = 0.89 );

    void load( const dataSet &dSet );
    void save( dataSet& dSet ) const;

    void distributeSources( const sourceSinkList *data );
    void distributeSinks( const sourceSinkList *data );

    void ensureMinimalFlow( reliability relTreshold = RELIABILITY_TRESHOLD );
    void mergeState( const initState &s );
    
    
    /* Returns the number of source links where initialized flow significantly differs from the measured flow */
    long testInitSources( const sourceSinkList *src, const dataSet &dSet ) const {
      return testMeasuredSS( src, FORWARD, dSet );
    }
    
    /* Returns the number of sink links where initialized flow significantly differs from the measured flow */
    long testInitSinks( const sourceSinkList *snk, const dataSet &dSet ) const {
      return testMeasuredSS( snk, BACKWARD, dSet );
    }

    real getFlow( edge E ) const { return flVector.getFlow( E ); };
    reliability getReliability( edge E ) const { return flVector.getReliability( E ); };

};

#endif /* _initState_H */
