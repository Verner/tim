/***************************************************************
 * paint.cpp
 * @Author:      Jonathan Verner (jonathan.verner@matfyz.cz)
 * @License:     GPL v2.0 or later
 * @Created:     2009-05-29.
 * @Last Change: 2009-05-29.
 * @Revision:    0.0
 * Description:
 * Usage:
 * TODO:
 *CHANGES:
 ***************************************************************/

#include <QtGui/QImage>
#include <QtGui/QPixmap>
#include <QtGui/QPainter>
#include <QtCore/QString>
#include <QtGui/QApplication>
#include <QtCore/QDebug>

#include <fstream>
#include <iostream>
#include <string>
#include <math.h>

#include "defs.h"
#include "util.h"

#include <boost/program_options.hpp>

namespace po = boost::program_options;


long wd=0,ht=0,x=0,y=0,maxEID;

struct line { 
  long x1,x2,y1,y2;
  int fClass;
  long edge;
};

std::vector<line> lines;

void getSize( const char *coordsFile ) { 
  std::ifstream coordsIN( coordsFile );
  edge E;
  int fClass;
  long maxX, minX, maxY, minY, x1,y1,x2,y2,lns=0;
  struct line ln;
  coordsIN >> E >> ln.fClass >> ln.x1 >> ln.y1 >> ln.x2 >> ln.y2;
  ln.edge = E;
  maxEID=E;
  minX = MIN(ln.x1,ln.x2); maxX = MAX(ln.x1,ln.x2);
  minY = MIN(ln.y1,ln.y2); maxY = MAX(ln.y1,ln.y2);
  lines.resize(E+1);
  lines[E]=ln; 
  while( ! coordsIN.eof() ) { 
    coordsIN >> E >> ln.fClass >> ln.x1 >> ln.y1 >> ln.x2 >> ln.y2;
    if ( coordsIN.eof() ) break;
    lines.resize(E+1);
    lines[E]=ln;
    maxX = MAX( MAX( maxX, ln.x1 ), ln.x2 );
    maxY = MAX( MAX( maxY, ln.y1 ), ln.y2 );
    minX = MIN( MIN( minX, ln.x1 ), ln.x1 );
    minY = MIN( MIN( minY, ln.y1 ), ln.y1 );
    maxEID=MAX( E, maxEID );
  }
  coordsIN.close();
  wd = maxX-minX;
  ht = maxY-minY;
  x = minX;
  y = minY;
  std::cerr << wd << " x " << ht <<  "\n";

}



int main( int argc, char **argv ) {
  QApplication app(argc, argv);
  std::string coordsFile, imageFile;
  int zoomFactor=10;
  po::options_description desc("Allowed options");
  bool displayDirection;
  desc.add_options()
  ("help,h", "produce help message")
  ("display-direction,d", "indicate direction of edges")
  ("coordinates,c", po::value<std::string>(&coordsFile), "coordinates file")
  ("output,o", po::value<std::string>(&imageFile), "output file")
  ("zoom-factor,z", po::value<int>(&zoomFactor), "zoom factor")
  ;

  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);

  displayDirection = vm.count("display-direction");
  
  if (vm.count("help")) {
    std::cout << desc << "\n";
    return 1;
  }

  if ( ! vm.count("coordinates") || ! vm.count("output") ) { 
    std::cout << desc << "\n";
    return 1;
  }


  getSize( coordsFile.c_str() );
  QString outImg( imageFile.c_str() );
  int margin = 100;
  QImage img( wd/zoomFactor + margin, ht/zoomFactor+margin, QImage::Format_RGB32);
  QPainter paint;
  QPen pen( Qt::red );
  QColor col( 0, 20, 20 );
  edge H;
  real intensity;
  long ln=0;
  std::cerr << wd/zoomFactor << " x " << ht/zoomFactor << " at " << zoomFactor << "\n";
  if ( ! paint.begin(&img) ) {
    std::cerr<<"Unexpected error\n";
    exit(1);
  }
  if ( displayDirection ) {
    paint.scale((real)1/(real)zoomFactor, (real) 1/(real) zoomFactor);
    paint.translate(-x+margin*zoomFactor/2,-y+margin*zoomFactor/2);
  }
  paint.setFont( QFont("Times",100,QFont::Bold) );
  while( ! std::cin.eof() ) { 
    std::cin >> H >> intensity;
    if ( H > maxEID ) continue;
    if ( std::cin.eof() ) break;
    pen.setWidth( abs((6-lines[H].fClass)*2*10/zoomFactor) );
    real redIntensity;
    if ( intensity > 100000 ) redIntensity=255;
    else if ( intensity > 50000) redIntensity= (150 + ((real) 40/(real)50000)*(intensity-50000));
    else if ( intensity > 10000) redIntensity= (110 + ((real)50/(real)40000)*(intensity-10000));
    else if ( intensity > 1000) redIntensity= (60 + ((real)50/(real)9000)*(intensity-1000) );
    else if ( intensity > 0 ) redIntensity= (10+((real)50/(real)1000)*intensity );
    else redIntensity=0;
    col.setRed( redIntensity );
    //col.setRed( 255 );
    pen.setColor(col);
    paint.setPen( pen );
    if ( ! displayDirection ) { 
      paint.drawLine( (lines[H].x1-x)/zoomFactor+margin/2, (lines[H].y1-y)/zoomFactor+margin/2, (lines[H].x2-x)/zoomFactor+margin/2, (lines[H].y2-y)/zoomFactor+margin/2 );
//      paint.drawText((lines[H].x1-x)/zoomFactor+margin/2,(lines[H].y1-y)/zoomFactor+margin/2,QString::number((long)H,10));
//      paint.drawText((lines[H].x1-x)/zoomFactor+margin/2,(lines[H].y1-y)/zoomFactor+margin/2,QString::number((long)H,10));
    } else {
      
    if ( intensity > 1000 ) redIntensity=255;
    else if ( intensity > 500) redIntensity= (150 + ((real) 40/(real)500)*(intensity-500));
    else if ( intensity > 100) redIntensity= (110 + ((real)50/(real)400)*(intensity-100));
    else if ( intensity > 10) redIntensity= (60 + ((real)50/(real)90)*(intensity-10) );
    else if ( intensity > 0 ) redIntensity= (10+((real)50/(real)10)*intensity );
    else redIntensity=0;
    real wd = (6-lines[H].fClass)*10;
    //real shift = wd + 10*zoomFactor;
        col.setRed( redIntensity );
    pen.setWidth( (6-lines[H].fClass)*10 );
    pen.setColor(col);

    paint.setPen( pen );
      long ax = lines[H].x1, ay = lines[H].y1;
      long bx = lines[H].x2, by = lines[H].y2;
    //std::cerr << H <<" "<< lines[H].fClass <<" "<< ax <<" "<< ay <<" "<< bx <<" "<< by << "\n";

      real len = sqrt((ax-bx)*(ax-bx) + (ay-by)*(ay-by));
      real angle;
      if ( bx == ax && by > ay ) angle = 90;
      else if ( bx == ax && by < ay ) angle = 270;
      else if ( by == ay && ax > bx ) angle = 180;
      else if ( by == ay && ax < bx ) angle = 0;
      else {
	angle = atan2( (real) (by-ay), (real) (bx-ax) )* 57.32;
	//angle=45;
      }
      paint.save();
      paint.translate( ax, ay );
      paint.rotate(angle);
      paint.translate(0,150);
      paint.drawArc( 0, 0, len, -300, 0, 180*16 );
      pen.setColor(Qt::white);
      paint.setPen( pen );
      paint.translate(len/2,-300);
      paint.rotate(-angle);
      paint.drawText(0,0,QString::number((long)intensity,10));
      paint.restore();

      /*if ( (lines[H].x1 < lines[H].x2) || (lines[H].x1 == lines[H].x2 && lines[H].y1 < lines[H].y2 ) ) { 
        paint.drawLine( (lines[H].x1-x-shift)/zoomFactor+margin/2, (lines[H].y1-y-shift)/zoomFactor+margin/2, (lines[H].x2-x-shift)/zoomFactor+margin/2, (lines[H].y2-y-shift)/zoomFactor+margin/2 );
      } else {
	paint.drawLine( (lines[H].x1-x+shift)/zoomFactor+margin/2, (lines[H].y1-y+shift)/zoomFactor+margin/2, (lines[H].x2-x+shift)/zoomFactor+margin/2, (lines[H].y2-y+shift)/zoomFactor+margin/2 );
      }*/
    }
  }
  paint.end();
  img.save( outImg );
}
    
    
    

 
