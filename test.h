#ifndef __TEST_H__
#define __TEST_H__

#include <iostream>
#define TEST(x) if (x()) std::cout << #x << ": Ok." << std::endl; else ret = false;
#define TESTCOND( condition, errmsg ) if ( ! (condition) ) { std::cout << errmsg << std::endl; ret = false; }

#endif // __TEST_H__