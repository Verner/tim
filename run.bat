@echo off
set /p schemaName= Load data from schema:

del %schemaName%-progress_report
type sql\db_create_dataset.sql | w64-build\replace.exe __tim__ %schemaName% __PWD__ %CD%/data \ / | psql -U postgres mop 2> %schemaName%-progress_report
w64-build\tim.exe -p init -d data\%schemaName%-measured-net -z data\%schemaName%-zakazy -o data\%schemaName%-init 2>> %schemaName%-progress_report
w64-build\tim.exe -p compute -d data\%schemaName%-init -o data\%schemaName%-results --control_file %schemaName%-control -f %schemaName%_dump_ 2>> %schemaName%-progress_report
