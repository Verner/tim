#include "config.h"
#include "util.h"
#include <fstream>
#include <sstream>

using namespace std;

bool config::parseLine( const string line, string &key, string &val ) { 
  string normalized = "";
  bool to_upper = true;
  for(uint i=0; i < line.length(); i++ ) { 
    if ( line[i] == '#' || line[i] == '\n' ) break;
    if ( line[i] == ' ' || line[i] == '\t' ) continue;
    if ( line[i] == '=' ) to_upper = false;
    if ( to_upper) normalized += toUpper( line[i] );
    else normalized += line[i];
  }
  return split( normalized, key, val, '=' );
}

bool config::addConfigLine(string ln) {
  string key, val;
  if ( parseLine( ln, key, val ) ) { 
    keys[key] = val;
    return true;
  }
  return false;
}



bool config::haveKey(string key) {
  return (keys.find( toUpper(key) ) != keys.end());
}

real config::getNum(string key, real default_val) {
  real ret;
  if ( haveKey(key) && toNum( keys[toUpper(key)], ret ) ) return ret;
  else return default_val;
}

bool config::getBool(string key, bool default_val) {
  bool ret;
  if ( toBool( keys[toUpper(key)],  ret ) )
    return ret;
  else return default_val;
}

string config::getStr(string key, string default_val) {
  if ( haveKey(key) ) return keys[toUpper(key)];
  else return default_val;
}


void config::loadDefaults() {
  addConfigLine(DATA_FILE_OPTION"                   = ");
  addConfigLine(OUTPUT_FILE_OPTION"                 = ");
  addConfigLine(ZAKAZY_FILE_OPTION"                 = ");
  addConfigLine(CONTROL_FILE_OPTION"                = control");
  addConfigLine(DUMP_INTERVAL_OPTION"               = -1");
  addConfigLine(DUMP_FILE_PREFIX_OPTION"            = dump_");
  addConfigLine(AVOID_ZERO_OPTION"                  = 100000" );
  addConfigLine(EPSILON_OPTION"                     = 1000");
  addConfigLine(ALLOW_DOWN_OPTION"                  = true");
  addConfigLine(INIT_FLOW_DEPTH_OPTION"             = 100");
  addConfigLine(RELIABILITY_TRESHOLD_OPTION"        = 0.05");
  addConfigLine(CROSS_FCLASS_NETWORK_OPTION"        = 2");
  addConfigLine(FC1_DECAY_OPTION"                   = 1");
  addConfigLine(FC2_DECAY_OPTION"                   = 0.5");
  addConfigLine(FC3_DECAY_OPTION"                   = 0.4");
  addConfigLine(FC4_DECAY_OPTION"                   = 0.3");
  addConfigLine(FC5_DECAY_OPTION"                   = 0.1");
  addConfigLine(COST_FACTOR_FC1_OPTION"             = 0.01");
  addConfigLine(COST_FACTOR_FC2_OPTION"             = 0.04");
  addConfigLine(COST_FACTOR_FC3_OPTION"             = 0.09");
  addConfigLine(COST_FACTOR_FC4_OPTION"             = 0.16");
  addConfigLine(COST_FACTOR_FC5_OPTION"             = 0.25");
}

config::config() {
  loadDefaults();
}


void config::load(string fname) {
  ifstream IN( fname.c_str() );
  char buf[1000];
  string ln;
  string key, val;
  
  if ( IN.is_open() ) { 
    while( ! IN.eof() ) {
      IN.getline(buf,1000);
      ln = buf;
      if ( parseLine( ln, key, val ) ) { 
        keys[key] = val;
      }      
    }
  }
}

ostream& operator<<( ostream& OUT, const config& cfg ) {
  for( map<string,string>::const_iterator ln = cfg.keys.begin(); ln != cfg.keys.end(); ++ln )
    OUT << ln->first << " = " << ln->second <<endl;
  return OUT;
}

