begin transaction;
drop view if exists map_layer_init CASCADE;
delete from init;
copy init from '/tmp/init' USING DELIMITERS ' ';

drop index if exists i_init_id;
create index i_init_id on init ( nodeid );

create view map_layer_init as
SELECT v.gid,
       v.cat,
       v.fclass,
       v.directed,
       m.measured AS measured,
       avg( i.reliability ) as avgReliability,
       concatenate( i.reliability || ', ' ) AS partReliability,
       round(sum ( i.intensity )/ max(g.multiplicity)) AS intensity,
       concatenate(g.id || ', ') AS edgeIDs,
       concatenate(g.toid || ', ' ) AS toNodeIDs,
       concatenate(g.fromID || ', ' ) AS fromNodeIDs,
       v.the_geom
  FROM vzorek v,
       init i,
       graph_edges g,
       measured_data as m
 WHERE ( ( g.id = i.nodeid ) AND ( g.cat = v.cat ) AND ( m.cat = g.cat ) )
 GROUP BY v.the_geom,
        v.gid,
        v.cat,
        v.fclass,
        v.directed,
        v.measured_i,
	m.measured;
end transaction;

vacuum analyze init;




