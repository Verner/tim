/* PARAMETERS
    #RUN_LENGTH 	hh:mm:ss.msec (time it took to run)
    #COMMAND 		commandline
    #CODE_VERSION	git version of the program
    #MEASUREMENT_SET	measurement set
    #MGRAPH_ID          the corresponding measured graph
    #CONNECTION_COND	sql where condition limiting the connection set
    #COMMENTS		comments
    #N_ITER		number of iterations of the computation run
    #INIT_RUN		corresponding init run
*/
begin transaction;
drop table if exists computation CASCADE;
create temporary table computation (
	nodeid INTEGER,
	intensity FLOAT
);
copy computation from '/tmp/results' USING DELIMITERS ' ';

INSERT INTO computation_runs ( run_length, command_line, code_version, measurement_set, data_subset_CONDITION, run_type, comments, n_iterations, init_run )
VALUES( INTERVAL '#RUN_LENGTH', '#COMMAND', '#CODE_VERSION', #MEASUREMENT_SET, '#CONNECTION_COND', 'compute', '#COMMENTS', #N_ITER, #INIT_RUN );

insert into edge_run_data
select c.nodeid,
       c.intensity,
       i.reliability,
       currval('computation_seq'),
       #MGRAPH_ID
from computation as c, edge_run_data as i
where i.run_id = #INIT_RUN AND i.id = c.nodeid;

create temporary table edata (
  nodeid integer,
  intensity integer,
  reliability float
);
insert into edata
select c.nodeid,
       c.intensity,
       i.reliability
from computation as c, edge_run_data as i
where i.run_id = #INIT_RUN AND i.id = c.nodeid;

create temporary table mgrp (
  id integer,
  cat integer,
  multiplicity integer
);
insert into mgrp
select id, cat, multiplicity
from mgraph_edges
where mgraph_id = #MGRAPH_ID;

delete from map_layer_cur_computation_data;
insert into map_layer_cur_computation_data
SELECT v.gid,
       v.cat,
       v.fclass,
       v.directed,
       m.measured,
       round(sum ( e.intensity )/ max(g.multiplicity))
  FROM vzorek v,
       edata e,
       mgrp g,
       measurements as m
 WHERE ( ( g.id = e.nodeid ) AND 
         ( g.cat = v.cat )   AND 
         ( m.cat = g.cat )   AND
	 ( m.measurement_set = #MEASUREMENT_SET) )
 GROUP BY v.gid,
        v.the_geom,
        v.cat,
        v.fclass,
        v.directed,
        v.measured_i,
	m.measured;

end transaction;

vacuum analyze computation;


