#ifndef _MAIN_DLG_H_
#define _MAIN_DLG_H_

#include <QtSql/QSqlDatabase>
#include <QtGui/QDialog>

#include "ui_maindlg.h"

class QSqlQueryModel;
class tableDialog;

class mainDialog: public QDialog {
  Q_OBJECT
  
public:
  mainDialog( QWidget *parent = 0);
  virtual ~mainDialog();
  
public slots:
  
  void connectToDB();
  void changeDB();
  void selectNetwork();
  void selectRestrictions();
  void selectAADF();
  void disableSelectTables();
  void enableSelectTables();
  
  void runInit();
  void runCompute();

  
signals:
  
private:
  void dbase_disconnect(bool doDisconnect = false);
  void dbase_connect();
  
  void setupTables();
  
  QSqlDatabase db;
  Ui::mainDlg ui;
  bool connected;
  QSqlQueryModel *dbModel, *schemaModel;
  tableDialog *networkTbl, *restrictionsTbl, *aadfTbl;

};


#endif //_MAIN_DLG_H_