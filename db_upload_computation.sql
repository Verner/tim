begin transaction;
drop view if exists map_layer_computation CASCADE;
drop table if exists computation CASCADE;
create table computation (
	nodeid INTEGER,
	intensity FLOAT
);
copy computation from '/tmp/results' USING DELIMITERS ' ';

drop index if exists i_computation_id;
create index i_computation_id on computation ( nodeid );


create view map_layer_computation as
SELECT v.gid,
       v.cat,
       v.fclass,
       v.directed,
       m.measured AS measured,
       round(sum ( i.intensity )/ max(g.multiplicity)) AS intensity,
       v.the_geom
  FROM vzorek v,
       computation i,
       graph_edges g,
       measured_data as m
 WHERE ( ( g.id = i.nodeid ) AND ( g.cat = v.cat ) AND ( m.cat = g.cat ) )
 GROUP BY v.the_geom,
        v.gid,
        v.cat,
        v.fclass,
        v.directed,
        v.measured_i,
	m.measured;
end transaction;

vacuum analyze computation;


