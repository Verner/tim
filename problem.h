#ifndef _problem_H
#define _problem_H

/***************************************************************
 * problem.h
 * @Author:      Jonathan Verner (jonathan.verner@matfyz.cz)
 * @License:     GPL v2.0 or later
 * @Created:     2009-05-18.
 * @Last Change: 2009-05-18.
 * @Revision:    0.0
 * Description:
 * Usage:
 * TODO:
 *CHANGES:
 ***************************************************************/

#include <string>
#include <iosfwd>

#include <list>
#include <vector>

#include "defs.h"

class graph;
class problemState;
class dataSet;

class problem { 
	private:
		graph *G;
		problemState *state;
		real epsilon;
		long long dump_interval;
		std::string dumpBaseName, controlFileName;
		bool allow_price_drop;
		enum OPERATION_TYPE { UP, DOWN };

		void processActiveNode( node N );
		edgeList getPushList( node N ) const;
		edgeList getPushList( node N, OPERATION_TYPE op ) const;
		void pushFlow( node N, edge E );
		void pushFlow( node N, edge E, OPERATION_TYPE op );
		void increasePrice( node N );
		void decreasePrice( node N );
		void dump();

		/* After modifying the graph structure to disable
		 * U-turns, original Edge IDs will remain
		 * valid. New edges are added with greater
		 * IDs. Original nodeIDs DO NOT remain valid
		 * but node2New will contain a mapping from
		 * old nodeIDs to (a subset of) new nodeIDs */
		std::vector<node> node2New;
		
		dataSet *dSet;

		
	public:
		void load( const dataSet &dSet, graph *G = NULL, const std::string control_fname = "control" );
		void save( dataSet &dSet ) const;
		void solve( real Epsilon = -1, bool allowDROP = false );
		void loadCheckPoint( dataSet *dSet, std::istream& checkpoint_file, long long dumpInterval = -1, const std::string& d_prefix = "" );
		bool checkSolution() const;

		void setDump(dataSet *dSet, std::string fname, long long interval);
		
		void statusOutput( std::ostream &out );
		friend std::ostream &operator<<( std::ostream &out, const problem &prob );
		void surplusDump() const;
};


#endif /* _problem_H */
