#!/bin/bash
# watch.sh -- created 2009-08-18, Jonathan Verner
# @Last Change: 24-Dec-2008.
# @Revision:    0.0

gnuplot cost_graph.pl &
gnuplot surplus_graph.pl &

# vi: 
