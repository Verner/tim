begin transaction;
DROP SEQUENCE IF EXISTS node_seq CASCADE;
DROP SEQUENCE IF EXISTS edge_seq CASCADE;
DROP TABLE IF EXISTS graph_edges CASCADE;
DROP TABLE IF EXISTS graph_nodes CASCADE;
DROP TABLE IF EXISTS node_cats CASCADE;
DROP TABLE IF EXISTS output_graph CASCADE;

/* NODES */
CREATE SEQUENCE node_seq MINVALUE 0;
/*CREATE TYPE nodeType AS ENUM ('regular', 'source', 'sink');*/

CREATE TABLE graph_nodes (
	id INTEGER DEFAULT nextval('node_seq'),
	cat INT8 DEFAULT 0,
	edge_cat INT8,
/*	nType nodeType DEFAULT 'regular', */
        nType VARCHAR(8) DEFAULT 'regular',
	intensity INTEGER DEFAULT 0
);

/* EDGES */
CREATE SEQUENCE edge_seq MINVALUE 0;

CREATE TABLE graph_edges (
	id INTEGER DEFAULT nextval('edge_seq'),
	cat INT8,
	fromID INTEGER,
	toID INTEGER,
	fClass INT2,
	multiplicity INTEGER
);
drop index if exists i_graph_edges_id;
drop index if exists i_graph_edges_cat;
create index i_graph_edges_id on graph_edges ( id );
create index i_graph_edges_cat on graph_edges ( cat );


/* POPULATE NODES */

CREATE TEMPORARY TABLE node_cats (
	cat INT8
);

INSERT INTO node_cats 
SELECT fromCAT
FROM connections;

INSERT INTO node_cats
SELECT toCAT
FROM connections;

INSERT INTO graph_nodes (cat) 
SELECT DISTINCT cat
FROM node_cats;

DROP TABLE node_cats;

/* Insert source nodes */
INSERT INTO graph_nodes (edge_cat, nType, intensity)
SELECT l.cat, 'source', l.measured
FROM measured_data as l
WHERE l.measured > 0 AND l.directed = 'F';

/* Insert sink nodes */
INSERT INTO graph_nodes (edge_cat, nType, intensity)
SELECT l.cat, 'sink', -l.measured
FROM measured_data as l
WHERE l.measured > 0 AND l.directed = 'F';




/* Insert source nodes for bidirectional links*/
INSERT INTO graph_nodes (edge_cat, nType, intensity)
SELECT l.cat, 'srcF', l.measured/2
FROM measured_data as l
WHERE l.measured > 0 AND l.directed = 'B';
INSERT INTO graph_nodes (edge_cat, nType, intensity)
SELECT l.cat, 'srcT', l.measured/2
FROM measured_data as l
WHERE l.measured > 0 AND l.directed = 'B';

/* Insert sink nodes for bidirectional links*/
INSERT INTO graph_nodes (edge_cat, nType, intensity)
SELECT l.cat, 'snkF', -l.measured/2
FROM measured_data as l
WHERE l.measured > 0 AND l.directed = 'B';
INSERT INTO graph_nodes (edge_cat, nType, intensity)
SELECT l.cat, 'snkT', -l.measured/2
FROM measured_data as l
WHERE l.measured > 0 AND l.directed = 'B';





/* POPULATE EDGES */

/* Non measured directed F --> T (F) links */
INSERT INTO graph_edges (cat, fromID, toID, fClass, multiplicity)
SELECT l.cat, nf.id, nt.id, l.fClass, 1
FROM measured_data as l, graph_nodes as nf, graph_nodes as nt
WHERE l.fromCAT = nf.cat AND l.toCAT = nt.cat AND nf.nType = 'regular' AND nt.nType = 'regular' AND l.directed = 'F' AND l.measured IS NULL;

/* Non measured undirected (B) links */
INSERT INTO graph_edges (cat, fromID, toID, fClass, multiplicity)
SELECT l.cat, nf.id, nt.id, l.fClass, 1
FROM measured_data as l, graph_nodes as nf, graph_nodes as nt
WHERE l.fromCAT = nf.cat AND l.toCAT = nt.cat AND nf.nType = 'regular' AND nt.nType = 'regular' AND l.directed = 'B' AND l.measured IS NULL;

INSERT INTO graph_edges (cat, fromID, toID, fClass, multiplicity)
SELECT l.cat, nt.id, nf.id, l.fClass, 1
FROM measured_data as l, graph_nodes as nf, graph_nodes as nt
WHERE l.fromCAT = nf.cat AND l.toCAT = nt.cat AND nf.nType = 'regular' AND nt.nType = 'regular' AND l.directed = 'B' AND l.measured IS NULL;

/* Measured directed F --> T (F) links */

INSERT INTO graph_edges (cat, fromID, toID, fClass, multiplicity)
SELECT l.cat, nf.id, nt.id, l.fClass, 2
FROM measured_data as l, graph_nodes as nf, graph_nodes as nt
WHERE l.cat = nf.edge_cat AND l.toCAT = nt.cat AND nf.nType = 'source' AND nt.nType = 'regular' AND l.directed = 'F' AND l.measured > 0;

INSERT INTO graph_edges (cat, fromID, toID, fClass, multiplicity)
SELECT l.cat, nf.id, nt.id, l.fClass, 2
FROM measured_data as l, graph_nodes as nf, graph_nodes as nt
WHERE l.cat = nt.edge_cat AND l.fromCAT = nf.cat AND nf.nType = 'regular' AND nt.nType = 'sink' AND l.directed = 'F' AND l.measured > 0;


/* Measured undirected (B) links */

INSERT INTO graph_edges (cat, fromID, toID, fClass, multiplicity)
SELECT l.cat, nf.id, nt.id, l.fClass, 2
FROM measured_data as l, graph_nodes as nf, graph_nodes as nt
WHERE l.cat = nf.edge_cat AND l.toCAT = nt.cat AND nf.nType = 'srcF' AND nt.nType = 'regular' AND l.directed = 'B' AND l.measured > 0;

INSERT INTO graph_edges (cat, fromID, toID, fClass, multiplicity)
SELECT l.cat, nf.id, nt.id, l.fClass, 2
FROM measured_data as l, graph_nodes as nf, graph_nodes as nt
WHERE l.cat = nt.edge_cat AND l.fromCAT = nf.cat AND nf.nType = 'regular' AND nt.nType = 'snkF' AND l.directed = 'B' AND l.measured > 0;

INSERT INTO graph_edges (cat, fromID, toID, fClass, multiplicity)
SELECT l.cat, nf.id, nt.id, l.fClass, 2
FROM measured_data as l, graph_nodes as nf, graph_nodes as nt
WHERE l.cat = nf.edge_cat AND l.fromCAT = nt.cat AND nf.nType = 'srcT' AND nt.nType = 'regular' AND l.directed = 'B' AND l.measured > 0;

INSERT INTO graph_edges (cat, fromID, toID, fClass, multiplicity)
SELECT l.cat, nf.id, nt.id, l.fClass, 2
FROM measured_data as l, graph_nodes as nf, graph_nodes as nt
WHERE l.cat = nt.edge_cat AND l.toCAT = nf.cat AND nt.nType = 'snkT' AND nf.nType = 'regular' AND l.directed = 'B' AND l.measured > 0;

/* Graph */
CREATE TEMPORARY TABLE output_graph (
	id INTEGER,
	fromID INTEGER,
	toID INTEGER,
	fClass INT2
);

INSERT INTO output_graph
SELECT id, fromID, toID, fClass
FROM graph_edges;

COPY output_graph TO '/tmp/edges' USING DELIMITERS ' ';

DROP TABLE output_graph;

/* Measurements */
create temporary table  output_meas (
	id int8,
	intensity integer
);
insert into output_meas 
select id, intensity from graph_nodes 
where ntype != 'regular';

copy output_meas to '/tmp/meas' using delimiters ' ';
drop table output_meas;

/* Neibours */
create temporary table output_nbs (
	idA int8,
	idB int8
);

insert into output_nbs
SELECT sink.id as id, nxt.id as nid
FROM graph_nodes as sink, 
     graph_edges as e_tonbr, 
     graph_edges as e_tosink,
     graph_nodes as nxt,
     graph_nodes as prv
WHERE 
        ( sink.ntype='sink' OR sink.ntype='snkF' OR sink.ntype='snkT' )
      AND 
        ( e_tonbr.cat = sink.edge_cat AND e_tosink.cat = sink.edge_cat)
      AND 
        ( e_tosink.toid = sink.id AND e_tonbr.toid = nxt.id )
      AND 
        ( prv.id = e_tosink.fromid )
      AND 
        nxt.ntype='regular' AND prv.ntype = 'regular'
      AND 
        NOT e_tonbr.toid = prv.id
UNION ALL 
SELECT src.id as id, prv.id as nid
FROM graph_nodes as src,
     graph_edges as e_fromprv,
     graph_edges as e_tonxt,
     graph_nodes as prv,
     graph_nodes as nxt
WHERE
         ( src.ntype='source' OR src.ntype='srcF' OR src.ntype = 'srcT' )
     AND
	 ( e_tonxt.cat = src.edge_cat AND e_fromprv.cat = src.edge_cat )
     AND
         ( e_tonxt.fromid = src.id AND e_fromprv.fromid = prv.id )
     AND 
         ( nxt.id = e_tonxt.toid )
     AND
         ( nxt.ntype = 'regular' AND prv.ntype = 'regular' )
     AND
         NOT e_fromprv.fromid = nxt.id
ORDER BY
        id;
copy output_nbs to '/tmp/nbs' using delimiters ' ';
drop table output_nbs;

/* Zakazy */
create temporary table  output_zakazy (
	fid int8,
	tid int8
);
insert into output_zakazy 
select f.id, t.id from zakazy as z, graph_edges as f, graph_edges as t 
where z.catfrom = f.cat and z.catto = t.cat;
copy output_zakazy to '/tmp/zakazy' using delimiters ' ';
drop table output_zakazy;




end transaction;
