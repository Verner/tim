/***************************************************************
 * problemState.cpp
 * @Author:      Jonathan Verner (jonathan.verner@matfyz.cz)
 * @License:     GPL v2.0 or later
 * @Created:     2009-05-22.
 * @Last Change: 2009-05-22.
 * @Revision:    0.0
 * Description:
 * Usage:
 * TODO:
 *CHANGES:
 ***************************************************************/

#include "problemState.h"

#include "dataSet.h"
#include "global_config.h"

#include <fstream>
#include <iostream>
#include <string>


using namespace std;

const long problemState::C_DOWN_OPERATIONS_1 = 100000000;
const real problemState::C_DOWN_OPERATIONS_2 = 0.00001;
//const long problemState::maxActive = 1000000;

ostream &operator<<( ostream &OUT, const problemStats &stats ) {
  OUT << "************* Status Summary ************"<< endl;
  OUT << "Total iterations: " << stats.iteration << endl;
  OUT << "Total cost: " << stats.cost << endl;
  OUT << "Total node flow: " << stats.totalNodeFlow << "( Max: " << stats.maxNodeFlow << " )"<< endl;
  OUT << "Total surplus: " << stats.totalSurplus <<" ( "<<stats.numOfNodes<<" nodes,"<<stats.numOfSurplusNodes<<" surplus nodes)"<<endl;
  OUT << "Max surplus: " << stats.maxSurplus << " [ " << stats.maxSurplusFraction <<" relative]"<<endl;
  OUT << "Active nodes: " << stats.activeNodes << " ( "<<stats.maxActive<<" maximum, "<< stats.canDown<< " can down)" << endl;
  OUT << "Cumulative surplus: " << stats.cumSurplus << endl;
  OUT << "*****************************************"<<endl;
  return OUT;
}

void problemState::calculateStats( struct problemStats &stats, const int histBins ) const {
  stats.numOfEdges=G->numOfEdges();
  stats.numOfNodes=G->numOfNodes();
  stats.iteration = iteration;
  stats.numOfSurplusNodes=0;
  stats.totalSurplus=0;
  stats.totalSurplusFraction=0;
  stats.maxSurplus=0;
  stats.activeNodes=activeNodesSize;
  stats.maxActive=maxActive;
  stats.canDown=0;
  stats.cost=computeTotalCost();
  stats.cumSurplus = cumulativeSurplus;
  stats.totalNodeFlow = 0;
  stats.maxNodeFlow = 0;
  for( node N = G->numOfNodes()-1; N>=0; --N ) {
    if ( ! FP_EQ(0, getSurplus(N)) ) {
      real absSurplus = ABS<real>(getSurplus(N)),
           nodeFlow = getNodeFlow( N );
      stats.numOfSurplusNodes++;
      stats.totalSurplus+=(long long unsigned int) absSurplus;
      stats.maxSurplus=(int) MAX<real>(absSurplus,(real) stats.maxSurplus);
      if ( nodeFlow > 0 ) { 
	stats.maxSurplusFraction=MAX<real>(absSurplus/MAX<real>(nodeFlow,50),stats.maxSurplusFraction);
        stats.totalSurplusFraction+=absSurplus/MAX<real>(nodeFlow,50);
        stats.totalNodeFlow+=(int) nodeFlow;
	stats.maxNodeFlow=(int) MAX<real>(nodeFlow, (real) stats.maxNodeFlow);
      }
      if ( canActivateNegative(N) ) stats.canDown++;
    }
  }
  if ( histBins > 0 ) {
  }
};

real problemState::getNodeFlow(const node N) const {
  edgeList in = G->inEdges(N), out = G->outEdges(N);
  real inFlow=0,outFlow=0;
  for( edgeList::iterator E = in.begin(); E != in.end(); ++E ) {
    inFlow+=flowVector[*E];
  }
  for( edgeList::iterator E = out.begin(); E != out.end(); ++E ) {
    outFlow+=flowVector[*E];
  }
  return (inFlow+outFlow)/2;
}

void problemState::randomizeActiveNodes(real factor) {
  if ( FP_SLE(factor,0) ) {
    problemStats stats;
    calculateStats(stats);
    factor = (real)((real)stats.numOfSurplusNodes/(real)maxActive);
  }
  if ( ! activeNodes.empty() ) activeNodes.clear();
  activeNodesSize=0;
  try {
    for( node N = G->numOfNodes()-1; N>=0; --N ) {
      if ( FP_SLE(0,surplusVector[N]) || ( allow_price_drop && FP_SLE(surplusVector[N],0) ) ) {
	if ( random_bit( factor ) ) {
	  activeNodes.push_back(N);
	  activeNodesSize++;
	  if ( activeNodesSize >= maxActive ) {
	    activeNodesUpToDate=false;
	    return;
	  }
	} else activeNodesUpToDate=false;
      }
    }
    activeNodesUpToDate=true;
  } catch (...) {
    activeNodesUpToDate=false;
  }
}

void problemState::printNode( node N ) const { 
  edgeList IN = G->inEdges( N ), OUT = G->outEdges( N );
  cerr<<"******* NODE( "<<N<<" ) *******"<<endl;
  cerr<<"Supply:  "<<supplyVector[ N ] <<";"<<endl;
  cerr<<"Surplus: "<<getSurplus( N ) <<";"<<endl;
  cerr<<"Price:   "<<getPrice( N ) <<";"<<endl;
  if ( IN.size()>0 ) { 
  cerr<<" __ IN __"<<endl;
  for( edgeList::iterator E = IN.begin(); E != IN.end(); ++E ) { 
    cerr<<*E<<" : "<<G->fromNode(*E)<<" --> "<<G->toNode(*E) <<"; Flow = "<<flowVector[*E] <<"; drC_E(flow) = ";
    cerr<<cost[*E].drC(flowVector[*E])<<"; From Price = "<<getPrice( G->fromNode(*E) )<<";"<<endl;
    cerr<<"Cost function: " << cost[*E] << endl;
  }}
  if ( OUT.size() > 0 ) { cerr<<" __ OUT __"<<endl;
  for( edgeList::iterator E = OUT.begin(); E != OUT.end(); ++E ) { 
    cerr<<*E<<" : "<<G->fromNode(*E)<<" --> "<<G->toNode(*E) <<"; Flow = "<<flowVector[*E] <<"; drC_E(flow) = ";
    cerr<<cost[*E].drC(flowVector[*E])<<"; To Price = "<<getPrice( G->toNode(*E) )<<";"<<endl;
    cerr<<"Cost function: " << cost[*E] << endl;
  }}
  cerr<<"********************************"<<endl;
}

void problemState::load( const dataSet &dSet, graph *gr ) { 
  if ( ! gr ) { 
    G = new graph();
    G->load( dSet );
  } else G = gr;
  edge maxE = G->numOfEdges();
  node maxN = G->numOfNodes();
  
#ifndef VERSION_1
  real factors[] = { 0.01, 0.04, 0.09, 0.16, 0.25 };
  factors[0] = (int) global_config.getNum(COST_FACTOR_FC1_OPTION);
  factors[1] = (int) global_config.getNum(COST_FACTOR_FC2_OPTION);
  factors[2] = (int) global_config.getNum(COST_FACTOR_FC3_OPTION);
  factors[3] = (int) global_config.getNum(COST_FACTOR_FC4_OPTION);
  factors[4] = (int) global_config.getNum(COST_FACTOR_FC5_OPTION);
#endif

  /* Resize the flow, cost, surplus, supply and price vectors
   * to accomodate the graph nodes and edges */
  flowVector.resize( maxE, 0 );
  cost.resize( maxE );
  surplusVector.resize( maxN, 0 );
  supplyVector.resize( maxN, 0 );
  priceVector.resize( maxN, 0 );
  upOperations.resize( maxN, 0 );
  node_data_type numOfSources=0, numOfSinks=0;
  cumulativeSurplus=0;

  real factorRel, factorFC;
  for( vector<dataRow>::const_iterator row = dSet.begin(); row != dSet.end(); ++row ) {
    switch( row->typ ) { 
      case dataRow::INVALID:
	continue;
      case dataRow::SOURCE:
	// Actually set sources/sinks from the initialized values
	// and not from aadf since aadf is sometimes inconsistent
	supplyVector[ G->fromNode( row->id ) ] = row->initialized;
	//supplyVector[ G->fromNode( row->id ) ] = row->measured;
	numOfSources++;
	break;
      case dataRow::SINK:
	// Actually set sources/sinks from the initialized values
	// and not from aadf since aadf is sometimes inconsistent
	supplyVector[ G->toNode( row->id ) ] = -row->initialized;
	//supplyVector[ G->toNode( row->id ) ] = row->measured;
	numOfSinks++;
	break;
      default:
	break;
    }

    /* Set the initial flow estimate */
    real ubound = row->ubound,lbound=row->lbound;
    if ( ubound < 0 ) ubound = 1000000;
    if ( lbound < 0 ) lbound = 0;
    real flow = MIN<real>(ubound,MAX<real>( row->initialized, row->lbound ));
    real rel = row->rel;
    if ( flow < 0 ) flow = 0;
    if ( rel < 0 ) rel = 0;
    flowVector[ row->id ] = flow;

    /* Initialize the cost functions based on the initial
     * flow estimate */

#ifdef VERSION_1
    factorFC = EXP<int>( G->getFClass(row->id, FORWARD), 2 )*0.01;
#else
    factorFC = factors[G->getFClass(row->id, FORWARD)-1];
#endif
    
    factorRel = EXP<real>( rel, 2 ) + 0.001;
    cost[row->id].setFactor( MAX<real>(factorFC, factorRel) );
    cost[row->id].setMinCostFlow( flow );
    if ( row->lbound > 0 ) cost[row->id].setLBound( row->lbound );
    if ( row->ubound > 0 ) cost[row->id].setUBound( row->ubound );
  }

  // Update the surplus vector
  for(node N = G->numOfNodes()-1; N>=0; --N) {
    updateSurplus( N );
    cumulativeSurplus += surplusVector[N];
  }

#ifdef D_ECS
  // The e-CS conditions must be satisfied
  assert(checkECS());
#endif

}

void problemState::save( dataSet &dSet ) const { 
  for( edge E = dSet.size()-1; E >= 0; --E ) { 
    dSet[ E ].computed = (int) flowVector[E];
  }
}

void problemState::checkPointSave( ostream &OUT ) const {
  for( edge E = G->numOfEdges()-1; E>=0; --E )
    OUT << flowVector[E] << endl;
  for( node N = G->numOfNodes()-1; N>=0; --N )
    OUT << priceVector[N] << " " << upOperations[N]<< endl;
  OUT << epsilon << " " << iteration << " "<< cumulativeSurplus << " "<< (int) allow_price_drop << endl;
}

void problemState::checkPointRestart( istream &IN ) { 
  for( edge E = G->numOfEdges()-1; E>=0; --E )
    IN >> flowVector[E];
  for( node N = G->numOfNodes()-1; N>=0; --N ) {
    IN >> priceVector[N] >> upOperations[N];
    updateSurplus(N);
  }
  int apd;
  IN >> epsilon >> iteration >> cumulativeSurplus >> apd;
  allow_price_drop = apd;
#ifdef D_ECS
  assert(checkECS());
#endif
}

bool problemState::sanityCheck() const {
  bool ok=true;
  for( node N = G->numOfNodes()-1; N>=0; --N) {
    if ( G->outEdges(N).empty() ) {
      if ( FP_LEQ(0,supplyVector[N]) ) {
	cerr << "Terminal node "<<N<<" is not a sink."<<endl;
	ok=false;
      }
    }
    if ( G->inEdges(N).empty() ) {
      if ( FP_LEQ(supplyVector[N],0) ) { 
	cerr << "Initial node "<<N<<" is not a source.";
	ok=false;
      }
    }

  }
  for( edge E=G->numOfEdges()-1;E>=0;--E){
  if (!( FP_LEQ(cost[E].getLBound(),cost[E].getMCF() ) && FP_LEQ(cost[E].getMCF(),cost[E].getUBound()) )) {
    cerr << "Inconsistent u/l-Bounds and minCost for edge "<<E<<endl;
    cerr << cost[E];
    ok=false;
  }}
  return ok;
}

