/* 
 * PARAMETERS
 *   #MEASUREMENT_SET
 *   #CONNECTION_COND
 */
begin transaction;
DROP SEQUENCE IF EXISTS tnode_seq CASCADE;
DROP SEQUENCE IF EXISTS tedge_seq CASCADE;
DROP TABLE IF EXISTS tgraph_edges CASCADE;
DROP TABLE IF EXISTS tgraph_nodes CASCADE;
DROP TABLE IF EXISTS tnode_cats CASCADE;
DROP VIEW IF EXISTS tmeasured_data;


CREATE VIEW tmeasured_data AS
SELECT * FROM measurements WHERE measurement_set = #MEASUREMENT_SET;

/* NODES */
CREATE SEQUENCE tnode_seq MINVALUE 0;
/*CREATE TYPE nodeType AS ENUM ('regular', 'source', 'sink');*/

CREATE TEMPORARY TABLE tgraph_nodes (
	id INTEGER DEFAULT nextval('tnode_seq'),
	cat INT8 DEFAULT 0,
	edge_cat INT8,
/*	nType nodeType DEFAULT 'regular', */
        nType VARCHAR(8) DEFAULT 'regular',
	intensity INTEGER DEFAULT 0
);

/* EDGES */
CREATE SEQUENCE tedge_seq MINVALUE 0;

CREATE TEMPORARY TABLE tgraph_edges (
	id INTEGER DEFAULT nextval('tedge_seq'),
	cat INT8,
	fromID INTEGER,
	toID INTEGER,
	fClass INT2,
	multiplicity INTEGER
);
drop index if exists i_tgraph_edges_id;
drop index if exists i_tgraph_edges_cat;
create index i_tgraph_edges_id on tgraph_edges ( id );
create index i_tgraph_edges_cat on tgraph_edges ( cat );


/* POPULATE NODES */

CREATE TEMPORARY TABLE tnode_cats (
	cat INT8
);

INSERT INTO tnode_cats 
SELECT fromCAT
FROM connections WHERE #CONNECTION_COND;

INSERT INTO tnode_cats
SELECT toCAT
FROM connections WHERE #CONNECTION_COND;

INSERT INTO tgraph_nodes (cat) 
SELECT DISTINCT cat
FROM tnode_cats WHERE #CONNECTION_COND;

DROP TABLE tnode_cats;

/* Insert source nodes */
INSERT INTO tgraph_nodes (edge_cat, nType, intensity)
SELECT l.cat, 'source', l.measured
FROM tmeasured_data as l
WHERE l.measured > 0 AND l.directed = 'F';

/* Insert sink nodes */
INSERT INTO tgraph_nodes (edge_cat, nType, intensity)
SELECT l.cat, 'sink', -l.measured
FROM tmeasured_data as l
WHERE l.measured > 0 AND l.directed = 'F';




/* Insert source nodes for bidirectional links*/
INSERT INTO tgraph_nodes (edge_cat, nType, intensity)
SELECT l.cat, 'srcF', l.measured/2
FROM tmeasured_data as l
WHERE l.measured > 0 AND l.directed = 'B';
INSERT INTO tgraph_nodes (edge_cat, nType, intensity)
SELECT l.cat, 'srcT', l.measured/2
FROM tmeasured_data as l
WHERE l.measured > 0 AND l.directed = 'B';

/* Insert sink nodes for bidirectional links*/
INSERT INTO tgraph_nodes (edge_cat, nType, intensity)
SELECT l.cat, 'snkF', -l.measured/2
FROM tmeasured_data as l
WHERE l.measured > 0 AND l.directed = 'B';
INSERT INTO tgraph_nodes (edge_cat, nType, intensity)
SELECT l.cat, 'snkT', -l.measured/2
FROM tmeasured_data as l
WHERE l.measured > 0 AND l.directed = 'B';

/* POPULATE EDGES */

/* Non measured directed F --> T (F) links */
INSERT INTO tgraph_edges (cat, fromID, toID, fClass, multiplicity)
SELECT l.cat, nf.id, nt.id, l.fClass, 1
FROM tmeasured_data as l, tgraph_nodes as nf, tgraph_nodes as nt
WHERE l.fromCAT = nf.cat AND l.toCAT = nt.cat AND nf.nType = 'regular' AND nt.nType = 'regular' AND l.directed = 'F' AND l.measured IS NULL;

/* Non measured undirected (B) links */
INSERT INTO tgraph_edges (cat, fromID, toID, fClass, multiplicity)
SELECT l.cat, nf.id, nt.id, l.fClass, 1
FROM tmeasured_data as l, tgraph_nodes as nf, tgraph_nodes as nt
WHERE l.fromCAT = nf.cat AND l.toCAT = nt.cat AND nf.nType = 'regular' AND nt.nType = 'regular' AND l.directed = 'B' AND l.measured IS NULL;

INSERT INTO tgraph_edges (cat, fromID, toID, fClass, multiplicity)
SELECT l.cat, nt.id, nf.id, l.fClass, 1
FROM tmeasured_data as l, tgraph_nodes as nf, tgraph_nodes as nt
WHERE l.fromCAT = nf.cat AND l.toCAT = nt.cat AND nf.nType = 'regular' AND nt.nType = 'regular' AND l.directed = 'B' AND l.measured IS NULL;

/* Measured directed F --> T (F) links */

INSERT INTO tgraph_edges (cat, fromID, toID, fClass, multiplicity)
SELECT l.cat, nf.id, nt.id, l.fClass, 2
FROM tmeasured_data as l, tgraph_nodes as nf, tgraph_nodes as nt
WHERE l.cat = nf.edge_cat AND l.toCAT = nt.cat AND nf.nType = 'source' AND nt.nType = 'regular' AND l.directed = 'F' AND l.measured > 0;

INSERT INTO tgraph_edges (cat, fromID, toID, fClass, multiplicity)
SELECT l.cat, nf.id, nt.id, l.fClass, 2
FROM tmeasured_data as l, tgraph_nodes as nf, tgraph_nodes as nt
WHERE l.cat = nt.edge_cat AND l.fromCAT = nf.cat AND nf.nType = 'regular' AND nt.nType = 'sink' AND l.directed = 'F' AND l.measured > 0;


/* Measured undirected (B) links */

INSERT INTO tgraph_edges (cat, fromID, toID, fClass, multiplicity)
SELECT l.cat, nf.id, nt.id, l.fClass, 2
FROM tmeasured_data as l, tgraph_nodes as nf, tgraph_nodes as nt
WHERE l.cat = nf.edge_cat AND l.toCAT = nt.cat AND nf.nType = 'srcF' AND nt.nType = 'regular' AND l.directed = 'B' AND l.measured > 0;

INSERT INTO tgraph_edges (cat, fromID, toID, fClass, multiplicity)
SELECT l.cat, nf.id, nt.id, l.fClass, 2
FROM tmeasured_data as l, tgraph_nodes as nf, tgraph_nodes as nt
WHERE l.cat = nt.edge_cat AND l.fromCAT = nf.cat AND nf.nType = 'regular' AND nt.nType = 'snkF' AND l.directed = 'B' AND l.measured > 0;

INSERT INTO tgraph_edges (cat, fromID, toID, fClass, multiplicity)
SELECT l.cat, nf.id, nt.id, l.fClass, 2
FROM tmeasured_data as l, tgraph_nodes as nf, tgraph_nodes as nt
WHERE l.cat = nf.edge_cat AND l.fromCAT = nt.cat AND nf.nType = 'srcT' AND nt.nType = 'regular' AND l.directed = 'B' AND l.measured > 0;

INSERT INTO tgraph_edges (cat, fromID, toID, fClass, multiplicity)
SELECT l.cat, nf.id, nt.id, l.fClass, 2
FROM tmeasured_data as l, tgraph_nodes as nf, tgraph_nodes as nt
WHERE l.cat = nt.edge_cat AND l.toCAT = nf.cat AND nt.nType = 'snkT' AND nf.nType = 'regular' AND l.directed = 'B' AND l.measured > 0;

INSERT INTO mgraphs (base_msetID)
VALUES( #MEASUREMENT_SET );
	
INSERT INTO mgraph_edges
SELECT id, cat, fromID, toID, fClass, multiplicity, currval('mgraphs_seq')
FROM tgraph_edges;

INSERT INTO mgraph_nodes
SELECT id, cat, edge_cat, nType, intensity, currval('mgraphs_seq')
FROM tgraph_nodes;

DROP SEQUENCE IF EXISTS tnode_seq CASCADE;
DROP SEQUENCE IF EXISTS tedge_seq CASCADE;
DROP TABLE IF EXISTS tgraph_edges CASCADE;
DROP TABLE IF EXISTS tgraph_nodes CASCADE;
DROP TABLE IF EXISTS tnode_cats CASCADE;
DROP VIEW IF EXISTS tmeasured_data;
drop index if exists i_tgraph_edges_id;
drop index if exists i_tgraph_edges_cat;
end transaction;

