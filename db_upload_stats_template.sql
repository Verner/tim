/* PARAMETERS 
   #INIT_ID
   #C_ID
*/
insert into computation_stats 
select #INIT_ID, #C_ID, 
       i.fclass,
       round(avg(i.intensity)),
       round(stddev(i.intensity)),
       round(min(i.intensity)),
       round(max(i.intensity)),
       round(avg(c.intensity)),
       round(stddev(c.intensity)),
       round(min(c.intensity)),
       round(max(c.intensity)),
       round(avg(abs(c.intensity-i.intensity))),
       round(stddev(abs(c.intensity-i.intensity))),
       round(max(abs(c.intensity-i.intensity))),
       round(avg((c.intensity-i.intensity))),
       round(stddev((c.intensity-i.intensity))),
       avg(i.avgreliability),
       stddev(i.avgreliability)
from map_layer_init as i, map_layer_computation as c
where i.cat=c.cat and c.intensity > 0
group by i.fclass;
