#!/bin/bash
# run_all.sh -- created 2009-07-20, Jonathan Verner
# @Last Change: 24-Dec-2008.
# @Revision:    0.0

#PARAMETERS
#  $1 ... MEASUREMENT SET
#  $2 ... CONNECTION CONDITION

M_SET=$1;
C_COND=$2;

echo -n Preparing data ...
#cat db_prepare_computation_template.sql | sed -e"s/#MEASUREMENT_SET/$M_SET/g" \
#                                        | sed -e"s/#CONNECTION_COND/$C_COND/g" \
#                                        | psql mge 2>/dev/null >/dev/null
echo "db_create_measured_graph_template.sql" > /tmp/dbase_errors;
cat db_create_measured_graph_template.sql | sed -e"s/#MEASUREMENT_SET/$M_SET/g" \
                                          | sed -e"s/#CONNECTION_COND/$C_COND/g" \
                                          | psql mge 2>/tmp/dbase_errors >/dev/null;
MGRAPH_ID=`echo "select last_value from mgraphs_seq;" | psql -t -A mge`;
echo "db_create_files_template.sql" >> /tmp/dbase_errors;
cat db_create_files_template.sql | sed -e"s/#MEASURED_GRAPH/$MGRAPH_ID/g" | psql mge 2>>/tmp/dbase_errors >/dev/null;
echo " OK (mgraph_id = $MGRAPH_ID) "

echo -n 'Running phase 1 (initialization) ...'
# with lbounds
#/usr/bin/time --format="%C\t%M\t%K\t%U\t%S\t%E" -a -o /tmp/run-stats ./build/init -g /tmp/edges -m /tmp/meas -n /tmp/nbs -z /tmp/zakazy -l /tmp/lbounds > /tmp/init 2>/dev/null
/usr/bin/time --format="%C\t%M\t%K\t%U\t%S\t%E" -a -o /tmp/run-stats ./build-win/init.exe -g /tmp/edges -m /tmp/meas -n /tmp/nbs -z /tmp/zakazy > /tmp/init 2>/dev/null
echo ' OK'

echo -n 'Uploading initialization data into dbase ... '
# PARAMETERS
#    #RUN_LENGTH
#    #COMMAND
#    #CODE_VERSION
#    #MEASUREMENT_SET
#    #MGRAPH_ID
#    #CONNECTION_COND
#    #COMMENT
RUN_LENGTH=`cat /tmp/run-stats | tail -1 | cut -f 6`;
if [ `echo $RUN_LENGTH | sed -e's/[^:]//g' | wc -c` -le 2 ]; then
  RUN_LENGTH="0:$RUN_LENGTH";
fi;

cat /tmp/run-stats | tail -1 | cut -f 1 | sed -e's/\//\\\//g' > /tmp/m.$$;
CMD=`cat /tmp/m.$$`;
rm /tmp/m.$$;
GIT_VERSION=`git-rev-list HEAD | head -1`;
echo "db_upload_init_template.sql" >> /tmp/dbase_errors;
cat db_upload_init_template.sql |  sed -e"s/#RUN_LENGTH/$RUN_LENGTH/g" \
                                |  sed -e"s/#COMMAND/$CMD/g" \
                                |  sed -e"s/#CODE_VERSION/$GIT_VERSION/g" \
                                |  sed -e"s/#MEASUREMENT_SET/$M_SET/g" \
                                |  sed -e"s/#MGRAPH_ID/$MGRAPH_ID/g"   \
				|  sed -e"s/#CONNECTION_COND/$C_COND/g" \
                                |  sed -e"s/#COMMENTS//g" \
    | psql mge 2>>/tmp/dbase_errors >/dev/null;
INIT_RUN_ID=`echo "select last_value from computation_seq" | psql mge -t -A`;
echo ' OK'

echo -n 'Running phase 2 (balancing) ...'
/usr/bin/time --format="%C\t%M\t%K\t%U\t%S\t%E" -a -o /tmp/run-stats ./build-win/minCostFlow.exe -g /tmp/edges -m /tmp/meas -e 1000  -d 1000000 -f /tmp/dump -i /tmp/init -n /tmp/nbs --zakazy /tmp/zakazy 2> /tmp/minCostFlow.iteration > /tmp/results
echo ' OK'

echo -n 'Uploading final data into dbase ... '
# PARAMETERS
#    #RUN_LENGTH
#    #COMMAND
#    #CODE_VERSION
#    #MEASUREMENT_SET
#    #CONNECTION_COND
#    #COMMEN
RUN_LENGTH=`cat /tmp/run-stats | tail -1 | cut -f 6`;
if [ `echo $RUN_LENGTH | sed -e's/[^:]//g' | wc -c` -le 2 ]; then
  RUN_LENGTH="0:$RUN_LENGTH";
fi
cat /tmp/run-stats | tail -1 | cut -f 1 | sed -e's/\//\\\//g' > /tmp/m.$$;
CMD=`cat /tmp/m.$$`;
rm /tmp/m.$$
GIT_VERSION=`git-rev-list HEAD | head -1`
N_ITERATIONS=`cat /tmp/minCostFlow.iteration | grep "iterations:" | tail -1 | sed -e's/.*://g'`;
echo "db_upload_computation_template.sql" >> /tmp/dbase_errors;
cat db_upload_computation_template.sql  | sed -e"s/#RUN_LENGTH/$RUN_LENGTH/g" \
					| sed -e"s/#COMMAND/$CMD/g" \
					| sed -e"s/#CODE_VERSION/$GIT_VERSION/g" \
					| sed -e"s/#MEASUREMENT_SET/$M_SET/g" \
					| sed -e"s/#MGRAPH_ID/$MGRAPH_ID/g" \
					| sed -e"s/#CONNECTION_COND/$C_COND/g" \
					| sed -e"s/#COMMENTS//g" \
					| sed -e"s/#N_ITER/$N_ITERATIONS/g" \
					| sed -e"s/#INIT_RUN/$INIT_RUN_ID/g" \
    | psql mge 2>>/tmp/dbase_errors >/dev/null
COMPUTATION_RUN_ID=`echo "select last_value from computation_seq" | psql mge -t -A`;
echo ' OK'

echo Computing statistics ...
echo "db_upload_stats_template.sql" >> /tmp/dbase_errors;
cat db_upload_stats_template.sql | sed -e"s/#INIT_ID/$INIT_RUN_ID/g" \
				 | sed -e"s/#C_ID/$COMPUTATION_RUN_ID/g" \
    | psql mge 2>>/tmp/dbase_errors >/dev/null
echo ' OK'

echo Creating histograms ...
./col_hist.sh hist2-$COMPUTATION_RUN_ID.jpg map_layer_computation intensity fclass=2
./col_hist.sh hist3-$COMPUTATION_RUN_ID.jpg map_layer_computation intensity fclass=3
./col_hist.sh hist4-$COMPUTATION_RUN_ID.jpg map_layer_computation intensity fclass=4
./col_hist.sh hist5-$COMPUTATION_RUN_ID.jpg map_layer_computation intensity fclass=5
echo ' OK'

# vi: 
