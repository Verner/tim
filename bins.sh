#!/bin/bash
# bins.sh -- created 2009-12-10, Jonathan Verner
# @Last Change: 24-Dec-2008.
# @Revision:    0.0

file=$1;
num_bins=$2;

lines=`cat $file | wc -l`;
bin_size=`echo $lines/$num_bins | bc`;

temp=bins.$$;
mkdir $temp;
cd $temp;
cat ../$file | split -l $bin_size;
for i in x*; do echo '[' `head -1 $i` -- `tail -1 $i` ']'; done
rm x*;
cd ..
rmdir $temp;
# vi: 
