#ifndef _dataSet_h
#define _dataSet_h

#include "defs.h"

#include <iosfwd>
#include <string>
#include <vector>


class graph;

class dataRow {
  public:

   enum edgeType { REGULAR = 0, SOURCE = 1, SINK = 2, ARTIFICIAL = 3, INVALID = 4, ESTIMATED=5 };

  public:
    edge id, cat;
    node from, to;
    char fClass, fClassBACK, multiplicity;
    edgeType typ;
    integer32 lbound, ubound, measured, initialized, computed, treshold;
    float rel;

    dataRow(): id(-1), cat(-1), 
               from(-1), to(-1),
               fClass(-1),fClassBACK(-1), multiplicity(-1),
               typ(INVALID),
               lbound(-1),ubound(-1),measured(-1),initialized(-1),computed(-1),treshold(-1),
               rel(-1) {};

    bool isMeasured() const { return (measured >= 0); };
    bool isInitialized() const { return (initialized >=0); };
    bool isComputed() const { return (computed >= 0 ); }
    bool hasReliability() const { return (rel >= 0 ); };
    bool hasLBound() const { return (lbound >=0 ); };
    bool hasUBound() const { return (ubound >=0 ); };
    bool hasTreshold() const { return (treshold >=0); };
    
    friend std::istream &operator>>(std::istream &IN, dataRow &R);
    friend std::ostream &operator<<(std::ostream &OUT, const dataRow &R);
};

/*
 * NOTE: The format of the file is as follows:
 *       Each line contains information about a single edge.
 *       The following attributes are included on each line 
 *       (in the specified order, separated by a space):
 *
 *        id cat fClass typ multiplicity from to lbound ubound measured initialized computed rel fClassBACK treshold
 *
 *       where -1 in any column (with the exception of cat) signifies undefined and a row
 *       having fClass == -1 && typ == dataRow::INVALID signifies the end of valid data. 
 *
 *       The implementation will, moreover, never write a file which would have the id 
 *	 column negative.
 *
 *       The interpretation of the columns is as follows:
 *		id 		The id of the edge in the internal representation (a nonnegative number)
 *		cat		The link_id of the corresponding link (or -1 in case of artificial edges)
 *		fClass  	The function class of the corresponding link [1..5] (-1 together with typ == 4, signifies the end of valid data )
 *		typ 		The type of the edge (REGULAR = 0, SOURCE = 1, SINK = 2, ARTIFICIAL = 3, INVALID = 4)
 *		multiplicity	The multiplicity of the link ( 1 for regular links, 2 for source/sink links )
 *		from/to		The internal id of the from/to node of the edge
 *		lbound/ubound	The capacity constraints of the given edge
 *		measured	The aadf count for the corresponding link (-1 for links without counts)
 *		initialized	The initial guess for the traffic flow (-1 none available) [result of the init phase]
 *		computed	The computed traffic flow (-1 none available) [result of the compute phase]
 *		rel		The reliability of the given flow estimate (-1 none available) [result of the init phase]
 *		fClassBACK	The backward fclass of ARTIFICIAL links (-1 for other type of links)
 *              treshold        The estimated minimal flow along edge (from outside sources, e.g. POI's, etc.)
 * 
 */
class dataSet { 
  private:
    std::vector<dataRow> rows;
    edge num_of_edges;
    node num_of_nodes;
    
     int linkSurplusTo( edge id, graph *G ) const;
     int linkSurplusFrom( edge id, graph *G ) const;
  public:
    dataSet(): num_of_edges(0), num_of_nodes(0) {};
    void load(const std::string &fname);
    void save(const std::string &fname, bool overwrite = true) const;

    std::vector<dataRow>::const_iterator begin() const { return rows.begin(); };
    std::vector<dataRow>::iterator begin() { return rows.begin(); };
    std::vector<dataRow>::const_iterator end() const { return rows.end(); };
    std::vector<dataRow>::iterator end() { return rows.end(); };

    dataRow &operator[]( const edge id ) { return rows[id]; };
    const dataRow &operator[]( const edge id ) const { return rows[id]; };
    edge size() const { return rows.size(); };
    void resize( const edge newSize );
    
    void summaryTable( const std::string& fName = "", graph* G = NULL ) const;
    void printEdgeID2CATMap( const std::string& fName = "" ) const;

    friend std::ostream &operator<<(std::ostream &OUT, const dataSet &S);
    static void finish( std::ostream &OUT );
    friend std::istream &operator>>(std::istream &IN, dataSet &S );
    edge numOfEdges() const  { return num_of_edges;}
    node numOfNodes() const { return num_of_nodes; };
};

class graph;
class initState;

void collect_save( const std::string& dSet, dataSet* G, graph* iState, initState* init );

#endif // _dataSet_h
