#include "graph.h"
#include "util.h"
#include "zakazy.h"
#include "dataSet.h"

#include <fstream>
#include <iostream>
#include <map>

using namespace std;

graph::graph():
        edgeSize(0), nodeSize(0)
{
}

void graph::resizeNodes( node maxNode ) {
  if ( maxNode >= nodeSize ) {
    mapIN.reserve( maxNode + 1 );
    mapOUT.reserve( maxNode + 1 );
    mapIN.resize( maxNode + 1 );
    mapOUT.resize( maxNode +1 );
    nodeFClassCount.resize( (maxNode+1)*5, 0 );
    nodeSize = maxNode + 1;
  }
}

void graph::resizeEdges( edge maxEdge ) { 
  if ( maxEdge >= edgeSize ) { 
    from.resize( maxEdge + 1 );
    to.resize( maxEdge + 1 );
    edgefClass.resize( maxEdge + 1 );
    prevFClass.resize( maxEdge + 1 );
//    edgeCosts.resize( maxEdge + 1 );
    edgeSize = maxEdge + 1;
  }
}

void graph::addEdge( node From, node To, edge E, int FClass, int PrevFClass ) {
            resizeEdges( E );
	    resizeNodes( To );
	    resizeNodes( From );

            from[E]=From;
            to[E]=To;
            mapIN[To].push_back(E);
            mapOUT[From].push_back(E);
            edgefClass[E]=(char) FClass;
	    if ( PrevFClass >= 0 ) prevFClass[E] = PrevFClass;
	    else prevFClass[E] = FClass;
	    incNodeFClassCount( From, FClass );
        }

edgeList graph::outEdges( node N, int fClass ) const { 
  edgeList ret;
  edgeList allOUT = mapOUT[N];
  for(edgeList::iterator it = allOUT.begin(); it != allOUT.end(); ++it)
    if (getFClass(*it, OUTGOING) == fClass) ret.push_back(*it);
  return ret;
}

edgeList graph::inEdges( node N, int fClass ) const { 
  edgeList ret;
  edgeList allOUT = mapIN[N];
  for(edgeList::iterator it = allOUT.begin(); it != allOUT.end(); ++it)
    if (getFClass(*it, INCOMING ) == fClass) ret.push_back(*it);
  return ret;
}

edgeList graph::nbrEdgesGE( node N, int fClass, dirT direction ) const { 
  edgeList ret;
  edgeList all;
  if ( direction == OUTGOING ) all = mapOUT[N];
  else all = mapIN[N];
  for(edgeList::iterator it = all.begin(); it != all.end(); ++it)
    if (getFClass(*it, direction ) > fClass) ret.push_back(*it);
  return ret;
}

void graph::load( const dataSet &dSet ) {
  resizeEdges( dSet.numOfEdges()-1 );
  resizeNodes( dSet.numOfNodes()-1 );
  for( vector<dataRow>::const_iterator row = dSet.begin(); row != dSet.end(); ++row ) {
    if ( 0 < row->fClass && row->fClass < 6 )
    addEdge( row->from, row->to, row->id, row->fClass, row->fClassBACK );
    else cerr << "Error adding edge " <<endl << *row <<"############"<<endl;
//    setEdgeCost( row->id, row->rel );
  }
}

bool graph::testDeadEnds(dataSet& dSet) const {
  long num_of_dead_ends = 0;
  bool ret = true;
  for( edge E = 0; E < numOfEdges(); ++E ) { 
    edgeList out = outEdges(toNode(E));
    edgeList in = inEdges(fromNode(E));
    if ( out.size() == 0 && dSet[E].typ != dataRow::SINK ) {
      if ( num_of_dead_ends < 100 ) cerr  << "Edge " << E << " (cat: " << dSet[E].cat << ") is a genuine out-dead end." << endl;
      num_of_dead_ends++;
      ret = false;
    } else if ( in.size() == 0 && dSet[E].typ != dataRow::SOURCE ) {
      if ( num_of_dead_ends < 100 ) cerr  << "Edge " << E << " (cat: " << dSet[E].cat << ") is a genuine in-dead end." << endl;
      num_of_dead_ends++;
      ret = false;
    }
  }
  if ( ! ret ) { 
    cerr << "Total # of deadends: " << num_of_dead_ends << endl;
  }
  return ret;
}


void graph::save( dataSet &dSet ) const { 
  dSet.resize( numOfEdges() );
  for( edge E = numOfEdges()-1; E>=0 ;--E ) { 
    dSet[E].from = fromNode( E );
    dSet[E].to = toNode( E );
    dSet[E].fClass = getFClass( E );
    dSet[E].fClassBACK = getFClass( E, BACKWARD );
    if ( dSet[E].typ == dataRow::INVALID ) dSet[E].typ = dataRow::ARTIFICIAL;
  }
}

void graph::printEdges(node N, bool IN ) {
  edgeList lst;
  if ( IN ) lst = inEdges( N );
  else lst = outEdges( N );
  for( edgeList::iterator it = lst.begin(); it != lst.end(); ++it )
    std::cerr<<*it<<",";
  std::cerr<<endl;
}



ostream &operator<<(ostream& out, graph& table) {
  node max = table.numOfNodes();
  for( int i=0; i < max; ++i ) { 
    out << "{";
    edgeList IN = table.inEdges( i );
    for(edgeList::iterator it = IN.begin(); it != IN.end(); ) { 
      out << table.fromNode(*it);
      ++it;
      if ( it != IN.end() ) out <<",";
    }
    out << "} -> " << i << " -> {";
    IN = table.outEdges( i );
    for(edgeList::iterator it = IN.begin(); it != IN.end(); ) { 
      out << table.toNode(*it);
      ++it;
      if ( it != IN.end() ) out <<",";
    }
    out << "}"<<endl;
    out.flush();
  }
  return out;
}

void graph::printGraph() {
  cerr << *this;
}

bool graph::isSingle( edge E, dirT direction ) const {
  int myFclass = getFClass( E, direction );
  node f, t;
  if ( direction == FORWARD ) {
    f = fromNode( E );
    for( edgeList::const_iterator e = mapOUT[f].begin(); e != mapOUT[f].end(); ++e ) {
      if (  *e != E ) {
	t = toNode( *e );
	int myFCcnt = 0;
	for( edgeList::const_iterator i = mapIN[t].begin(); i != mapIN[t].end(); ++i )
	  if ( getFClass( E, BACKWARD ) == myFclass ) {
	    myFCcnt++;
	    i = mapIN[t].end();
	  }
	if ( myFCcnt == 0 ) return true;
      }
    }
    return false;
  } else {
    t = toNode( E );
    for( edgeList::const_iterator e = mapIN[t].begin(); e != mapIN[t].end(); ++e ) {
      if ( *e != E ) {
	f = fromNode( *e );
	int myFCcnt = 0;
	for( edgeList::const_iterator i = mapOUT[f].begin(); i != mapOUT[f].end(); ++i )
	  if ( getFClass( E, FORWARD ) == myFclass ) {
	    myFCcnt++;
	    i = mapOUT[f].end();
	  }
	if ( myFCcnt == 0 ) return true;
      }
    }
    return false;
  }
  return false;
}

void graph::enforceZakazy( zakazy &z ) {
  graph *newG = new graph();
  node maxNode=0;
  edge maxEdge = numOfEdges();
  pair<node,node> edgePair;
  vector< pair<node,node> > edge2Nodes;
  edge2Nodes.resize( numOfEdges() );
  newG->resizeNodes( numOfEdges()*2-1 );
  newG->resizeEdges( numOfEdges()-1 );
  for( edge E = numOfEdges()-1; E >=0 ; --E ) {
    newG->addEdge( maxNode, maxNode+1, E, getFClass( E ) );
    edgePair.first = maxNode++;
    edgePair.second = maxNode++;
    edge2Nodes[E]=edgePair;
  }
  for( edge E = numOfEdges()-1; E>=0; --E ) { 
    edgeList out = outEdges( toNode(E) );
    for( edgeList::iterator F = out.begin(); F != out.end(); ++F ) { 
      if ( out.size() == 1 || z.isAllowed( E, *F ) )
	newG->addEdge( edge2Nodes[E].second, edge2Nodes[*F].first, maxEdge++, getFClass( *F ), getFClass( E ) );
    }
  }
  (*this) = *newG;
  delete newG;
}

void graph::loadZakazy( const string &fName, dataSet *dS ) {
  zakazy z;
  z.load( fName, dS );
  enforceZakazy( z );
}
