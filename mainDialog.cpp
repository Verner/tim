#include "mainDialog.h"
#include "tableDialog.h"

#include <QtCore/QDebug>
#include <QtCore/QProcess>

#include <QtGui/QMessageBox>

#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlError>
#include <QtSql/QSqlQueryModel>

void mainDialog::runInit()
{
  QString timPath = "w64-build/tim.exe";
  QStringList arguments;
  arguments << "-p" << "init" << "-d" << "data/network" << "-z" << "data/zakazy" << "";
  QProcess *initProcess = new QProcess(this);
  initProcess->start(timPath, arguments);
}


void mainDialog::setupTables()
{
  networkTbl = new tableDialog( &db, this, Qt::Dialog, "network" );
  networkTbl->addColumn("link_id","a unique positive id of the link (negative values allowed, but such links will be ignored)");
  networkTbl->addColumn("nodeid_from","the unique id of the from junction");
  networkTbl->addColumn("nodeid_to","the unique id of the to junction");
  networkTbl->addColumn("func_class","an integer >= 1 and <= 5 (other values allowed, but such links will be ignored)");
  networkTbl->addColumn("dir_of_travel","either B, F, T, where B means the link is a both-way link\
					 T means the traffic goes in the direction from_junction ==> to_junction\
					 F means the traffic goes in the direction to_junction ==> from_junction");
  networkTbl->addColumn("residential_traffic","either NULL, or the minimal amount of traffic that should be traveling along\
					       this link (for both-way links, this amount of traffic is ensured in both directions)");
  
  
  
  restrictionsTbl = new tableDialog( &db, this, Qt::Dialog,  "restrictions" );
  restrictionsTbl->addColumn("linkid_from","ID of the from link");
  restrictionsTbl->addColumn("linkid_to","ID of the to link");
  
  aadfTbl = new tableDialog( &db, this, Qt::Dialog, "aadf" );
  aadfTbl->addColumn("link_id","a unique positive id of the link");
  aadfTbl->addColumn("intensity","the aadf count connected to this link or NULL if no such count is available.\
				  If the link is a both-way link, this number shall be interpreted as the\
				  sum of traffic in both directions and will be divided among the two directions.");
}

void mainDialog::selectNetwork()
{
  networkTbl->setModels("tim");
  if ( networkTbl->exec() == QDialog::Accepted ) {
    ui.networkTableEdit->setText( networkTbl->getSchema()+"."+networkTbl->getTable() );
  }
}

void mainDialog::selectAADF()
{
  aadfTbl->setModels("tim");
  if ( aadfTbl->exec() == QDialog::Accepted ) { 
    ui.aadfTableEdit->setText( aadfTbl->getSchema()+"."+aadfTbl->getTable() );
  }
}

void mainDialog::selectRestrictions()
{
  restrictionsTbl->setModels("tim");
  if ( restrictionsTbl->exec() == QDialog::Accepted ) { 
    ui.restrictionsTableEdit->setText( restrictionsTbl->getSchema()+"."+restrictionsTbl->getTable() );
  }
}

void mainDialog::disableSelectTables()
{
  ui.selectNetTableBtn->setDisabled(true);
  ui.selectAADFTableBtn->setDisabled(true);
  ui.selectRestrictTableBtn->setDisabled(true);
}

void mainDialog::enableSelectTables()
{
  ui.selectNetTableBtn->setEnabled(true);
  ui.selectAADFTableBtn->setEnabled(true);
  ui.selectRestrictTableBtn->setEnabled(true);
}



mainDialog::mainDialog(QWidget *parent):
QDialog(parent),
db(QSqlDatabase::addDatabase("QPSQL")),
connected(false)
{ 
  ui.setupUi(this);
  dbModel = new QSqlQueryModel;
  schemaModel = new QSqlQueryModel;
  ui.databaseCombo->setModel( dbModel );
  
  setupTables();
  
  connect( ui.connectDBButton, SIGNAL(clicked()), this, SLOT(connectToDB()) );
  connect( ui.databaseCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(changeDB()) );
  connect( ui.selectNetTableBtn, SIGNAL(clicked(bool)), this, SLOT(selectNetwork()) );
  connect( ui.selectRestrictTableBtn, SIGNAL(clicked(bool)), this, SLOT(selectRestrictions()) );
  connect( ui.selectAADFTableBtn, SIGNAL(clicked()), this, SLOT(selectAADF()) );
  
}

mainDialog::~mainDialog()
{
  if (connected) {
    db.close();
  }
  delete schemaModel;
  delete dbModel;
  delete networkTbl;
  delete restrictionsTbl;
  delete aadfTbl;
}


void mainDialog::connectToDB() {
  if ( connected ) dbase_disconnect(true);
  else dbase_connect();
}


void mainDialog::dbase_disconnect(bool doDisconnect)
{
  dbModel->clear();
  networkTbl->clearModels();
  restrictionsTbl->clearModels();
  aadfTbl->clearModels();
  disableSelectTables();
  connected = false;
  ui.connectDBButton->setText(tr("&Connect"));
  if (doDisconnect) db.close();
  else {
    QSqlError err = db.lastError();
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Warning);
    msgBox.setText(tr("Error connecting to dbase:")+err.driverText());
    msgBox.exec();
  }
}

void mainDialog::dbase_connect()
{
  db.setHostName(ui.hostEdit->text());
  db.setDatabaseName(ui.databaseCombo->currentText());
  db.setUserName(ui.usernameEdit->text());
  db.setPassword(ui.passwordEdit->text());
  if ( db.open() ) { 
    ui.connectDBButton->setText(tr("&Disconnect"));
    dbModel->setQuery("SELECT datname FROM pg_database ORDER BY datname");
    //schemaModel->setQuery("SELECT schema_name FROM information_schema.schemata ORDER BY schema_name");
    ui.databaseCombo->setCurrentIndex(ui.databaseCombo->findText(db.databaseName(),Qt::MatchFixedString));
    enableSelectTables();
    connected = true;
  } else {
    dbase_disconnect(false);
  }
}


void mainDialog::changeDB()
{
  QString dbName = ui.databaseCombo->currentText();
  if ( dbName != db.databaseName() && connected ) {
    dbModel->clear();
    schemaModel->clear();
    dbase_disconnect(true);
    db.setDatabaseName(dbName);
    dbase_connect();
  }
}
