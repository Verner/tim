#include "dataSet.h"

#include <fstream>
#include <sstream>
#include <map>
#include <utility>
#include "graph.h"
#include "initState.h"

using namespace std;

istream &operator>>( istream &IN, dataRow &R ) {
  int fc, fcb, typ, mul;
  IN >> R.id >> R.cat >> fc >> typ >> mul >> R.from >> R.to >> R.lbound >> R.ubound >> R.measured >> R.initialized >> R.computed >> R.rel >> fcb >> R.treshold;
  R.fClass=(char)fc;
  R.typ = (dataRow::edgeType)typ;
  R.multiplicity=(char)mul;
  R.fClassBACK=(char)fcb;
  return IN;
}

ostream &operator<<( ostream &OUT, const dataRow &R) {
  OUT << R.id <<" "<< R.cat <<" "<< (int)R.fClass <<" "<< (int)R.typ <<" "<< (int)R.multiplicity <<" "<< R.from <<" "<< R.to<<" " << R.lbound<<" " << R.ubound <<" "<< R.measured <<" "<< R.initialized <<" "<< R.computed <<" " << R.rel << " " << (int)R.fClassBACK << " "<< R.treshold << endl;
  return OUT;
}

void dataSet::load( const string &fname ) { 
  ifstream IN( fname.c_str() );
  dataRow row;
  node max_node=0;
  unsigned long num_rows = 0;
  while( ! IN.eof() ) { 
    num_rows++;
    IN >> row;
    if ( IN.eof() || ( ! valid_edge(row.cat) && row.typ == dataRow::INVALID ) ) break;
    resize( row.id + 1 );
    rows[row.id] = row;
    max_node = MAX<node>(row.from,row.to);
    num_of_nodes=MAX<node>(max_node,num_of_nodes);
    if ( num_rows % 100000 == 0 ) {
      std::cerr << "*";
    }
  }
  std::cerr << std::endl;
}

void dataSet::save( const std::string& fname, bool overwrite ) const {
  if ( ! overwrite && file_exists( fname ) ) return;
  ofstream OUT( fname.c_str() );
  OUT << *this;
  finish( OUT );
}

void dataSet::finish( ostream &OUT ) { 
  dataRow row;
  //row.cat = -1;
  OUT << row;
}
  
ostream &operator<<( ostream &OUT, const dataSet &S) {
  for( vector<dataRow>::const_iterator row = S.begin(); row != S.end(); ++row ) {
    if ( row->typ != dataRow::INVALID ) OUT << *row;
  }
  return OUT;
}

istream &operator>>( istream &IN, dataSet &S ) { 
  dataRow row;
  node max_node;
  while( IN ) {
    IN >> row;
    if ( valid_edge(row.cat) && row.typ == dataRow::INVALID ) break;
    S.resize( row.id + 1 );
    S.rows[row.id]=row;
    max_node = MAX<node>(row.from,row.to);
    S.num_of_nodes=MAX<node>(max_node,S.num_of_nodes);
  }
  return IN;
}

void dataSet::resize( const edge newSize ) {
  edge oldSize = size();
  if ( newSize < oldSize ) return;
  num_of_edges = newSize;
  rows.resize( newSize );
  for( edge E = oldSize; E < newSize; ++E ) { 
    rows[E].id = E;
  }
}
/*
 * Works for regular links taking advantage
 * of the fact that the target node only has
 * a single incoming edge
 */
int dataSet::linkSurplusTo( edge eID, graph *G ) const {
  if ( ! G ) return -1;
  edgeList outEdges = G->outEdges(G->toNode(eID));
  int surplus = 0;
  for( edgeList::iterator e = outEdges.begin(); e != outEdges.end(); ++e )
    surplus-=rows[*e].computed;
  if ( rows[eID].typ == dataRow::SINK ) surplus-=rows[eID].measured;
  surplus+=rows[eID].computed;
  return surplus;
}
/*
 * Works for regular links taking advantage
 * of the fact that the source node only has
 * a single outgoing edge
 */
int dataSet::linkSurplusFrom( edge eID, graph *G ) const {
  if ( ! G ) return -1;
  edgeList inEdges = G->inEdges(G->fromNode(eID));
  int surplus = 0;
  for( edgeList::iterator e = inEdges.begin(); e != inEdges.end(); ++e )
    surplus+=rows[*e].computed;
  if ( rows[eID].typ == dataRow::SOURCE ) surplus+=rows[eID].measured;
  surplus-=rows[eID].computed;
  return surplus;
}

void dataSet::printEdgeID2CATMap( const string &fName ) const {
  ostream *OUT;
  ofstream ofile;
  if ( fName == "" ) OUT = &std::cout;
  else {
    ofile.open( fName.c_str() );
    OUT = &ofile;
  }
  for(edge id = rows.size()-1;id>=0;--id)
    (*OUT) << id << rows[id].cat << endl;
}

void dataSet::summaryTable( const string &fName, graph *G ) const {
  ostream *OUT;
  ofstream ofile;
  if ( fName == "" ) OUT = &std::cout;
  else {
    ofile.open( fName.c_str() );
    OUT = &ofile;
  }
  multimap<edge,edge> linkID2ID;
  for(edge id = rows.size()-1;id>=0;--id) {
    edge lID = rows[id].cat;
    if ( lID >=0 && rows[id].typ < dataRow::SINK ) 
      linkID2ID.insert( make_pair(lID,id) );
  }
  edge step=0, step_size=linkID2ID.size()/50;
  for( multimap<edge,edge>::iterator link = linkID2ID.begin(); link != linkID2ID.end(); ++link ) {
    step++;
    if ( step % step_size == 0 ) cerr << "*";
    int f_init=-1,b_init=-1,f_flow=-1,b_flow=-1,f_lbound=-1, b_lbound=-1;
    int f_measured=-1,b_measured=-1;
    int f_surplusTarget=-1,b_surplusTarget=-1,f_surplusSource=-1,b_surplusSource=-1;
    float f_rel=-1,b_rel=-1;
    stringstream ss;
    multimap<edge,edge>::iterator next = link;
    next++;
    dataRow r;
    r = rows[link->second];
    f_init = r.initialized;
    f_flow = r.computed;
    f_rel = r.rel;
    f_lbound=r.lbound;
    f_measured=r.measured;
    f_surplusTarget = linkSurplusTo(link->second, G);
    f_surplusSource = linkSurplusFrom(link->second,G);
    ss<<"EID:"<<link->second<<"("<< r.from <<"-->"<<r.to<<")";
    if ( next->first == link->first ) {
      r = rows[next->second];
      b_init = r.initialized;
      b_flow = r.computed;
      b_rel = r.rel;
      b_lbound=r.lbound;
      b_measured=r.measured;
      b_surplusTarget = linkSurplusTo(next->second, G);
      b_surplusSource = linkSurplusFrom(next->second,G);
      ss<<","<<next->second<<"("<< r.from <<"-->"<<r.to<<")";
      while( next->first == link->first ) {
	++link;
	++next;
      }
    };
    (*OUT) << link->first << " " << f_measured <<" "<<b_measured <<" "<< f_init << " " << b_init << " " << f_flow << " " << b_flow << " " << f_rel << " " << b_rel << " " << f_lbound << " " << b_lbound << " ";
    (*OUT) << f_surplusSource << " " << b_surplusSource << " " << f_surplusTarget << " " << b_surplusTarget << " ";
    (*OUT) << (int) r.fClass << " " << ss.str() << endl;
  }
  cerr << endl;
}

void collect_save(const string &out_file,  dataSet *dS, graph *G, initState *init ) {
  ofstream OUT( out_file.c_str() );
  for( edge E = 0; E<dS->size(); ++E ) {
    (*dS)[E].from = G->fromNode(E);
    (*dS)[E].to=G->toNode(E);
  }
  dataRow row;
  row.typ = dataRow::ARTIFICIAL;
  row.multiplicity=1;
  for( edge E = G->numOfEdges()-1; E>= dS->size(); --E ) { 
    row.id = E;
    row.fClass = (char) G->getFClass(E,FORWARD);
    row.fClassBACK = (char) G->getFClass(E, BACKWARD);
    row.from = G->fromNode(E);
    row.to = G->toNode(E);
    if ( init ) {
      row.initialized = (int) init->getFlow(E);
      row.rel = (float) init->getReliability(E);
    }
    OUT << row;
  }
  OUT << *dS;
  dataSet::finish( OUT );
}
