#include "timModel.h"

#include "graph.h"
#include "initState.h"
#include "problem.h"
#include "sourceSinkList.h"
#include "dataSet.h"
#include "translator.h"
#include "global_config.h"

#include <fstream>

using namespace std;

timModel::timModel(): 
  G(NULL), dSet( new dataSet() ), allow_down( false )
{}

void timModel::setControlFile(const string& control_f) {
  control_file = control_f;
}

void timModel::load( const string& data_file, const string& zakazy_file ) {
  delete G;
  dSet->load( data_file );
  cerr << "Constructing the graph ..." << endl;
  G = new graph();
  G->load( *dSet );
  if ( zakazy_file != "" ) setZakazy( zakazy_file );
  G->testDeadEnds( *dSet );
}

void timModel::save( const string &data_file ) const { 
  dSet->save( data_file );
};

void timModel::setZakazy( const string &zakazy_file ) { 
  cerr << "Enforcing the zakazy ... " << endl;
  G->loadZakazy( zakazy_file, dSet );
  //G->save( *dSet );
}

ostream &operator<<( ostream &OUT, const timModel &model ) { 
  OUT << *(model.dSet);
  return OUT;
}

istream &operator>>( istream &IN, timModel &model ) { 
  delete model.G;
  IN >> *(model.dSet);
  model.G = new graph();
  model.G->load( *model.dSet );
  return IN;
}

void timModel::init_phase( const string &out_file) {
  initState *A = new initState(G), *B = new initState(G);
  sourceSinkList *ssList = new sourceSinkList;
  cerr << "Loading source/sinks ... " << endl;
  ssList->load( *dSet );
  cerr << "Distributing sources ... " << endl;
  A->distributeSources( ssList );
  cerr << "Distributing sinks ..." << endl;
  B->distributeSinks( ssList );

  cerr << "Merging values ... " << endl;
  A->mergeState( *B );
  
  cerr << "Ensuring minimal flow ..." << endl;

#ifdef VERSION_1
  A->ensureMinimalFlow();
#else
  A->ensureMinimalFlow(global_config.getNum(RELIABILITY_TRESHOLD_OPTION));
#endif

  cerr << "Checking consistency ..." << endl;
  long severe = A->testInitSinks( ssList, *dSet ) + A->testInitSources( ssList, *dSet );
  if ( severe > 0 ) { 
    cerr << "# of links with addf counts different from init flow: " << severe << endl;
  } else {
    cerr << "Consistent." << endl;
  }

  cerr << "Freeing memory ..." << endl;
  delete B;
  delete ssList;
  cerr << "Updating dataSet ..." << endl;
  A->save( *dSet );
  if ( out_file != "" ) {
    cerr << "Saving to " << out_file<< " ..." << endl;
    collect_save( out_file, dSet, G, A );
  }
  delete A;
}

void timModel::compute_phase(long long int epsilon, long long dumpInterval, const string& dump_prefix) {
  problem prob;
  prob.load( *dSet, G, control_file );
  prob.setDump( dSet, dump_prefix, dumpInterval );
  prob.solve( (real) epsilon, allow_down );
  prob.save( *dSet );
}

void timModel::restartFromCheckPoint( const string &check_point_file, long long dumpInterval, const string &dump_prefix ) {
  delete G;
  ifstream IN( check_point_file.c_str() );
  cerr << "Restarting from checkpoint ("<<check_point_file<<")."<<endl;
  cerr << "Loading edge data from checkpoint file ...";
  IN >> (*dSet);
  cerr << "OK" << endl;
  cerr << "Constructing the graph ...";
  G = new graph();
  G->load( *dSet );
  cerr << "OK" << endl;
  problem prob;
  prob.load( *dSet, G );
  prob.loadCheckPoint( dSet, IN, dumpInterval, dump_prefix );
  prob.solve();
  prob.save( *dSet );
}
  
void timModel::update( const string &old_data ) {
  edge oldEdge;
  dataSet oldSet;
  oldSet.load(old_data);
  translator tr( oldSet, *dSet );
  for(vector<dataRow>::iterator row = dSet->begin(); row != dSet->end(); ++row ) {
    oldEdge = tr[row->id];
    if ( oldEdge>=0 && FP_EQ(row->initialized,oldSet[oldEdge].initialized) ) {
      row->initialized = oldSet[oldEdge].computed;
    }
  }
}
