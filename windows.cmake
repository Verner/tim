# the name of the target operating system
SET(CMAKE_SYSTEM_NAME Windows)
SET(DRIVE_C /home/jonathan/.wine/drive_c)
SET(QT_VERSION 4.7.4)
SET(QT_PREFIX ${DRIVE_C}/Qt/${QT_VERSION})


if(${TARGET} MATCHES "64")
  set(COMPILER_PREFIX x86_64)
else(${TARGET} MATCHES "64")
  set(COMPILER_PREFIX i686)
endif(${TARGET} MATCHES "64")

# which compilers to use for C and C++
SET(CMAKE_C_COMPILER ${COMPILER_PREFIX}-w64-mingw32-gcc)
SET(CMAKE_CXX_COMPILER ${COMPILER_PREFIX}-w64-mingw32-g++)
SET(CMAKE_RC_COMPILER ${COMPILER_PREFIX}-w64-mingw32-windres)

# here is the target environment located
SET(CMAKE_FIND_ROOT_PATH  /usr/${COMPILER_PREFIX}-w64-mingw32 ${DRIVE_C} ${QT_PREFIX} )


SET(CMAKE_SHARED_LIBRARY_LINK_C_FLAGS "")

# adjust the default behaviour of the FIND_XXX() commands:
# search headers and libraries in the target environment, search 
# programs in the host environment
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

# FindQt4.cmake querys qmake to get information, this doesn't work when crosscompiling
set(QT_HOST_DIR /usr/share/qt4)
set(QT_DIR ${DRIVE_C}/Qt/4.8.0)
set(QT_BINARY_DIR   ${QT_HOST_DIR}/bin)
set(QT_LIBRARY_DIR  ${QT_DIR}/lib)
set(QT_INCLUDE_DIR  ${QT_DIR}/include)
set(QT_QTCORE_LIBRARY   ${QT_LIBRARY_DIR}/libQtCore4.a)
set(QT_QTCORE_INCLUDE_DIR ${QT_INCLUDE_DIR}/QtCore)
set(QT_MKSPECS_DIR  ${QT_DIR}/mkspecs)
set(QT_MOC_EXECUTABLE  ${QT_BINARY_DIR}/moc -DQ_WS_WIN)
set(QT_QMAKE_EXECUTABLE  ${QT_BINARY_DIR}/qmake)
set(QT_UIC_EXECUTABLE  ${QT_BINARY_DIR}/uic)
set(QT_RCC_EXECUTABLE  ${QT_BINARY_DIR}/rcc)


# the following is needed, don't ask me why
set(QT_QTCORE_INCLUDE_DIR ${QT_INCLUDE_DIR}/QtCore)
set(QT_QTGUI_INCLUDE_DIR ${QT_INCLUDE_DIR}/QtGui)
set(QT_QTSQL_INCLUDE_DIR ${QT_INCLUDE_DIR}/QtSql)
set(QT_QTNETWORK_INCLUDE_DIR ${QT_INCLUDE_DIR}/QtNetwork)
LINK_DIRECTORIES(${QT_LIBRARY_DIR})
set(QT_QTNETWORK_LIBRARIES QtNetwork4)
set(QT_QTCORE_LIBRARIES QtCore4)
set(QT_QTGUI_LIBRARIES QtGui4)
set(QT_QTSQL_LIBRARIES QtSql)
