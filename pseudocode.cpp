/*#include <list>
#include <vector>
#include <fstream>

#include "util.h"
#include "minCost.h"








edgeTable graph;
costTable<costFunction> cost;
std::vector<real> flowVector;
std::vector<real> priceVector;
std::vector<real> nodeSurplus;
std::list<node> activeNodes;
real epsilon;

void initActiveNodes() {
    node max = graph.numOfNodes();
    for( node i = 0; i< max; i++ )
        if ( nodeSurplus[i] != 0 ) activeNodes.push_back( i );
}

void distributeFlow( edge e, real flow, int maxDistance = 10 ) {
    node activeNode = graph.toNode(e);
    edge activeEdge;
    edgeList out = graph.outEdges( activeNode );
    if ( maxDistance <= 0 ) {
        nodeSurplus[ activeNode ]+=flow;
        return;
    }
    for( edgeList::iterator it = out.begin(); it != out.end(); ++it ) {
        activeEdge = *it;
        real flowPortion = graph.flowPortion( activeEdge, flow );
        flowVector[ activeEdge ] += flowPortion;
        distributeFlow( activeEdge, flowPortion, maxDistance-1 );
    }
}

void initFlowVector( std::string measureFile ) {
    edge maxE = graph.numOfEdges(), maxN = graph.numOfNodes();
    flowVector.resize( maxE, 0 );
    nodeSurplus.resize( maxN, 0 );
    std::ifstream input( measureFile.c_str() );
    int treshold = 10;
    std::string edg, flow;
    edge e;
    real f;
    while ( ! input.eof() ) {
        input >> edg >> flow;
        e = str2edge( edg );
        f = str2edge( flow );
        distributeFlow( e, f, 10 );
    }
};

void initCosts() {
    edge max = graph.numOfEdges();
    cost.costFuncs.resize(max);
    for(edge i = 0; i < max; ++i) {
        costFunction func( flowVector[ i ], 1 );
        cost.costFuncs[i]= func;
    }
};

void initPriceVector() {
    node max = graph.numOfNodes();
    priceVector.resize(max,0);
    for(node i = 0; i<max; ++i ) {
        priceVector[i]=0;
    }
};

void initialize() {
    graph.loadFromFile( "graph.dat" );
    initFlowVector( "measurement.dat" );
    initCosts();
    initPriceVector();
    initActiveNodes();
};



real computeMargin( node A, edge E ) {
    node from = graph.fromNode(E), to = graph.fromNode(E);
    if ( from == A ) {
        return cost.computeMarginOUT( E, priceVector[A], priceVector[to], flowVector[E] );
    } else {
        return cost.computeMarginIN( E, priceVector[A], priceVector[from], flowVector[E] );
    }
}


edgeList constructPushList( node N ) {
    edgeList ret;
    edgeList in = graph.inEdges( N ), out = graph.outEdges( N );
    real priceN = priceVector[N];
    for( edgeList::iterator it = in.begin(); it != in.end(); ++it ) {
        edge curE = *it;
        if ( cost.unblockedIN( curE, priceN, priceVector[ graph.fromNode( curE ) ], flowVector[ curE ], epsilon ) )
            ret.push_back( curE );
    }
    for( edgeList::iterator it = out.begin(); it != out.end(); ++it ) {
        edge curE = *it;
        if ( cost.unblockedOUT( curE, priceN, priceVector[ graph.toNode( curE ) ], flowVector[ curE ], epsilon ) )
            ret.push_back( curE );
    }
}





void pushFlow( node N, edge E, real dF ) {
    node fromNode = graph.fromNode( E );
    node target;
    if ( fromNode == N ) {
        nodeSurplus[ N ] -= dF;
        target = graph.toNode( E );
        if ( nodeSurplus[ target ] == 0 ) activeNodes.push_back( target );
        nodeSurplus[ target ] += dF;
        flowVector[ E ] += dF;
    } else {
        nodeSurplus[ N ] -= dF;
        if ( nodeSurplus[ fromNode ] == 0 ) activeNodes.push_back( fromNode );
        nodeSurplus[ fromNode ] += dF;
        flowVector[ E ] -= dF;
    }
}

void increasePrice( node N ) {
    edgeList out = graph.outEdges( N ), in = graph.inEdges( N );
    real increase = 0;
    real priceN = priceVector[ N ];
    edge curE;
    if ( in.size() > 0 ) {
        edgeList::iterator it = in.begin();
        increase = cost.maxAllowablePriceIncreaseTo( *it, priceVector[graph.fromNode(*it)], priceN, flowVector[*it], epsilon );
        for( ++it; it != in.end(); ++it ) {
            curE = *it;
            increase = MIN( increase, cost.maxAllowablePriceIncreaseTo( curE, priceVector[graph.fromNode(curE)], priceN, flowVector[curE], epsilon ) );
        }
        for( it = out.begin(); it != out.end(); ++it ) {
            curE = *it;
            increase = MIN( increase, cost.maxAllowablePriceIncreaseFrom( curE, priceN, priceVector[graph.toNode(curE)], flowVector[curE], epsilon ) );
        }
    } else if ( out.size() > 0 ) {
        edgeList::iterator it = out.begin();
        increase, cost.maxAllowablePriceIncreaseFrom( *it, priceN, priceVector[graph.toNode(*it)], flowVector[*it], epsilon );
        for( ++it ; it != out.end(); ++it ) {
            curE = *it;
            increase = MIN( increase, cost.maxAllowablePriceIncreaseFrom( curE, priceN, priceVector[graph.toNode(curE)], flowVector[curE], epsilon ) );
        }
    }
    priceVector[N]+=increase;
};

void doIteration() {
    node active = activeNodes.front();
    activeNodes.pop_front();
    real surplus = nodeSurplus[active];
    if ( surplus == 0 ) return;
    edgeList pushList = constructPushList( active );
    while( pushList.size() > 0 ) {
        edge e = pushList.front();
        pushList.pop_front();
        real margin = computeMargin( active, e );
        real delta = MIN( margin, surplus );
        pushFlow( active, e, delta );
        surplus -= delta;
        if ( surplus == 0 ) return;
    }
    increasePrice( active );
    activeNodes.push_back( active );
    return;
}



int main() {
    epsilon = 1;
    initialize();
    while( activeNodes.size() > 0 )
        doIteration();
}



*/
