#ifndef _costFunction_H
#define _costFunction_H

/***************************************************************
 * costFunction.h
 * @Author:      Jonathan Verner (jonathan.verner@matfyz.cz)
 * @License:     GPL v2.0 or later
 * @Created:     2009-05-28.
 * @Last Change: 2009-05-28.
 * @Revision:    0.0
 * Description:
 * Usage:
 * TODO:
 *CHANGES:
 ***************************************************************/
#include "defs.h"
#include "util.h"

#include <iosfwd>

class costFunction { 
	private:
		bigreal minCostFlow;
		bigreal factor;
		bigreal lBound, uBound;
		static bigreal K;
	public:
	  costFunction(): minCostFlow(0),factor(0), lBound(0),uBound(1000000) {};
	  static void setAvoidZero( bigreal factor ) { K = factor; };
	  void setMinCostFlow( const bigreal mcf ) { 
	    minCostFlow = mcf; 
	    if ( FP_LEQ(mcf,lBound) ) lBound=mcf/2; 
	    if ( FP_LEQ(uBound,mcf) ) uBound=mcf*2;
	  };
	  bigreal getUBound() const { return uBound;};
	  bigreal getLBound() const { return lBound;};
	  bigreal getMCF() const { return minCostFlow;};
	  void setFactor( const bigreal fc ) { factor = fc; };
	  void setBounds( const bigreal lbound, const bigreal ubound ) { lBound = lbound, uBound = ubound; };
	  void setLBound( const bigreal lbound ) { lBound = lbound; };
	  void setUBound( const bigreal ubound ) { uBound = ubound; };

		
		/* Cost computing functions */
		
		/* Returns the cost and derivative of the cost respectively of the edge E
		 * at flow(E). The cost function is given by:
		 * 
		 *            / factor_E*(flow-minCostFlow)^2 + K*(flow-lbound)^2; flow \in (-oo, lbound]
		 *  c_E(flow) =  factor_E*(flow-minCostFlow)^2 +      0          ; flow \in [lbound,ubound]
		 *            \ factor_E*(flow-minCostFlow)^2 + K*(flow-ubound)^2; flow \in [ubound, oo)
		 *
		 * so the derivative dC_E(flow) is given by
		 *
		 *               / 2*factor_E*(flow-minCostFlow) + 2*K*(flow-lbound); flow \in (-oo, lbound]
		 *  dC_E(flow) =    2*factor_E*(flow-minCostFlow) +       0         ; flow \in [lbound,ubound]
		 *               \ 2*factor_E*(flow-minCostFlow) + 2*K(flow-ubound) ; flow \in [ubound, oo)
		 *
		 * where the third case comes into play only if ubound != -1.
		 */
  private:
		bigreal sf( const bigreal flow, const bigreal bound ) const { return 2*K*(flow-bound); };
    		bigreal dC( const bigreal flow ) const { return 2*factor*(flow-minCostFlow);};
  public:
		bigreal dlC( const bigreal flow ) const {
		  if ( flow < lBound ) return ( dC( flow ) + sf( flow, lBound ) );
		  else if ( flow > uBound ) return ( dC(flow ) + sf(flow, uBound ) );
		  return dC(flow);
		};
		
		bigreal drC( const bigreal flow ) const { 
		  return dlC( flow );
		};
		
		
		bigreal C( const bigreal flow ) const { 
		  bigreal x = flow-minCostFlow;
		  bigreal y;
		  if ( flow < lBound ) { 
		    y = flow-lBound;
		    return (factor*x*x + K*y*y);
		  } else if ( flow > uBound ) {
		    y = flow-uBound;
		    return (factor*x*x + K*y*y);
		  }
		  return factor*x*x;
		};
		
		
		/* SOLVERS FOR EQUATIONS NEEDED BY THE ALGORITHM 
		 * ALL ARE CONST FUNCTIONS
		 */
		
	

		/* Returns the maximum POSITIVE delta by which flow  
		 * along edge E: N --> M can be increased while maintaining p(N)-p(M) >= drC
		 * NOTE: This assumes p(N)-p(M) >= drC+epsilon/2
		 * USED: During the flow push operation on unblocked arcs (see step 2., p. 8.)
		 * More specifically:
		 *
		 * Returns delta such that
		 *
		 *    p(N) - p(M) = drC_E(flowE+delta)
		 *
		 */
		bigreal _delta_OutOfBound_Help( bigreal dP, bigreal edgeFlow, bigreal bound ) const { 
		  bigreal FC = factor + K;
		  return ( dP/(2*FC) + (factor*(minCostFlow-edgeFlow)+K*(bound-edgeFlow))/FC );
		}
		bigreal solveDeltaOUT( const bigreal priceFrom, const bigreal priceTo, const bigreal edgeFlow ) const {
		  bigreal dP = priceFrom - priceTo;
		  bigreal candidate;
		  if ( FP_LEQ( uBound, edgeFlow ) ) return _delta_OutOfBound_Help( dP, edgeFlow, uBound );
		  if ( FP_SLE( edgeFlow, lBound ) ) {
		    candidate = _delta_OutOfBound_Help( dP, edgeFlow, lBound );
		    if ( FP_SLE(lBound, edgeFlow + candidate) ) return (lBound-edgeFlow)+solveDeltaOUT( priceFrom, priceTo, lBound );
		    else return candidate;
		  }
		  candidate = ( dP / (2*factor) - edgeFlow + minCostFlow );
		  if ( FP_SLE(uBound, edgeFlow + candidate ) ) return (uBound-edgeFlow)+_delta_OutOfBound_Help(dP,uBound,uBound);
		  else return candidate;
		}

		
		/* Returns the maximum POSITIVE delta by which flow  
		 * along edge E: M --> N can be decreased while maintaining p(N)-p(M)<=dlC. 
		 * NOTE: This assumes p(N)-p(M) <= dlC-epsilon/2
		 * USED: During the flow push operation on unblocked arcs (see step 2., p. 8.)
		 * More specifically:
		 *
		 * Returns delta such that
		 *
		 *    p(M) - p(N) = dlC_E(flowE-delta)
		 *
		 */
		
		bigreal solveDeltaIN( const bigreal priceFrom, const bigreal priceTo, const bigreal edgeFlow ) const {
		  bigreal dP = priceFrom - priceTo;
		  bigreal candidate;
		  if ( FP_LEQ(edgeFlow, lBound) ) return -_delta_OutOfBound_Help( dP, edgeFlow, lBound );
		  if ( FP_SLE( uBound, edgeFlow ) ) { 
		    candidate =-_delta_OutOfBound_Help( dP, edgeFlow, uBound );
		    if ( FP_SLE(edgeFlow-candidate, uBound) ) return ( (edgeFlow-uBound) + solveDeltaIN( priceFrom, priceTo, uBound ) );
		    else return candidate;
		  }
		  candidate =-( dP / (2*factor) - edgeFlow + minCostFlow ); 
		  if ( FP_SLE(edgeFlow-candidate, lBound) ) return (edgeFlow-lBound) -_delta_OutOfBound_Help( dP, lBound, lBound );
		  else return candidate;
		}

		
		/* Returns the maximum POSITIVE delta by which price
		 * of node N may be increased so as to maintain e-CS
		 * along edge E: N --> M
		 * USED: During the price rise operation on active arcs (see step 3., p. 8.)
		 * More specifically:
		 *
		 * Returns delta, such that
		 *
		 *    (p(N)+delta) - p(M) = dCostE(flow_E) + epsilon,
		 *
		 */

		bigreal solveIncreaseOUT( const bigreal priceFrom, const bigreal priceTo, const bigreal flow, const bigreal epsilon ) const { 
		  // dC_E( flow ) = 2*factor_E( flow - minCostFlow_E ) 
		  return drC(flow) + epsilon + priceTo - priceFrom;
		}
		
		/* Returns the maximum POSITIVE delta by which price
		 * of node N may be increased so as to maintain e-CS
		 * along edge E: M --> N
		 * USED: During the price rise operation on active arcs (see step 3., p. 8.)
		 * More specifically:
		 *
		 * Returns the delta, such that
		 *
		 *   dCost_E(flow)-epsilon = p(M) - (p(N) + delta) 
		 *
		 */
		bigreal solveIncreaseIN( const bigreal priceFrom, const bigreal priceTo, const bigreal flow, const bigreal epsilon ) const { 
		  // dC_E( flow ) = 2*factor_E( flow - minCostFlow_E ) 
		  return - (dlC(flow)-epsilon-priceFrom + priceTo);
		}

		/* Returns the maximum POSITIVE delta by which price
		 * of node N may be decreased so as to maintain e-CS
		 * along edge E: M --> N
		 * USED: During the price drop operation on active arcs (see [a variation on] step 3., p. 8.)
		 * More specifically:
		 *
		 * Returns the delta, such that
		 *
		 *   dCost_E(flow)+epsilon = p(M) - (p(N) - delta) 
		 *
		 */
		bigreal solveDecreaseIN( const bigreal priceFrom, const bigreal priceTo, const bigreal flow, const bigreal epsilon ) const {
		  return drC(flow)+epsilon-priceFrom + priceTo;
		}
		
		/* Returns the maximum POSITIVE delta by which price
		 * of node N may be decreased so as to maintain e-CS
		 * along edge E: N --> M
		 * USED: During the price drop operation on active arcs (see [a variation on] step 3., p. 8.)
		 * More specifically:
		 *
		 * Returns delta, such that
		 *
		 *    (p(N)+delta) - p(M) = dCostE(flow_E) - epsilon,
		 *
		 */

		bigreal solveDecreaseOUT( const bigreal priceFrom, const bigreal priceTo, const bigreal flow, const bigreal epsilon ) const { 
		  return priceFrom-priceTo +epsilon -dlC(flow);
		}
		
		friend std::ostream &operator<<(std::ostream &OUT, const costFunction &cf );
		
};


#endif /* _costFunction_H */
