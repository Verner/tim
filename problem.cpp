/***************************************************************
 * problem.cpp
 * @Author:      Jonathan Verner (jonathan.verner@matfyz.cz)
 * @License:     GPL v2.0 or later
 * @Created:     2009-05-18.
 * @Last Change: 2009-05-18.
 * @Revision:    0.0
 * Description: The references point to 
 *
 *                 Bertsekas, Polymenakos, Tseng: AN C-RELAXATION METHOD FOR SEPARABLE CONVEX COST NETWORK FLOW PROBLEMS, Siam J. of Optimization, Apr. 1995
 *
 *              where the algorithm is described and its correctness is proved.
 * Usage:
 * TODO:
 *CHANGES:
 ***************************************************************/

#include <list>
#include <vector>
#include <fstream>
#include <iostream>
#include <sstream>


#include "defs.h"
#include "util.h"

#include "problem.h"
#include "graph.h"
#include "problemState.h"

#include "dataSet.h"

#include "global_config.h"

using namespace std;

/* 
 * Returns the list of edges E: M --> N and F: N --> O
 * such that 
 *
 *     drC(flow_F) + epsilon/2 < p(N) - p(O) <= drC(flow_F) + epsilon
 *
 * or
 *     
 *     dlC(flow_E)-epsilon <= p(M) - p(N) < dlC(flow_E)-epsilon/2
 *
 */

edgeList problem::getPushList( node N ) const {
  edgeList ret, IN = G->inEdges( N ), OUT = G->outEdges( N );
  ret.clear();
  real priceN = state->getPrice( N );

  /* Add outgoing edges F: N --> O */
  for( edgeList::iterator F = OUT.begin(); F != OUT.end(); ++F ) { 
    bigreal dPrice = priceN - state->getPrice( G->toNode(*F) );
    bigreal dFlowF  = state->drC( *F );
    #ifdef D_ALG
    if ( ! FP_LEQ(dPrice, dFlowF+epsilon) ) {
      cerr<<"dPrice="<<dPrice << ";dFlow="<<dFlowF<<endl;
      state->printNode(N);
    }
    assert( FP_LEQ( dPrice, dFlowF + epsilon ) );
    #endif
    if ( FP_LE( dFlowF+epsilon/2, dPrice ) ) ret.push_back( *F );
  }

  /* Add incoming edges E: M --> N */
  for( edgeList::iterator E = IN.begin(); E != IN.end(); ++E ) { 
    bigreal dPrice = state->getPrice( G->fromNode(*E) ) - priceN;
    bigreal dFlowF  = state->dlC( *E );
    #ifdef D_ALG
    if ( ! FP_LEQ(dFlowF-epsilon, dPrice) ) {
      cerr<<"dPrice="<<dPrice << ";dFlow="<<dFlowF<<endl;
      state->printNode(N);
    }
    assert( FP_LEQ(dFlowF - epsilon, dPrice) );
    #endif
    if ( FP_LE( dPrice, dFlowF - epsilon/2) ) ret.push_back( *E );
  }

  return ret;

}

edgeList problem::getPushList( node N, OPERATION_TYPE op ) const {
  if ( op == UP ) return getPushList( N );
  edgeList ret, IN = G->inEdges( N ), OUT = G->outEdges( N );
  ret.clear();
  bigreal priceN = state->getPrice( N );

  /* Add outgoing edges F: N --> O */
  for( edgeList::iterator F = OUT.begin(); F != OUT.end(); ++F ) { 
    bigreal dPrice = priceN - state->getPrice( G->toNode(*F) );
    bigreal dFlowF  = state->dlC( *F );
    #ifdef D_ALG
      if ( ! FP_LEQ(dFlowF-epsilon, dPrice) ) {
      cerr<<"dPrice="<<dPrice << ";dFlow="<<dFlowF<<endl;
      state->printNode(N);
    }
    assert( FP_LEQ( dFlowF - epsilon, dPrice ) );
    #endif
    if ( FP_LE( dPrice, dFlowF-epsilon/2 ) ) ret.push_back( *F );
  }

  /* Add incoming edges E: M --> N */
  for( edgeList::iterator E = IN.begin(); E != IN.end(); ++E ) { 
    bigreal dPrice = state->getPrice( G->fromNode(*E) ) - priceN;
    bigreal dFlowF  = state->drC( *E );
    #ifdef D_ALG
    if ( ! FP_LEQ(dPrice, dFlowF + epsilon) ) { 
      cerr << " dPrice=" << dPrice << " dFlowF=" << dFlowF <<" epsilon=" << epsilon << endl;
      state->printNode(N);
      state->printNode(G->fromNode(*E));
    }
    assert( FP_LEQ(dPrice, dFlowF + epsilon) );
    #endif
    if ( FP_LE( dFlowF + epsilon/2, dPrice ) ) ret.push_back( *E );
  }
  return ret;

}

/* 
 * Performs a delta-flow push increasing flow along E: N --> M such 
 * that
 *
 *    p(N) - p(M) = dC(flowE+delta)
 *
 * or a delta-flow push decreasing flow along E: M --> N such that
 *
 *    p(M) - p(N) = dC(flowE-delta)
 *
 */ 

void problem::pushFlow( node N, edge E ) { 
  bigreal delta = state->solveDelta( N, E );
  delta = MIN( state->getSurplus(N), delta );
  #ifdef D_ALG
  assert( delta > 0 );
  #endif
  if ( G->fromNode(E) == N ) { // E: N --> M 
    state->increaseFlow( E, delta );
  } else { // E: M --> N
    state->decreaseFlow( E, delta );
  }
}

void problem::pushFlow( node N, edge E, OPERATION_TYPE op ) { 
  if ( op == UP ) return pushFlow( N, E );
  bigreal delta = state->solveDeltaDOWN( N, E );
  delta = MIN( -state->getSurplus(N), delta );
  #ifdef D_ALG
  assert( delta > 0 );
  #endif
  if ( G->fromNode(E) == N ) { // E: N --> M
    state->decreaseFlow( E, delta );
  } else { // E: M --> N
    state->increaseFlow( E, delta );
  }
}

/*
 * Increases the price of node N by the maximum
 * delta, such that for each edge E: M --> N
 * we have
 *
 *   dCost_E(flow)-epsilon <= p(M) - (p(N) + delta) <= dCost_E(flow_E)+epsilon
 *
 * and for each edge E: N --> M we have
 *
 *   dCost_E(flow)-epsilon <= (p(N)+delta) - p(M) <= dCostE(flow_E) + epsilon,
 *
 * i.e. the delta is the maximum price increase
 * of node N which still satisfies the epsilon-CS 
 * conditions.
 * NOTE: We assume the graph has no nodes of order 0
 */

void problem::increasePrice( node N ) {
  edgeList OUT = G->outEdges( N ), IN = G->inEdges( N );
  bigreal inc;
  edgeList::iterator E;
#ifdef D_ALG
  edgeList push = getPushList( N );
  assert( push.size() == 0 );
#endif
  
  if ( IN.size() > 0 )  {
    // E: M --> N
    E = IN.begin();
    inc = state->solveIncrease( N, *E );
    ++E;
    for( ; E != IN.end(); ++E )
      inc = MIN( inc, state->solveIncrease( N, *E ) );
    // E: N --> M
    for( E = OUT.begin(); E != OUT.end(); ++E )
      inc = MIN( inc, state->solveIncrease( N, *E ) );
  } else {
    // E: N --> M
    E = OUT.begin();
    inc = state->solveIncrease( N, *E );
    ++E;
    for( ; E != OUT.end(); ++E )
      inc = MIN( inc, state->solveIncrease( N, *E ) );
  }

#ifdef D_ALG
  if ( inc < epsilon/2 ) {
    state->printNode( N );
    std::cerr<<"inc = "<<inc<<"; epsilon/2 = "<<epsilon/2<<";\n";
  }
  assert( FP_LEQ(epsilon/2,inc) ); // As per proposition 2., p. 8
#endif
  state->increasePrice( N, inc );
}

void problem::decreasePrice( node N ) {
  edgeList OUT = G->outEdges( N ), IN = G->inEdges( N );
  bigreal inc;
  edgeList::iterator E;
#ifdef D_OPT
  edgeList push = getPushList( N, DOWN );
  assert( push.size() == 0 );
#endif
  
  if ( IN.size() > 0 )  {
    // E: M --> N
    E = IN.begin();
    inc = state->solveDecrease( N, *E );
    ++E;
    for( ; E != IN.end(); ++E )
      inc = MIN( inc, state->solveDecrease( N, *E ) );
    // E: N --> M
    for( E = OUT.begin(); E != OUT.end(); ++E )
      inc = MIN( inc, state->solveDecrease( N, *E ) );
  } else {
    // E: N --> M
    E = OUT.begin();
    inc = state->solveDecrease( N, *E );
    ++E;
    for( ; E != OUT.end(); ++E )
      inc = MIN( inc, state->solveDecrease( N, *E ) );
  }
#ifdef D_ALG
  if ( inc < epsilon/2 ) {
    state->printNode( N );
    std::cerr<<"inc = "<<inc<<"; epsilon/2 = "<<epsilon/2<<";\n";
    std::cerr<<"Increase price of "<<N<<" by "<<inc<<"\n";
  }
  assert( FP_LEQ(epsilon/2,inc) ); // As per proposition 2., p. 8
#endif
  state->increasePrice( N, -inc );
}

/*
 * Takes care of step 2., p. 8.
 */

void problem::processActiveNode( node N ) {
  edgeList pushList;
  edge E;
  if ( (! allow_price_drop) || FP_SLE(0,state->getSurplus(N)) ) {
    pushList = getPushList( N );
    while( pushList.size() > 0 ) {
      E = pushList.back();
      pushList.pop_back();
      pushFlow( N, E );
      if ( FP_LEQ(state->getSurplus( N ), 0) ) return; // See step 2. p. 8
    }
    increasePrice( N );
  } else {
    pushList = getPushList( N, DOWN );
    while( pushList.size() > 0 ) {
      E = pushList.back();
      pushList.pop_back();
      pushFlow( N, E, DOWN );
      if ( ! FP_SLE(state->getSurplus( N ), 0) ) return; // See step 2. p. 8
    }
    decreasePrice( N );
  }
}

/*
 * Iterate through steps 1., 2. 3. of the
 * method (see p. 8)
 */
void problem::solve( real Epsilon, bool allowDROP ) {
  if ( Epsilon > 0 ) epsilon = Epsilon;
  allow_price_drop = ( allow_price_drop || allowDROP );
  cerr << "Solving with epsilon = " << epsilon << endl;
  state->setEpsilon( epsilon );
  if ( allow_price_drop ) state->allowPriceDrop();
  state->initActiveNodes();
  state->incIteration();
  node active;
  while( state->haveActiveNodes() ) {
    if ( state->getIteration() % dump_interval == 0 ) dump();
    if ( state->getIteration() % 10000000 == 0 ) statusOutput( cerr );
    active = state->selectActiveNode(); // Step 1.
    if ( active < 0 ) return;
    processActiveNode( active ); // Steps 2. & 3.
    state->returnActiveNode( active );
    state->incIteration();
  }
}

void problem::loadCheckPoint( dataSet *dS, istream &IN, long long dumpInterval, const string &d_prefix ) {
  dSet = dS;
  cerr << "Reading state checkpoint ...";
  state->checkPointRestart( IN );
  cerr << "OK" << endl;
  int apd;
  IN >> dump_interval >> dumpBaseName >> apd;
  allow_price_drop = apd;
  if ( dumpInterval != -1 ) dump_interval = dumpInterval;
  if ( d_prefix != "" ) dumpBaseName = d_prefix;
  epsilon = state->getEpsilon();
}

void problem::load( const dataSet& dS, graph* Gr, const string control_fname ) {
  controlFileName = control_fname;
  epsilon = 1;
  allow_price_drop = false;
  state = new problemState();
  cerr << "Initializing state ... ";
  state->load( dS, Gr );
  cerr << "OK" << endl;
  G = state->getGraph();
}

void problem::save( dataSet &dS ) const { 
  state->save( dS );
}


bool problem::checkSolution() const {
  bool ret = true;
  for(node N = G->numOfNodes()-1; N>=0; --N) {
    if ( FP_SLE( 0, state->calculateNodeSurplus( N ) ) ) { 
      #ifdef DEBUG
      ret = false;
      cerr << "Node: "<< N <<"; Surplus: "<<state->getSurplus( N ) << endl;
      #else
      return false;
      #endif
    }
  }
  return ret;
}


void problem::setDump( dataSet *dS, std::string fname, long long interval ) { 
  dump_interval = interval;
  dumpBaseName = fname;
  dSet = dS;
}

/*
 * FORMAT OF THE CHECKPOINT FILE:
 *	1. dataSet
 *		edges ...
 *		-1 ... 4 ...
 *	2. problemState
 *		flowVector
 *		priceVector
 *		epsilon iteration
 *	3. dump_interval dumpBaseName allow_price_drop
 */
void problem::dump() {
  if ( global_config.haveKey("create_dumps") && ! global_config.getBool("create_dumps") ) return;
  cerr << "Checkpointing ... ";
  string fn = dumpBaseName+"."+number2Str<long long>(state->getIteration()),fnCHK=fn+".chk";
  ofstream OUT( fnCHK.c_str() );
  save( *dSet );
  OUT << (*dSet);
  state->checkPointSave( OUT );
  OUT << dump_interval << " "<< dumpBaseName << " " << (int) allow_price_drop << endl;
  OUT.close();
  cerr << "OK" << endl;
  statusOutput( cerr );
}

void problem::surplusDump() const {
  stringstream s;
  s << "surplus-" << state->getIteration() <<".dat";
  ofstream splus(s.str().c_str());
  for( node N = G->numOfNodes()-1; N>=0; --N ) { 
    splus << N << " " << ABS<real>(state->getSurplus( N )) << endl;
  }
  splus.close();
}

void problem::statusOutput( std::ostream &OUT ) { 
  problemStats stats;
  state->calculateStats( stats );
  OUT << stats;
  ifstream IN( controlFileName.c_str() );
  if ( IN.is_open() ) {
    node activeSize;
    int allowPriceDrop;
    int dumpGraph;
    long long int cumSurplus;
    string dumpFileName;
    IN >> dumpGraph;
    IN >> dumpFileName;
    IN >> activeSize;
    IN >> allowPriceDrop;
    IN >> cumSurplus;
    if ( activeSize > 0 ) {
      if ( state->getMaxActiveNodes() != activeSize ) cerr << "Resizing active nodes from " << state->getMaxActiveNodes() << " to " << activeSize << endl;
      state->setMaxActiveNodes(activeSize);
      if ( allowPriceDrop > 0 ) {
	if ( ! allow_price_drop ) cerr << "Enabling down operations ... " << endl;
	allow_price_drop = true;
	if ( cumSurplus < 0 ) {
	  cerr << "Setting surplus to " << -cumSurplus << endl;
	  state->setCumulativeSurplus( (long unsigned int) -cumSurplus );
	}
      } else if ( allowPriceDrop == 0 ) {
	if ( allow_price_drop ) cerr << "Disabling down operations ... " << endl;
	allow_price_drop = false;
      }
      state->randomizeActiveNodes();
    }
    if ( dumpGraph > 0 ) {
      cerr << "Saving computation to dSet ... ";
      save(*dSet);
      cerr << "OK" << endl;
      cerr << "Dumping dSet to " << dumpFileName << " ...";
      dSet->save(dumpFileName, false);
      touch(controlFileName+"-dump_finished");
      cerr << "OK" << endl;
    }
  }
  return;
}

std::ostream &operator<<( std::ostream &OUT, const problem &prob ) {

  for( edge E = prob.G->numOfEdges()-1; E>=0; --E ) { 
    OUT << E<<" "<<prob.state->flowVector[E]<<endl;
  }
  return OUT;
}
