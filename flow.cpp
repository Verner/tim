#include "flow.h"
#include "dataSet.h"
#include "util.h"

#include <fstream>

using namespace std;

void relFlowVector::average( const relFlowVector &B ) { 
  long step = flow.size()/25;
  for(edge E = flow.size()-1; 0<=E; --E ) {

#ifdef DEBUG
    if ( E == 3222759 || E == 3222749 || E == 3222599 || E == 3222444 ) { 
      edgeFlow fA = flow[E], fB = B.flow[E];
      cerr << endl;
      cerr << "Edge: " << E << endl;
      cerr << "  A Flow: " <<  fA.getFlow()  << " Rel: " << fA.getRel() << endl;
      cerr << "  B Flow: " <<  fB.getFlow()  << " Rel: " << fB.getRel() << endl;
    }
#endif

    if ( E % step == 0 ) cerr << "*";
    flow[E].avgFlow( B.flow[E] );
  }
  cerr << endl;
}


void edgeFlow::avgFlow( const edgeFlow &B ) {

#ifndef VERSION_1
  /* If one of the values is much more reliable (differs by more than order 10)
   * than keep the other */
  if ( FP_SLE( getRel(), B.getRel()/10 ) ) {
    setFlow( B.getFlow(), B.getRel() );
    return;
  }
  if ( FP_SLE( B.getRel(), getRel()/10 ) ) { 
    return;
  }
#endif

  if ( FP_EQ((getRel() + B.getRel()), 0) ) { 
    setFlow( (getFlow() + B.getFlow())/2, 0 );
  } else {

    /* Compute a weighted average of the flows (weightedAvgFlow) 
     * which will be the resulting flow */
    real sumRel          = getRel() + B.getRel();
    real weightedSumFlow = getFlow()*getRel() + B.getFlow()*B.getRel();
    real weightedAvgFlow = weightedSumFlow/sumRel;
    
    /* To compute the reliability (avgRel), we do the following:
     *  1) Compute a linear function L(x) such that
     *     L(myFlow) = myRel &
     *     L(BFlow)  = BRel
     *  2) The reliability (avgRel) will now be
     *     L(weightedAvgFlow) */
    reliability deltaRel  = B.getRel()-getRel();
    real        deltaFlow = B.getFlow()-getFlow();
    reliability avgRel;
    if ( FP_EQ(deltaFlow, 0) ) { 
      avgRel = (getRel() + B.getRel())/2;
    } else avgRel = (deltaRel/deltaFlow)*(weightedAvgFlow-getFlow())+getRel();
    
    /* Finally set the flow */
    setFlow( weightedAvgFlow, avgRel );
  }
  
}
 
edgeFlow avgFlow( const edgeFlow &A, const edgeFlow &B ) { 
  struct edgeFlow ret = A;
  ret.avgFlow( B );
  return ret;
}

void relFlowVector::load( const dataSet &dSet ) {
  flow.resize( dSet.numOfEdges() );
  for( vector<dataRow>::const_iterator row = dSet.begin(); row != dSet.end(); ++row ) {
    setFlow( row->id, row->initialized, row->rel );
  }
}

void relFlowVector::save( dataSet& dSet ) const {
  edge maxE = MIN<edge_data_type>(dSet.size()-1,flow.size()-1);
  for( edge E = maxE; E>= 0; --E ) { 
    dSet[ E ].initialized = (int) flow[E].getFlow();
    dSet[ E ].rel = (float) flow[E].getRel();
  }
}
reliability edgeFlow::getRel() const {
    if ( FP_EQ( totalFlow, 0 ) ) return 0; 
    else return ( reliability )( partRel / totalFlow );
}
