#include "util.h"

#include <unistd.h>

#include <math.h>
#include <fstream>

std::iostream &operator<<(std::iostream &OUT, const edgeList &lst) { 
  for(edgeList::const_iterator it = lst.begin(); it != lst.end(); ++it)
    OUT <<*it<<" ";
  return OUT;
}


real gauss( real mean, real meanRel, real value ) {
  if ( FP_EQ( meanRel, 0 ) ) return 0;
  real sigma = 1/(meanRel*sqrt( 2*3.1415 ) );
  real exponent = -(value-mean)*(value-mean)/(2*sigma*sigma);
  return meanRel * exp( exponent );
}

void REL_AVG( real valA, real relA, real valB, real relB, real &tval, real &trel ) {
  if ( FP_EQ( relA, 0 ) ) { 
    tval = valB;
    trel = relB;
    return;
  }

  if ( FP_EQ( relB, 0 ) ) { 
    tval = valA;
    trel = relA;
    return;
  }
  
  if ( FP_EQ( valB, valA ) ) { 
    tval = valA;
    trel = (relA+relB)/2;
  }
  
  real wA = relA/(relA+relB),
       wB = relB/(relA+relB);
       
  REL_SUM( valA * wA, relA/wA, valB * wB, relB/wB, tval, trel );
  
}

/* Should be fine for normal distributions
 * where relA = 1/[sigma*sqrt(2*pi)]
 * and   valA = mu
 * NOTE: rel{A,B} == 0 && val{A,B] == 0 is special !!!
 */
void REL_SUM( real valA, real relA, real valB, real relB, real &tval, real &trel ) {
  if ( FP_EQ(relA,0) && FP_EQ(valA,0) )  {
    trel = relB;
    tval = valB;
    return;
  }
  if ( FP_EQ(relB,0) && FP_EQ(valB,0) ) { 
    trel = relA;
    tval = valA;
    return;
  }
  tval = valA + valB;
  if ( FP_EQ( sqrt(relA*relB + relB*relB), 0 ) ) trel = (relA+relB)/2;
  else trel = relA*relB/sqrt(relA*relA+relB*relB);
}

void REL_WEIGHT( real valA, real relA, real weight, real &tval, real &trel ) {
  if ( FP_EQ( weight, 0 ) ) {
    tval = 0;
    trel = 1;
    return;
  }
  tval = weight*valA;
  trel = relA/weight;
}

/* Probably not statistically sound!!! */
void REL_MAX( real valA, real relA, real valB, real relB, real &maxVal, real &maxRel ) {
  if ( FP_EQ(valA,0) && FP_EQ(relA,0) ) { 
    maxVal = valB;
    maxRel = relB;
    return;
  } else if ( FP_EQ(valB,0) && FP_EQ(relB,0) ) { 
    maxVal = valA;
    maxRel = relA;
    return;
  }
  maxVal = MAX<real>( valA, valB );
  maxRel = ( gauss( valA, relA, maxVal ) + gauss( valB, relB, maxVal ) )/2;
}

bool file_exists( const std::string &fname ) {
  std::ifstream IN;
  IN.open( fname.c_str(), std::ifstream::in );
  return ( IN.is_open() );
}

void touch( const std::string &fname ) {
  std::ofstream OUT;
  OUT.open( fname.c_str(), std::ofstream::app );
  OUT.close();
}

bool notDigit( const char s ) { 
  return (s < '0' || '9' < s);
}

int dig2num( const char s ) { 
  return (unsigned int)s-(unsigned int)'0';
}

long long readNumberToNextNonDigit( const std::string& number, uint& pos ) { 
  uint len = number.length();
  if ( len <= pos || notDigit( number[pos] ) ) return -1;
  long long ret = dig2num( number[pos++] );
  while( pos < len ) {
    if ( notDigit( number[pos] ) ) return ret;
    ret *= 10;
    ret += dig2num( number[pos++] );
  }
  return ret;
}

bool toNum( const std::string& number, real& ret ) { 
  uint len = number.length();
  uint pos = 0;
  real flpart = 0;
  real koef = 1;
  
  if ( number[pos] == '-' ) {
    koef = -1;
    pos++;
  }
  
  ret = (real) readNumberToNextNonDigit( number, pos );
  if ( FP_EQ(ret, -1) ) return false;
  if ( pos == len ) {
    ret *= koef;
    return true;
  }

  if ( number[pos] == '.' ) {
    int spos = ++pos;
    flpart = (real) readNumberToNextNonDigit( number, pos );
    if ( FP_EQ(flpart, -1) ) return false;
    flpart = flpart/EXP<real>(10,pos-spos);
  }
  
  if ( pos == len ) {
    ret += flpart;
    ret *= koef;
    return true;
  }
  if ( number[pos] == 'e' ) { 
    bool ng_mantissa = false;
    ++pos;
    if ( number[pos] == '-' ) ng_mantissa = true;
    else if ( number[pos] == '+' ) ng_mantissa = false;
    else return false;
    pos++;
    real mantissa = (real) readNumberToNextNonDigit( number, pos );
    if ( FP_EQ(mantissa, -1) ) return false;
    if ( ng_mantissa ) koef *= 1.0/EXP<real>(10,(int)mantissa);
    else koef *= EXP<real>(10,(int)mantissa);
  }
  
  if ( pos == len ) {
    ret+= flpart;
    ret*= koef;
    return true;
  }
  return false;
}

std::string ltrim( const std::string& str, int num ) { 
  std::string ret = "";
  for(unsigned int i=num; i< str.length(); i++) 
    ret += str[i];
  return ret;
}

std::string rtrim( const std::string& str, int num ) { 
  std::string ret = "";
  for(unsigned int i=0; i < str.length()-num; i++)
    ret += str[i];
  return ret;
}

char toUpper(const char c) {
  if ( (uint) 'a' <= (uint) c && (uint) c <= (uint) 'z' ) { 
    uint ord = (uint) c - (uint) 'a' + (uint) 'A';
    return (char) ord;
  } else return c;
}

bool split(const std::string& str, std::string& left, std::string& right, char split) {
  uint i=0;
  left = ""; right ="";
  for( ; i < str.length(); i++ ) { 
    if ( str[i] == split ) break;
    left += str[i];
  }
  
  if ( i >= str.length() ) return false;
  
  for( i++; i < str.length(); i++ )
    right += str[i];
  
  return true;

}

std::string toUpper(const std::string& str) {
  std::string ret="";
  for(uint i=0; i<str.length(); i++)
    ret+=toUpper(str[i]);
  return ret;
}


bool toBool(const std::string& str, bool& val) {
  std::string norm = toUpper(str);
  if ( norm == "FALSE" || norm == "NO" || norm == "0" ) { 
    val = false;
    return true;
  } else if ( norm == "TRUE" || norm == "YES" || norm == "1" ) { 
    val = true;
    return true;
  }
  return false;
}

void rm( const std::string& fname ) { 
  unlink( fname.c_str());
}

