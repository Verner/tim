/* 
 * PARAMETERS
 *   #MEASURED_GRAPH
 */
begin transaction;

/* Graph */
CREATE TEMPORARY TABLE output_graph (
	id INTEGER,
	fromID INTEGER,
	toID INTEGER,
	fClass INT2
);

INSERT INTO output_graph
SELECT id, fromID, toID, fClass
FROM mgraph_edges WHERE mgraph_id = #MEASURED_GRAPH;

COPY output_graph TO '/tmp/edges' USING DELIMITERS ' ';

DROP TABLE output_graph;

/* Measurements */
create temporary table  output_meas (
	id int8,
	intensity integer
);
insert into output_meas 
select id, intensity from mgraph_nodes 
where ntype != 'regular' AND mgraph_id = #MEASURED_GRAPH;

copy output_meas to '/tmp/meas' using delimiters ' ';
drop table output_meas;

/* Neibours */
create temporary table output_nbs (
	idA int8,
	idB int8
);

create temporary table n_sinks (
	id INTEGER,
	cat INT8 DEFAULT 0,
	edge_cat INT8,
	nType VARCHAR(8) DEFAULT 'regular',
	intensity INTEGER DEFAULT 0,
	mgraph_id INTEGER
);

insert into n_sinks
select * 
from mgraph_nodes
where mgraph_id = #MEASURED_GRAPH AND ( nType = 'sink' OR nType = 'snkF' OR ntype = 'snkT' );

create temporary table n_sources (
	id INTEGER,
	cat INT8 DEFAULT 0,
	edge_cat INT8,
	nType VARCHAR(8) DEFAULT 'regular',
	intensity INTEGER DEFAULT 0,
	mgraph_id INTEGER
);
insert into n_sources
select * 
from mgraph_nodes
where mgraph_id = #MEASURED_GRAPH AND ( nType = 'source' OR nType = 'srcF' OR ntype = 'srcT' );

create temporary table n_regular(
	id INTEGER,
	cat INT8 DEFAULT 0,
	edge_cat INT8,
	nType VARCHAR(8) DEFAULT 'regular',
	intensity INTEGER DEFAULT 0,
	mgraph_id INTEGER
);
insert into n_regular
select * 
from mgraph_nodes
where mgraph_id = #MEASURED_GRAPH AND  nType = 'regular';

CREATE temporary TABLE g_edges (
	id INTEGER,
	cat INT8,
	fromID INTEGER,
	toID INTEGER,
	fClass INT2,
	multiplicity INTEGER,
	mgraph_id INTEGER
);
insert into g_edges 
select *
from mgraph_edges
where mgraph_id=#MEASURED_GRAPH;

insert into output_nbs
SELECT sink.id as id, nxt.id as nid
FROM n_sinks as sink, 
     g_edges as e_tonbr, 
     g_edges as e_tosink,
     n_regular as nxt,
     n_regular as prv
WHERE   ( e_tonbr.cat = sink.edge_cat AND e_tosink.cat = sink.edge_cat)
      AND 
        ( e_tosink.toid = sink.id AND e_tonbr.toid = nxt.id )
      AND 
        ( prv.id = e_tosink.fromid )
      AND 
        NOT e_tonbr.toid = prv.id
UNION ALL 
SELECT src.id as id, prv.id as nid
FROM n_sources as src,
     g_edges as e_fromprv,
     g_edges as e_tonxt,
     n_regular as prv,
     n_regular as nxt
WHERE	 ( e_tonxt.cat = src.edge_cat AND e_fromprv.cat = src.edge_cat )
     AND
         ( e_tonxt.fromid = src.id AND e_fromprv.fromid = prv.id )
     AND 
         ( nxt.id = e_tonxt.toid )
     AND
         NOT e_fromprv.fromid = nxt.id
ORDER BY
        id;


/*
insert into output_nbs
SELECT sink.id as id, nxt.id as nid
FROM mgraph_nodes as sink, 
     mgraph_edges as e_tonbr, 
     mgraph_edges as e_tosink,
     mgraph_nodes as nxt,
     mgraph_nodes as prv
WHERE 
        ( sink.ntype='sink' OR sink.ntype='snkF' OR sink.ntype='snkT' )
      AND 
        ( e_tonbr.cat = sink.edge_cat AND e_tosink.cat = sink.edge_cat)
      AND 
        ( e_tosink.toid = sink.id AND e_tonbr.toid = nxt.id )
      AND 
        ( prv.id = e_tosink.fromid )
      AND 
        nxt.ntype='regular' AND prv.ntype = 'regular'
      AND 
        NOT e_tonbr.toid = prv.id
      AND
	sink.mgraph_id = #MEASURED_GRAPH AND
	e_tonbr.mgraph_id = #MEASURED_GRAPH AND
	e_tosink.mgraph_id = #MEASURED_GRAPH AND
	nxt.mgraph_id = #MEASURED_GRAPH AND
	prv.mgraph_id = #MEASURED_GRAPH
UNION ALL 
SELECT src.id as id, prv.id as nid
FROM mgraph_nodes as src,
     mgraph_edges as e_fromprv,
     mgraph_edges as e_tonxt,
     mgraph_nodes as prv,
     mgraph_nodes as nxt
WHERE
         ( src.ntype='source' OR src.ntype='srcF' OR src.ntype = 'srcT' )
     AND
	 ( e_tonxt.cat = src.edge_cat AND e_fromprv.cat = src.edge_cat )
     AND
         ( e_tonxt.fromid = src.id AND e_fromprv.fromid = prv.id )
     AND 
         ( nxt.id = e_tonxt.toid )
     AND
         ( nxt.ntype = 'regular' AND prv.ntype = 'regular' )
     AND
         NOT e_fromprv.fromid = nxt.id
     AND
 	src.mgraph_id = #MEASURED_GRAPH AND
	e_fromprv.mgraph_id = #MEASURED_GRAPH AND
	e_tonxt.mgraph_id = #MEASURED_GRAPH AND
	nxt.mgraph_id = #MEASURED_GRAPH AND
	prv.mgraph_id = #MEASURED_GRAPH
ORDER BY
        id;*/

copy output_nbs to '/tmp/nbs' using delimiters ' ';
drop table output_nbs;

/* Zakazy */
create temporary table  output_zakazy (
	fid int8,
	tid int8
);

insert into output_zakazy 
select f.id, t.id from zakazy as z, mgraph_edges as f, mgraph_edges as t 
where z.catfrom = f.cat and 
      z.catto = t.cat   and 
      f.mgraph_id=#MEASURED_GRAPH and
      t.mgraph_id=#MEASURED_GRAPH;

copy output_zakazy to '/tmp/zakazy' using delimiters ' ';
drop table output_zakazy;

end transaction;
