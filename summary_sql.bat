@echo begin transaction;
@echo drop table if exists tim.%1 cascade;

@echo CREATE TABLE tim.%1 (
@echo    link_id int PRIMARY KEY NOT NULL,
@echo    measured_f int,
@echo    measured_b int DEFAULT NULL,
@echo    init_f int,
@echo    init_b int DEFAULT NULL,
@echo    computed_f int DEFAULT 0,
@echo    computed_b int DEFAULT NULL,
@echo    rel_f float(19) DEFAULT 0,
@echo    rel_b float(19) DEFAULT NULL,
@echo    lbound_f int DEFAULT 0,
@echo    lbound_b int DEFAULT NULL,
@echo    surplus_src_f int DEFAULT 0,
@echo    surplus_src_b int DEFAULT NULL,
@echo    surplus_tgt_f int DEFAULT 0,
@echo    surplus_tgt_b int DEFAULT NULL,
@echo    fclass smallint NOT NULL,
@echo    str_info varchar(100) DEFAULT ''
@echo );

@echo copy tim.%1 from 'h:/Data/verner/my_summary_table' USING DELIMITERS ' ' WITH NULL as '-1';
@echo end transaction;

