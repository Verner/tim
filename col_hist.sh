#!/bin/bash
# col_hist.sh -- created 2009-08-17, Jonathan Verner
# @Last Change: 24-Dec-2008.
# @Revision:    0.0

OUT_FILE=$1;
TABLE=$2;
COLUMN=$3;
CONDITION=$4;

if [ "$CONDITION" == "" ]; then
	CONDITION="1=1";
fi

echo "select $COLUMN from $TABLE where $CONDITION; " | psql -t -A mge > /tmp/temp_histo.$$;

echo " a = load /tmp/temp_histo.$$ " > /tmp/oct.scr.$$
echo " hist(a,30) " >> /tmp/oct.scr.$$
echo " print $OUT_FILE " >> /tmp/oct.scr.$$;

octave /tmp/oct.scr.$$ 2>/dev/null >/dev/null


# vi: 
