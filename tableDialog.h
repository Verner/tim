#ifndef _TABLE_DIALOG_H_
#define _TABLE_DIALOG_H_

#include "ui_tableselectdlg.h"

#include <QtCore/QString>
#include <QtCore/QList>

#include <QtGui/QDialog>

class QSqlQueryModel;
class QSqlDatabase;
class QComboBox;
class QLabel;

class col {
public:
  QString default_name;
  QComboBox *combo;
  QLabel *label;
};

class tableDialog : public QDialog {
  Q_OBJECT
  
public:
    tableDialog(QSqlDatabase *DB, QWidget* parent = 0, Qt::WindowFlags f = 0, const QString table_name = "");
    virtual ~tableDialog();
    int addColumn(const QString &default_name, const QString &description);
    QString getColumn(int column);
    QString getTable() const;
    QString getSchema() const;
    
    void setModels(const QString schema_name = "");
    void clearModels();

private slots:
  
  void schemaChanged();
  void tableChanged();

private:
  void default_value( QComboBox *combo, const QString default_value = "");
  QString default_table, curSchema, curTable;
  QList<col> columns;
  Ui::tableDialog dlg;
  QSqlDatabase *db;
  QSqlQueryModel *schema_model, *table_model, *column_model;
};

#endif // _TABLE_DIALOG_H_
