/***************************************************************
 * main.cpp
 * @Author:      Jonathan Verner (jonathan.verner@matfyz.cz)
 * @License:     GPL v2.0 or later
 * @Created:     2009-05-18.
 * @Last Change: 2009-05-18.
 * @Revision:    0.0
 * Description:
 * Usage:
 * TODO:
 *CHANGES:
 ***************************************************************/

#include "problem.h"
#include "costFunction.h"

#include <string>
#include <iostream>

#include "tim_options.h"
#include "global_config.h"

#include "timModel.h"
#include "dataSet.h"
#include "graph.h"


using namespace std;

int main(int argc, const char **argv) {
    tim_options opt;
    timModel model;

    if (! opt.parse(argc, argv) ) exit(-1);

    if ( global_config.getBool(ALLOW_DOWN_OPTION) ) model.allowDown();
    
    model.setControlFile(global_config.getStr(CONTROL_FILE_OPTION));


    /* RESTART FROM CHECKPOINT */
    if ( opt.checkpoint != "" ) { 
      model.restartFromCheckPoint( opt.checkpoint, (long long int) global_config.getNum(DUMP_INTERVAL_OPTION), global_config.getStr(DUMP_FILE_PREFIX_OPTION) );
      return 0;
    };
    
    /* EXIT IF SETTING ZAKAZY IN THE COMPUTE PHASE */
    if ( opt.command == PHASE_COMPUTE && global_config.getStr(ZAKAZY_FILE_OPTION) != "" ) {
      cerr << "Error: Cannot set zakazy in the second phase. " << endl;
      exit(-1);
    }
    
    /*
     * PRINT THE CURRENT CONFIGURATION
     */
    
    cerr << "***************** CURRENT CONFIGURATION ******************" << endl;
    cerr << global_config;
    cerr << "**********************************************************" << endl;
    
    /*
     * 
     *  SWITCH ACCORDING TO THE GIVEN COMMAND
     * 
     */
    
    /* PRINT SUMMARY TABLE */
    if ( opt.command == PHASE_SUMMARY )  {
      dataSet d;
      graph *G = new graph;
      cerr << "Loading data ... ";
      if ( global_config.getStr(DATA_FILE_OPTION) == "" ) cin >> d;
      else d.load( global_config.getStr(DATA_FILE_OPTION) );
      cerr << "OK" << endl;
      cerr << "Constructing the graph ... ";
      G->load( d );
      cerr << "OK" << endl;
      cerr << "Saving data ... ";
      d.summaryTable( global_config.getStr(OUTPUT_FILE_OPTION), G );
      cerr << "OK" << endl;
      return 0;    
    }
    
    /* PRINT id2cat MAP */
    if ( opt.command == PHASE_ID2CAT ) {
      dataSet d;
      graph *G = new graph;
      cerr << "Loading data ... ";
      if ( global_config.getStr(DATA_FILE_OPTION) == "") cin >> d;
      else d.load( global_config.getStr(DATA_FILE_OPTION) );
      cerr << "OK" << endl;
      cerr << "Constructing the graph ... ";
      G->load( d );
      cerr << "OK" << endl;
      cerr << "Printing the edge_id to cat map ... ";
      d.printEdgeID2CATMap( global_config.getStr(OUTPUT_FILE_OPTION) );
      cerr << "OK" << endl;
      return 0;
    }

 
    /* LOAD DATA */
    if ( global_config.getStr(DATA_FILE_OPTION) == "") {
        cerr << "Loading data from standard input." << endl;
        cin >> model;
    } else {
        cerr << "Loading data from " << global_config.getStr(DATA_FILE_OPTION) << endl;
        model.load( global_config.getStr(DATA_FILE_OPTION) );
    }

    /* INIT PHASE */
    if ( opt.command == PHASE_INIT ) {
      cerr << "Phase: init ..." << endl;
      
      /* ENFORCE ZAKAZY */
      if ( global_config.getStr(ZAKAZY_FILE_OPTION, "") != "" ) {
        cerr << "Loading zakazy from " << global_config.getStr(ZAKAZY_FILE_OPTION,"") << endl;
        model.setZakazy( global_config.getStr(ZAKAZY_FILE_OPTION,"") );
      }
      
      costFunction::setAvoidZero( global_config.getNum(AVOID_ZERO_OPTION) );
      model.init_phase( global_config.getStr(OUTPUT_FILE_OPTION,"") );
      
      return 0;
    }

    /* COMPUTE PHASE */
    if ( opt.command == PHASE_COMPUTE ) {
      if ( opt.update_file != "" ) model.update( opt.update_file );
      cerr << "Phase: compute ..." << endl;
      model.compute_phase( (long long int) global_config.getNum(EPSILON_OPTION), (long long int) global_config.getNum(DUMP_INTERVAL_OPTION), global_config.getStr(DUMP_FILE_PREFIX_OPTION) );
      
      /* SAVE OUTPUT */
      if ( global_config.getStr(OUTPUT_FILE_OPTION) != "" ) {
        cerr << "Saving data to " << global_config.getStr(OUTPUT_FILE_OPTION) << endl;
        model.save( global_config.getStr(OUTPUT_FILE_OPTION) );
      } else {
        cerr << "Saving data to standard output. " << endl;
        cout << model;
      }
      return 0;
    }
    
    return 0;
}

