CMAKE_MINIMUM_REQUIRED(VERSION 0.0)
PROJECT(TIM)
INCLUDE(CheckIncludeFile)
INCLUDE(CheckLibraryExists)
INCLUDE(UsePkgConfig)
SET(PROJECT_NAME TIM)
SET(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake/modules")


#if( CMAKE_SYSTEM_NAME MATCHES "Windows")
#else(CMAKE_SYSTEM_NAME MATCHES "Windows")


# this command finds Qt4 libraries and sets all required variables
# note that it's Qt4, not QT4 or qt4
SET( QT_USE_QTSQL TRUE )
FIND_PACKAGE( Qt4 REQUIRED )
  
# add some useful macros and variables
# (QT_USE_FILE is a variable defined by FIND_PACKAGE( Qt4 ) that contains a path to CMake script)
INCLUDE( ${QT_USE_FILE} )
#endif(CMAKE_SYSTEM_NAME MATCHES "Windows")


IF(CMAKE_SYSTEM_NAME MATCHES "Windows")
	SET(BOOST_INCLUDE_DIRS "~/.wine/drive_c/MinGW/include")
	#	SET(Boost_LIBRARIES "/home/jonathan/.wine/drive_c/MinGW/bin/libboost_program_options-gcc34-mt-1_38.dll")
	#SET(Boost_LIBRARIES "/home/jonathan/.wine/drive_c/MinGW/bin/libboost_program_options-mt.dll")


ENDIF(CMAKE_SYSTEM_NAME MATCHES "Windows")


INCLUDE_DIRECTORIES(
 ${BOOST_INCLUDE_DIRS}
 ${CMAKE_CURRENT_SOURCE_DIR}
 ${CMAKE_CURRENT_BINARY_DIR}
)


SET(PROJ_SRC 
	main.cpp
	problem.cpp
	problemState.cpp
	graph.cpp
	util.cpp
	costFunction.cpp
	sourceSinkList.cpp
	zakazy.cpp
	dataSet.cpp
	initState.cpp
	timModel.cpp
	flow.cpp
	translator.cpp
	defs.cpp
	options.cpp
	tim_options.cpp
	global_config.cpp
	config.cpp
)

SET(TEST_OPTIONS_SRC
	options.cpp
	tim_options.cpp
	test_options.cpp
	util.cpp
	global_config.cpp
	config.cpp
)

SET(TEST_CONFIG_SRC
	config.cpp
	util.cpp
	test_config.cpp
)

SET(TEST_DATASET_SRC
	testDataSet.cpp
	dataSet.cpp
	util.cpp
	flow.cpp
)

SET(LINK_LIBS
#${Boost_LIBRARIES}
-static-libgcc
)

#SET(LINK_LIBS ${Boost_LIBRARIES} --large-address-aware )



SET(QTIM_UI_FILES
	tableselectdlg.ui
	maindlg.ui
)

# this will run uic on .ui files:
QT4_WRAP_UI( QTIM_UI_HDRS ${QTIM_UI_FILES} )

SET(QTIM_SRC
	qtim.cpp
	mainDialog.cpp
	tableDialog.cpp
)

SET(QTIM_MOC_HDRS
	tableDialog.h
	mainDialog.h
)

qt4_wrap_cpp( QTIM_MOC_SRCS ${QTIM_MOC_HDRS})



INCLUDE_DIRECTORIES( ${CMAKE_BINARY_DIR} )

ADD_EXECUTABLE(tim ${PROJ_SRC})
TARGET_LINK_LIBRARIES(tim ${LINK_LIBS})

ADD_EXECUTABLE(tim-v1 ${PROJ_SRC})
TARGET_LINK_LIBRARIES(tim-v1 ${LINK_LIBS})
set_target_properties(tim-v1 PROPERTIES
  COMPILE_FLAGS "-DVERSION_1"
)

ADD_EXECUTABLE(replace replace.cpp)
target_link_libraries(replace ${LINK_LIBS})

if(${TARGET} MATCHES "64")
  add_definitions(
    -m64
  )
else(${TARGET} MATCHES "64")
  ADD_EXECUTABLE(qtim ${QTIM_SRC} ${QTIM_UI_HDRS} ${QTIM_MOC_SRCS})
  TARGET_LINK_LIBRARIES(qtim ${QT_LIBRARIES})
endif(${TARGET} MATCHES "64")

ADD_EXECUTABLE(test_options ${TEST_OPTIONS_SRC})
ADD_EXECUTABLE(test_dataset ${TEST_DATASET_SRC})
ADD_EXECUTABLE(test_config ${TEST_CONFIG_SRC})

IF(CMAKE_SYSTEM_NAME MATCHES "Windows")
ADD_DEFINITIONS(
#	-DQT_NO_DEBUG_OUTPUT
	# -g
	-O3
	-static-libgcc
)
ELSE(CMAKE_SYSTEM_NAME MATCHES "Windows")
ADD_DEFINITIONS(
	-g
#	-O3
        -DUNIX
#	-DQT_NO_DEBUG
#	-DQT_NO_DEBUG_OUTPUT
        #-DNDEBUG
        #-DDEBUG
	#-DD_ALG
        #-DD_OPT
	-Wall
	-Wextra
	-Wfloat-equal
	-Wshadow
	-Wconversion
	#        -pg
#       -fprofile-arcs
#       -lgcov
)
ENDIF(CMAKE_SYSTEM_NAME MATCHES "Windows")
