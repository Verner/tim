#!/bin/bash
# run_init.sh -- created 2009-07-15, Jonathan Verner
# @Last Change: 24-Dec-2008.
# @Revision:    0.0

./build/init -g /tmp/edges -m /tmp/meas -n /tmp/nbs -z /tmp/zakazy > /tmp/init 2>/dev/null

cat db_upload_init.sql | psql mge

# vi: 
