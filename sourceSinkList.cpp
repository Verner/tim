#include "sourceSinkList.h"
#include "graph.h"
#include "dataSet.h"

#include <fstream>

using namespace std;

void sourceSinkList::load( const dataSet &dSet ) {
  sources.clear(); sinks.clear();
  for( vector<dataRow>::const_iterator row = dSet.begin(); row != dSet.end(); ++row ) {
    switch( row->typ ) { 
      case dataRow::SOURCE:
	assert( row->measured >= 0 );
	sources.push_back( pair<edge,real>( row->id, row->measured ) );
	break;
      case dataRow::SINK:
	assert( row->measured <= 0 );
	sinks.push_back( pair<edge,real>( row->id, row->measured ) );
	break;
      case dataRow::ESTIMATED:
	assert( row->measured >=0 );
	sources.push_back( pair<edge,real>( row->id, row->measured ) );
	sinks.push_back( pair<edge,real>( row->id, row->measured ) );
      default:
	break;
    }
  }
}
