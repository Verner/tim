begin transaction;
drop table if exists deadendorder cascade;

create table deadendorder (
  cat int8,
  deadorder integer
);

copy deadendorder from '/tmp/deadends';

end transaction;


