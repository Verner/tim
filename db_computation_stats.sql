select i.fclass,
       round(avg(abs(c.intensity-i.intensity))) as avg_mod, 
       round(stddev(abs(c.intensity-i.intensity))) as dev_mod,
       round(max(abs(c.intensity-i.intensity))) as max_mod,
       round(avg(i.intensity)) as avg_i, 
       round(stddev(i.intensity)) as dev_i,
       round(max(i.intensity)) as max_i,
       round(avg(c.intensity)) as avg_c, 
       round(stddev(c.intensity)) as dev_c,  
       round(min(c.intensity)) as min_c,
       round(max(c.intensity)) as max_c,
       avg(i.avgreliability) as avg_rel,
       stddev(i.avgreliability) as dev_rel
from map_layer_init as i, map_layer_computation as c 
where i.cat=c.cat and c.intensity > 0 
group by i.fclass;
