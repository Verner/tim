/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#ifndef GRAPHSCENE_H
#define GRAPHSCENE_H

#include <QtGui/QGraphicsScene>
#include <QtCore/QString>

#include <string>
#include <vector>

#include "defs.h"

class edgeItem;
class vertexItem;
class edgeTable;

class graphScene : public QGraphicsScene {
  private:
    edgeTable *graph;
    std::vector<vertexItem *> vertices;
    std::vector<edgeItem *> edges;

  public:
    void addGraph( edgeTable *graph, std::string coordsFile );

  public slots:
    void setVertexColor( node V, QColor col );
    void setVertexDiam( node V, real diam );
    void setEdgeColor( edge E, QColor col );
    void setEdgeDiam( edge E, real col );
    void setEdgeText( edge E, std::string text );
    void setEdgeText( edge E, const QString text );
    void setVertexText( node V, std::string text );
    void setVertexText( node V, const QString text );

};

#endif // GRAPHSCENE_H
