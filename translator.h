#ifndef _translator_h
#define _translator_h

#include "defs.h"

#include <vector>

class dataSet;

class translator {
  private:
    std::vector<edge> new2old;
  public:
    translator( const dataSet &oldSet, const dataSet &newSet );
    edge operator[]( edge E ) const {return new2old[E];};
};

#endif // _translator_h
