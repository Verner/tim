/*
 * PARAMETERS
 *  #BASE_COMPUTATION ... computation to base the averages on
 *  #FC1 ... max. number of measured fclass 1 nodes
 *  #FC2 ... max. number of measured fclass 2 nodes
 *  #FC3 ... max. number of measured fclass 3 nodes
 *  #FC4 ... max. number of measured fclass 4 nodes
 *  #FC5 ... max. number of measured fclass 5 nodes
 */

begin transaction;

create sequence temp_mmts_seq minvalue 0;
create temporary table mmts (
    ord integer default nextval('temp_mmts_seq'),
    cat int8,
    measured integer
);

/* FCLASS 1 */
insert into mmts (cat, measured)
select c.cat, 
       round(s.avg_c-s.dev_c+random()*2*s.dev_c) as measured
from connections as c, 
     computation_stats as s
where c.fclass = 1 and
      s.fclass = 1 and
      s.computation_run_id  = #BASE_COMPUTATION
order by random();
update mmts set measured = NULL where ord > #FC1;
insert into measurements
select m.cat,
       c.fromcat,
       c.tocat,
       c.fclass,
       c.directed,
       m.measured,
       currval('measurements_seq')
from mmts as m, connections as c
where c.cat = m.cat;

/* FCLASS 2 */
insert into mmts (cat, measured)
select c.cat, 
       round(s.avg_c-s.dev_c+random()*2*s.dev_c) as measured
from connections as c, 
     computation_stats as s
where c.fclass = 2 and
      s.fclass = 2 and
      s.computation_run_id  = #BASE_COMPUTATION
order by random();
update mmts set measured = NULL where ord > #FC2;
insert into measurements
select m.cat,
       c.fromcat,
       c.tocat,
       c.fclass,
       c.directed,
       m.measured,
       currval('measurements_seq')
from mmts as m, connections as c
where c.cat = m.cat;

/* FCLASS 3 */
delete from mmts;
select setval('temp_mmts_seq',0);
insert into mmts (cat, measured)
select c.cat, 
       round(s.avg_c-s.dev_c+random()*2*s.dev_c) as measured
from connections as c, 
     computation_stats as s
where c.fclass = 3 and
      s.fclass = 3 and
      s.computation_run_id  = #BASE_COMPUTATION
order by random();
update mmts set measured = NULL where ord > #FC3;
select cat, measured from mmts;
insert into measurements
select m.cat,
       c.fromcat,
       c.tocat,
       c.fclass,
       c.directed,
       m.measured,
       currval('measurements_seq')
from mmts as m, connections as c
where c.cat = m.cat;


/* FCLASS 4 */
delete from mmts;
select setval('temp_mmts_seq',0);
insert into mmts (cat, measured)
select c.cat, 
       round(s.avg_c-s.dev_c+random()*2*s.dev_c) as measured
from connections as c, 
     computation_stats as s
where c.fclass = 4 and
      s.fclass = 4 and
      s.computation_run_id  = #BASE_COMPUTATION
order by random();
update mmts set measured = NULL where ord > #FC4;
select cat, measured from mmts;
insert into measurements
select m.cat,
       c.fromcat,
       c.tocat,
       c.fclass,
       c.directed,
       m.measured,
       currval('measurements_seq')
from mmts as m, connections as c
where c.cat = m.cat;

/* FCLASS 5 */
delete from mmts;
select setval('temp_mmts_seq',0);
insert into mmts (cat, measured)
select c.cat, 
       round(s.avg_c-s.dev_c+random()*2*s.dev_c) as measured
from connections as c, 
     computation_stats as s
where c.fclass = 5 and
      s.fclass = 5 and
      s.computation_run_id  = #BASE_COMPUTATION
order by random();
update mmts set measured = NULL where ord > #FC5;
select cat, measured from mmts;
insert into measurements
select m.cat,
       c.fromcat,
       c.tocat,
       c.fclass,
       c.directed,
       m.measured,
       currval('measurements_seq')
from mmts as m, connections as c
where c.cat = m.cat;

drop sequence temp_mmts_seq cascade;
select setval('measurements_seq',currval('measurements_seq')+1);

end transaction;
