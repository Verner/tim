#include <iostream>
#include <string>
#include <list>
using namespace std;

class repl {
public:
  repl( char *what, char *by ): replace_what(what), replace_by(by) {};
  string replace_what, replace_by;
};

int main(int argc, char **argv) { 
  list<repl> replacements;
  for(int i=1;i<argc-1;) {
    replacements.push_back( repl(argv[i], argv[i+1]) );
    i+=2;
  } 
  string str;
  int pos;
  while( cin ) { 
    getline (cin, str);
    for( std::list<repl>::const_iterator r = replacements.begin(); r != replacements.end(); ++r ) { 
      pos = str.find(r->replace_what);
      while( pos != string::npos ) {
	str.replace( pos, r->replace_what.size(), r->replace_by );
	pos += r->replace_by.size();
	pos = str.find(r->replace_what,pos);
      }
    }
    cout << str << endl;
  }
}