/* 
 * PARAMETERS
 *   #MEASUREMENT_SET
 *   #CONNECTION_COND
 */

begin transaction;

set search_path=tim;

DROP SEQUENCE IF EXISTS tnode_seq CASCADE;
DROP SEQUENCE IF EXISTS tedge_seq CASCADE;
DROP TABLE IF EXISTS tgraph_edges CASCADE;
DROP TABLE IF EXISTS tgraph_nodes CASCADE;
DROP TABLE IF EXISTS tnode_cats CASCADE;

drop table if exists measurements;
create table measurements (
  cat int8,
  fromCat int8,
  toCat int8,
  fclass int2,
  directed char(1),
  measured integer,
  lbound int4 default NULL
);

delete from measurements;
insert into measurements
select c.link_id ,c.nodeid_from, c.nodeid_to,c.func_class,c.dir_of_travel,m.intensity,c.residential_traffic
from tim.network as c left outer join tim.aadf_sets as m 
on c.link_id=m.link_id 
where c.link_id>0 and  
      c.dir_of_travel in ('B','F') and 
      c.func_class > 0 and 
      c.func_class < 6 and
      c.access_restriction_allowed = 'Y' and
      NOT c.residential;
/*      ( deadend < 1 OR ( deadendrecipient = 0 and c_resid_cumlative > 0 ) );*/

insert into measurements 
select c.link_id ,c.nodeid_to, c.nodeid_from,c.func_class,'F',m.intensity,c.residential_traffic
from tim.network as c left outer join tim.aadf_sets as m 
on c.link_id=m.link_id 
where c.link_id>0 and 
      c.dir_of_travel in ('T') and 
      c.func_class > 0 and 
      c.func_class < 6 and
      c.access_restriction_allowed = 'Y' and
      NOT c.residential;
/*      ( deadend < 1 OR ( deadendrecipient = 0 and c_resid_cumlative > 0 ) );*/

update measurements set measured = -1 where measured is null;
update measurements set lbound   = -1 where lbound is null;


/* NODES */
CREATE SEQUENCE tnode_seq MINVALUE 0;

CREATE TEMPORARY TABLE tgraph_nodes (
	id INTEGER DEFAULT nextval('tnode_seq'),
	cat INT8 DEFAULT 0,
	edge_cat INT8,
        nType VARCHAR(8) DEFAULT 'regular',
	intensity INTEGER DEFAULT 0
);

/* EDGES */
CREATE SEQUENCE tedge_seq MINVALUE 0;

CREATE TEMPORARY TABLE tgraph_edges (
	id INTEGER DEFAULT nextval('tedge_seq'),
	cat INT8,
	fClass INT2,
	typ INT2 DEFAULT 0, /* 0 ... REGULAR, 
                               1 ... SOURCE, 
                               2 ... SINK,
                               3 ... ARTIFICIAL
                               4 ... INVALID
                               5 ... ESTIMATED (i.e. deadendorder > 0 and deadendrecipient = 0) */
	multiplicity INTEGER,
	fromID INTEGER,
	toID INTEGER,
	lbound INTEGER DEFAULT NULL,
	ubound INTEGER DEFAULT NULL,
        measured INTEGER DEFAULT NULL,
	init INTEGER DEFAULT NULL,
	computed INTEGER DEFAULT NULL,
	reliability FLOAT DEFAULT NULL,
	fclassBACK INT2 DEFAULT NULL
);

--create index i_tgraph_edges_id on tgraph_edges ( id );
--create index i_tgraph_edges_cat on tgraph_edges ( cat );


/* POPULATE NODES */

CREATE TEMPORARY TABLE tnode_cats (
	cat INT8
);

INSERT INTO tnode_cats 
SELECT fromCAT
FROM measurements;

INSERT INTO tnode_cats
SELECT toCAT
FROM measurements;

INSERT INTO tgraph_nodes (cat) 
SELECT DISTINCT cat
FROM tnode_cats ;

DROP TABLE tnode_cats;

/* Insert source nodes */
INSERT INTO tgraph_nodes (edge_cat, nType, intensity)
SELECT l.cat, 'source', l.measured
FROM measurements as l
WHERE l.measured > 0 AND l.directed = 'F';

/* Insert sink nodes */
INSERT INTO tgraph_nodes (edge_cat, nType, intensity)
SELECT l.cat, 'sink', -l.measured
FROM measurements as l
WHERE l.measured > 0 AND l.directed = 'F';


/* Insert source nodes for bidirectional links*/
INSERT INTO tgraph_nodes (edge_cat, nType, intensity)
SELECT l.cat, 'srcF', l.measured/2
FROM measurements as l
WHERE l.measured > 0 AND l.directed = 'B';
INSERT INTO tgraph_nodes (edge_cat, nType, intensity)
SELECT l.cat, 'srcT', l.measured/2
FROM measurements as l
WHERE l.measured > 0 AND l.directed = 'B';

/* Insert sink nodes for bidirectional links*/
INSERT INTO tgraph_nodes (edge_cat, nType, intensity)
SELECT l.cat, 'snkF', -l.measured/2
FROM measurements as l
WHERE l.measured > 0 AND l.directed = 'B';
INSERT INTO tgraph_nodes (edge_cat, nType, intensity)
SELECT l.cat, 'snkT', -l.measured/2
FROM measurements as l
WHERE l.measured > 0 AND l.directed = 'B';

/* POPULATE EDGES */
create index i_tgraph_nodes_cat_ntype on tgraph_nodes ( cat,ntype );

/* Non measured directed F --> T (F) links */
INSERT INTO tgraph_edges (cat, fromID, toID, fClass, multiplicity, lbound)
SELECT l.cat, nf.id, nt.id, l.fClass, 1,l.lbound
FROM measurements as l, tgraph_nodes as nf, tgraph_nodes as nt
WHERE l.fromCAT = nf.cat AND l.toCAT = nt.cat AND nf.nType = 'regular' AND nt.nType = 'regular' AND l.directed = 'F' AND l.measured =-1;

/* Non measured undirected (B) links */
INSERT INTO tgraph_edges (cat, fromID, toID, fClass, multiplicity, lbound)
SELECT l.cat, nf.id, nt.id, l.fClass, 1, l.lbound
FROM measurements as l, tgraph_nodes as nf, tgraph_nodes as nt
WHERE l.fromCAT = nf.cat AND l.toCAT = nt.cat AND nf.nType = 'regular' AND nt.nType = 'regular' AND l.directed = 'B' AND l.measured =-1;

INSERT INTO tgraph_edges (cat, fromID, toID, fClass, multiplicity, lbound)
SELECT l.cat, nt.id, nf.id, l.fClass, 1, l.lbound
FROM measurements as l, tgraph_nodes as nf, tgraph_nodes as nt
WHERE l.fromCAT = nf.cat AND l.toCAT = nt.cat AND nf.nType = 'regular' AND nt.nType = 'regular' AND l.directed = 'B' AND l.measured =-1;

/* Measured directed F --> T (F) links */

INSERT INTO tgraph_edges (cat, fromID, toID, fClass, multiplicity, typ, measured)
SELECT l.cat, nf.id, nt.id, l.fClass, 2, 1 /* SOURCE */, l.measured
FROM measurements as l, tgraph_nodes as nf, tgraph_nodes as nt
WHERE l.cat = nf.edge_cat AND l.toCAT = nt.cat AND nf.nType = 'source' AND nt.nType = 'regular' AND l.directed = 'F' AND l.measured > 0;

INSERT INTO tgraph_edges (cat, fromID, toID, fClass, multiplicity, typ, measured)
SELECT l.cat, nf.id, nt.id, l.fClass, 2, 2 /* SINK */, -l.measured
FROM measurements as l, tgraph_nodes as nf, tgraph_nodes as nt
WHERE l.cat = nt.edge_cat AND l.fromCAT = nf.cat AND nf.nType = 'regular' AND nt.nType = 'sink' AND l.directed = 'F' AND l.measured > 0;


/* Measured undirected (B) links */

INSERT INTO tgraph_edges (cat, fromID, toID, fClass, multiplicity, typ, measured)
SELECT l.cat, nf.id, nt.id, l.fClass, 2, 1 /* SOURCE */, l.measured/2
FROM measurements as l, tgraph_nodes as nf, tgraph_nodes as nt
WHERE l.cat = nf.edge_cat AND l.toCAT = nt.cat AND nf.nType = 'srcF' AND nt.nType = 'regular' AND l.directed = 'B' AND l.measured > 0;

INSERT INTO tgraph_edges (cat, fromID, toID, fClass, multiplicity, typ, measured)
SELECT l.cat, nf.id, nt.id, l.fClass, 2, 2 /* SINK */, -l.measured/2
FROM measurements as l, tgraph_nodes as nf, tgraph_nodes as nt
WHERE l.cat = nt.edge_cat AND l.fromCAT = nf.cat AND nf.nType = 'regular' AND nt.nType = 'snkF' AND l.directed = 'B' AND l.measured > 0;

INSERT INTO tgraph_edges (cat, fromID, toID, fClass, multiplicity, typ, measured)
SELECT l.cat, nf.id, nt.id, l.fClass, 2, 1 /* SOURCE */, l.measured/2
FROM measurements as l, tgraph_nodes as nf, tgraph_nodes as nt
WHERE l.cat = nf.edge_cat AND l.fromCAT = nt.cat AND nf.nType = 'srcT' AND nt.nType = 'regular' AND l.directed = 'B' AND l.measured > 0;

INSERT INTO tgraph_edges (cat, fromID, toID, fClass, multiplicity, typ, measured)
SELECT l.cat, nf.id, nt.id, l.fClass, 2, 2 /* SINK */, -l.measured/2
FROM measurements as l, tgraph_nodes as nf, tgraph_nodes as nt
WHERE l.cat = nt.edge_cat AND l.toCAT = nf.cat AND nt.nType = 'snkT' AND nf.nType = 'regular' AND l.directed = 'B' AND l.measured > 0;

copy tgraph_edges to 'h:/Data/verner/data/uk-measured-net' USING DELIMITERS ' ' WITH NULL as '-1';
copy restrictions to 'h:/Data/verner/data/uk-zakazy' USING DELIMITERS ' ' WITH NULL as '-1';

DROP SEQUENCE IF EXISTS tnode_seq CASCADE;
DROP SEQUENCE IF EXISTS tedge_seq CASCADE;
DROP TABLE IF EXISTS tgraph_edges CASCADE;
DROP TABLE IF EXISTS tgraph_nodes CASCADE;
DROP TABLE IF EXISTS tnode_cats CASCADE;
DROP TABLE IF EXISTS measurements;
drop index if exists i_tgraph_edges_id;
drop index if exists i_tgraph_edges_cat;
end transaction;