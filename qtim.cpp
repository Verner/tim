#include <QtGui/QApplication>
#include "mainDialog.h"

int main(int argc, char **argv) { 
  QApplication app( argc, argv);
  
  mainDialog dlg;

  dlg.show();
  
  return app.exec();

}