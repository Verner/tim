#ifndef _flow_H
#define _flow_H

#include <list>
#include <vector>
#include <iosfwd>
#include <string>

#include "defs.h"

class dataSet;

class edgeFlow { 
  private:
    /* Contains
     *    \sum_{i=1}{num_of_contribs} contributedFlow_i
     */
    real totalFlow;
    
    /* Contains
     *    \sum_{i=1}{num_of_contribs} contributedFlow_i \cdot reliability_i
     */
    reliability partRel;

  public:
    edgeFlow(): totalFlow(0), partRel(0) {};
    void addContrib( real fl, reliability rel ) { 
      totalFlow += fl;
      partRel += (reliability)fl*rel;
    }

    reliability getRel() const;
    real getFlow() const { return totalFlow; };
    void setFlow( real fl, reliability rel ) { 
      totalFlow=0;partRel=0;
      addContrib( fl, rel );
    }
    
    void avgFlow( const edgeFlow &B );
    friend edgeFlow avgFlow( const edgeFlow &A, const edgeFlow &B );
};

class relFlowVector { 
  private:
    std::vector<edgeFlow> flow;
  
  public:
    relFlowVector( edge size ): flow(size) {};
    real getFlow( edge E ) const { return flow[E].getFlow(); };
    reliability getReliability( edge E ) const { return flow[E].getRel(); };
    void setFlow( edge E, real fl, reliability rel ) { flow[E].setFlow( fl, rel ); };
    void addFlowContribution( edge E, real portion, reliability rel ) { flow[E].addContrib( portion, rel ); };
    void average( const relFlowVector &B );


    void load( const dataSet &dSet );
    /* Updates only the edges which are both in flow
     * and in dSet. Only modifies the flow column of dSet
     * and does not add any rows to it! */
    void save( dataSet& dSet ) const;
};

#endif // _flow_H
