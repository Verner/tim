begin transaction;
CREATE temporary TABLE my_init_edges (
	id INTEGER PRIMARY KEY,
	cat INT8,
	fClass INT2,
	typ INT2 DEFAULT 0, /* 0 ... REGULAR, 
                               1 ... SOURCE, 
                               2 ... SINK,
                               3 ... ARTIFICIAL
                               4 ... INVALID */
	multiplicity INTEGER,
	fromID INTEGER,
	toID INTEGER,
	lbound INTEGER DEFAULT NULL,
	ubound INTEGER DEFAULT NULL,
        measured FLOAT DEFAULT NULL,
	init FLOAT DEFAULT NULL,
	computed FLOAT DEFAULT NULL,
	reliability FLOAT DEFAULT NULL
);
copy my_init_edges from '/tmp/dset-results' using delimiters ' ' WITH NULL AS '-1';
create index i_my_init_edges on my_init_edges( cat );

delete from map_layer_cur_init_data;
insert into map_layer_cur_init_data
select v.gid,
       i.cat, 
       max(i.fClass) as fClass, 
       v.directed as directed, 
       max(i.measured), 
       avg(i.reliability) as avgreliability,
       concatenate( i.reliability || ',' ),
       round(sum(i.init)/max(i.multiplicity)) as intensity,
       concatenate(i.id || ', ') AS edgeIDs,
       concatenate(i.toid || ', ' ) AS toNodeIDs,
       concatenate(i.fromID || ', ' ) AS fromNodeIDs
from my_init_edges as i, vzorek as v
where i.typ != 3 and v.cat = i.cat
group by i.cat,
         v.gid, 
	 v.directed;

delete from map_layer_cur_computation_data;
insert into map_layer_cur_computation_data
select v.gid,
       i.cat, 
       max(i.fClass) as fClass, 
       v.directed as directed, 
       max(i.measured), 
       round(sum(i.init)/max(i.multiplicity)) as intensity
from my_init_edges as i, vzorek as v
where i.typ != 3 and v.cat = i.cat
group by i.cat,
         v.gid, 
	 v.directed;

end transaction;

