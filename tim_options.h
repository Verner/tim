#ifndef _TIM_OPTIONS_H
#define _TIM_OPTIONS_H

#include "options.h"

#define PHASE_COMPUTE   "compute"
#define PHASE_INIT      "init"
#define PHASE_SUMMARY   "summary"
#define PHASE_ID2CAT    "print_id2cat_map"

class tim_options : public options {
public:
  std::string command, data_file, update_file, zakazy_file, dump_prefix, output_file, checkpoint, control_file, cfg_file;
  int epsilon;
  long dumpInterval, zerofactor;
  bool allowDown;
  
  tim_options();
  bool parse(int argc, const char *argv[]); 

};

#endif // _TIM_OPTIONS_H