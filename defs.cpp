#include "defs.h"

void edgeList::append( edgeList &lst ) {
  resize( lst.size()+size() );
  for( edgeList::iterator it = lst.begin(); it != lst.end(); ++it ) {
    push_back( *it );
  }
  lst.resize(0);
}

#ifdef D_EDGE
#include <iostream>

using namespace std;
ostream &operator<<(ostream &OUT, const edge E) { 
  OUT << E.nID;
  return OUT;
}

istream &operator>>(istream &IN, edge &E ) {
  IN >> E.nID;
  return IN;
}

ostream &operator<<(ostream &OUT, const node N) { 
  OUT << N.nID;
  return OUT;
}

istream &operator>>(istream &IN, node &N ) {
  IN >> N.nID;
  return IN;
}



#ifdef D_EDGE

bool valid_node( const node N ) {
  return N.valid();
}

bool valid_edge( const edge E ) {
  return E.valid();
}

#else  // D_EDGE

bool valid_node( const node N ) {
  return (N>=0);
}

bool valid_edge( const edge E ) {
  return (E>=0);
}
#endif // D_EDGE
*/

#endif // D_EDGE
