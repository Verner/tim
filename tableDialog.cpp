#include "tableDialog.h"

#include <QtCore/QDebug>

#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQueryModel>

#include <QtGui/QDialog>




tableDialog::tableDialog(QSqlDatabase* DB, QWidget* parent, Qt::WindowFlags f, const QString table_name):
  QDialog(parent, f),
  db(DB),
  default_table(table_name)
{
  dlg.setupUi(this);
  
  schema_model = new QSqlQueryModel;
  table_model = new QSqlQueryModel;
  column_model = new QSqlQueryModel;
  
  dlg.schemaComboBox->setModel(schema_model);
  dlg.tableComboBox->setModel(table_model);
  
  connect( dlg.schemaComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(schemaChanged()) );
  connect( dlg.tableComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(tableChanged()) );
}

void tableDialog::setModels( const QString schema_name )
{
  schema_model->setQuery("SELECT schema_name FROM information_schema.schemata ORDER BY schema_name");
  default_value( dlg.schemaComboBox, schema_name );
  schemaChanged();
}

void tableDialog::clearModels()
{
  schema_model->clear();
  table_model->clear();
  column_model->clear();
}




QString tableDialog::getTable() const
{
  return dlg.tableComboBox->currentText();
}

QString tableDialog::getSchema() const
{
  return dlg.schemaComboBox->currentText();
}

QString tableDialog::getColumn(int column)
{
  return columns[column].combo->currentText();
}




int tableDialog::addColumn(const QString& name, const QString& description)
{
  col c;
  int ret = columns.size();
  c.default_name = name;
  c.combo = new QComboBox(this);
  c.label = new QLabel(this);
  c.label->setText(description);
  c.combo->setModel(column_model);
  dlg.columnsLayout->addWidget(c.combo, columns.size(),0);
  dlg.columnsLayout->addWidget(c.label, columns.size(),1);
  columns.append(c);
  return ret;
}

void tableDialog::default_value(QComboBox* combo, const QString value)
{
  int default_index = combo->findText(value);
  if ( default_index != -1 ) {
    qDebug() << "Default value" << value << "found.";
    combo->setCurrentIndex( default_index );
  } else {
    qDebug() << "Default value" << value << "not found.";
  }
}

void tableDialog::schemaChanged()
{
  QString schema = dlg.schemaComboBox->currentText();
  if ( schema != curSchema || curSchema == "") { 
    qDebug() << "schemaChanged(): From schema:" << curSchema << " to " << schema;
    curSchema = schema;
    table_model->setQuery("SELECT table_name FROM information_schema.tables WHERE table_schema='"+schema+"' ORDER BY table_name");
    default_value( dlg.tableComboBox, default_table);
  }
}

void tableDialog::tableChanged()
{
  QString table = dlg.tableComboBox->currentText();
  QString schema = dlg.schemaComboBox->currentText();
  QString newTable = schema+"."+table;
  if ( newTable != curTable || curTable == "") {
    qDebug() << "tableChanged(): from table:" << curTable << " to " << schema+"."+table;
    curTable = newTable;
    column_model->setQuery("SELECT column_name FROM information_schema.columns WHERE table_schema='"+schema+"' AND table_name='"+table+"' ORDER BY column_name");
    foreach( col c, columns ) { 
      default_value( c.combo, c.default_name );
    }
  }
}




tableDialog::~tableDialog()
{
  foreach( col c, columns ) {
    delete c.combo;
    delete c.label;
  }
  delete schema_model;
  delete table_model;
  delete column_model;
}




