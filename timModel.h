#ifndef _timModel_h
#define _timModel_h

#include <string>
#include <iosfwd>

class graph;
class dataSet;

class timModel {
  private:
    graph *G;
    dataSet *dSet;
    
    std::string control_file;
    
    bool allow_down;

  public:
    timModel();
    void load( const std::string &data_file, const std::string &zakazy_file = "");
    void save( const std::string &data_file ) const;
    
    void setZakazy( const std::string &zakazy_file );
    void setControlFile( const std::string &control_f );

    void init_phase( const std::string &out_file = "");
    void compute_phase(long long epsilon = 500, long long dumpInterval = 1000000, const std::string &dump_prefix = "dump" );
    void restartFromCheckPoint( const std::string& checkpoint_file, long long dumpInterval = -1, const std::string& dump_prefix = "");
    void update( const std::string &old_data );
    
    friend std::ostream &operator<<( std::ostream &OUT, const timModel &model );
    friend std::istream &operator>>( std::istream &IN, timModel &model );
    
    void allowDown() { allow_down = true; };

};

#endif // _timModel_h
