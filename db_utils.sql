CREATE OR REPLACE FUNCTION catenate(text,text) RETURNS text AS '
      SELECT COALESCE($1 || $2,$1,$2,NULL)
   ' LANGUAGE SQL;

CREATE AGGREGATE concatenate (
      sfunc = catenate,
      basetype = text,
      stype = text,
      initcond = ''
   );