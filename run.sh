#!/bin/bash
# run.sh -- created 2009-06-04, Jonathan Verner
# @Last Change: 24-Dec-2008.
# @Revision:    0.0

INPUT=$PWD/$1;
OUT=$INPUT.out;
GRAPH=$INPUT/connections.txt
MEASUREMENTS=$INPUT/measurements.txt
COORDINATES=$INPUT/nodes.txt
DUMP_INTERVAL=100
EPSILON=100
AVOID_ZERO_FACTOR=10000
ZOOM_FACTOR=10
ANIMATION_SPEED=1

CONVERT_PROG=$PWD/build/convert
MIN_COST_FLOW=$PWD/build/minCostFlow
PAINT=$PWD/build/paint

mkdir $INPUT.work;
cd $INPUT.work;
mkdir dumps;
mkdir pics;
mkdir output;

$CONVERT_PROG -g $GRAPH > g.IN;
$CONVERT_PROG -g $GRAPH -m $MEASUREMENTS > m.IN;
$CONVERT_PROG -g $GRAPH -c $COORDINATES > coords;
$MIN_COST_FLOW -g g.IN -m m.IN -e $EPSILON -z $AVOID_ZERO_FACTOR -d $DUMP_INTERVAL -f dumps/dump > m.OUT;
#$CONVERT_PROG -g ../$GRAPH -s m.OUT > output/solution
for i in dumps/dump.*; do 
  cat $i | $PAINT -d -c coords -o pics/`basename $i | sed -e's/\.//g'`.jpg -z $ZOOM_FACTOR; 
done;
cd pics;
echo dump* | sed -e's/dump//g' | sed -e's/.jpg/\n/g' | sort -n | sed -e's/\([0-9][0-9]*\)/dump\1.jpg/g' | xargs jpeg2swf -r 1 -o ../output/animation.swf;
cd ..
cat m.OUT | $PAINT -d -c coords -o output/final.jpg -z $ZOOM_FACTOR;
cat dumps/dump.0 | $PAINT -d -c coords -o output/init.jpg -z $ZOOM_FACTOR;
mv output $OUT;
cd ..


# vi: 
