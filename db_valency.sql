begin transaction;
create temporary table temp_nodes (
 nodeid int8
);

insert into temp_nodes
select fromCat from connections
union
select toCat from connections;

create temporary table inval (
  nodeid int8,
  val integer
);

create temporary table outval (
  nodeid int8,
  val integer
);

insert into inval
select n.nodeid, count( c.tocat ) 
from connections as c right join temp_nodes as n ON  n.nodeid = c.tocat
group by n.nodeid;

insert into outval
select n.nodeid, count( c.fromcat )
from connections as c right join temp_nodes as n ON n.nodeid = c.fromcat
group by n.nodeid;

delete from valency;

insert into valency 
select i.nodeid, i.val, o.val
from inval as i, outval as o
where i.nodeid = o.nodeid;

delete from dead_ends;
insert into dead_ends 
select e.cat, false from valency as v, connections as e
where v.invalency=0 and v.outvalency = 1 and e.fromcat = v.cat and e.fclass < 5
      and e.directed != 'B'
union 
select e.cat, true from valency as v, connections as e
where v.invalency=1 and v.outvalency = 0 and e.tocat = v.cat and e.fclass < 5
      and e.directed != 'B';
end transaction;
  