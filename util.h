#ifndef UTIL_H
#define UTIL_H

#include <string>
#include <iostream>
#include <sstream>
#include <list>
#include <stdlib.h>

#include "defs.h"

const bigreal fp_error_margin = 0.0001;



template<typename contType> void printList( std::list<contType> lst ) {
  for(typename std::list<contType>::iterator it = lst.begin(); it != lst.end(); ) { 
    std::cout << *it;
    ++it;
    if ( it != lst.end() ) std::cout <<",";
  }
}

template<class realType> realType str2real( const std::string &s ) {
    realType hi=0, low=0;
    int i;
    for( i = s.size()-1; i >= 0 && s[i] >= '0' && s[i] <= '9'; --i ) {
        hi+=s[i]-'0';
        low+=s[i]-'0';
        hi*=10;
        low/=10;
    }
    low*=10;
    if ( i < 0 || s[i] != '.' ) return hi/10;
    hi = 0;
    --i;
    for(--i;i>=0 && s[i] >='0' && s[i] <='9'; --i ) {
        hi+=s[i]-'0';
        hi*=10;
    }
    return hi/10+low;
}

template<class integralType> integralType str2int( const std::string &s ) {
    integralType e = 0;
    for(int i=s.size()-1;i > 0 && s[i] >= '0' && s[i] <= '9'; --i) {
        e+=s[i]-'0';
        e*=10;
    }
    return e/10;
}

template<class numberType> inline numberType EXP( const numberType base, const int ex ) { 
  numberType ret = 1;
  for(int i = ex; i > 0; --i)
    ret*=base;
  return ret;
}

template<class numberType> inline numberType ABS( const numberType A ) { 
  if ( A>=0 ) return A;
  return -A;
}

template<class numberType> inline numberType MAX( const numberType A, const numberType B ) {
    if ( A <= B ) return B;
    return A;
}

template<class numberType> inline numberType MIN( const numberType A, const numberType B ) {
    if ( A <= B ) return A;
    return B;
}

template<class numberType> std::string number2Str( const numberType A ) { 
  std::stringstream ss;
  ss << A;
  return ss.str();
}

/* Computes the average of the values valA, valB weighted according
 * to the reliability of valA and valB respectively and computes
 * the reliability of this average */
void REL_AVG( real valA, real relA, real valB, real relB, real &tval, real &trel );


/* Computes the sum of the values valA, valB and 
 * the reliability of the sum */
void REL_SUM( real valA, real relA, real valB, real relB, real &tval, real &trel );

/* Computes the maximum of valA and valB and its reliability
 * based on the reliability of valA and valB */
void REL_MAX( real valA, real relA, real valB, real relB, real &maxVal, real &maxRel );


/* Computes the reliability of valA*weight (and the product valA*weight) */
void REL_WEIGHT( real valA, real relA, real weight, real &tval, real &trel );

/* Does safe floating point number equality check (up to error_margin) */
inline bool FP_EQ( const bigreal A, const bigreal B, const bigreal error_margin = fp_error_margin) { 
  return ( A>=B-error_margin && A<=B+error_margin);
}
/* Does safe floating point number <= check (up to error_margin) */
inline bool FP_LEQ( const bigreal A, const bigreal B, const bigreal error_margin = fp_error_margin) { 
  return ( A<=B+error_margin );
}
/* Does safe floating point number < check (up to error_margin) */
inline bool FP_LE( const bigreal A, const bigreal B, const bigreal error_margin = fp_error_margin) { 
  return ( A<B+error_margin );
}

/* Does safe floating point number < check (< true even after taking
 * the error_margin into account) */
inline bool FP_SLE( const bigreal A, const bigreal B, const bigreal error_margin = fp_error_margin) { 
  return ( A+error_margin<B );
}

inline bool random_bit( const real factor ) {
  return FP_LEQ( rand(), factor*RAND_MAX );
}

bool file_exists( const std::string &fname );
void touch( const std::string &fname );
void rm( const std::string &fname );

std::iostream &operator<<(std::iostream &OUT, const edgeList &lst);

std::string ltrim( const std::string& str, int num );
std::string rtrim( const std::string& str, int num );
bool toNum( const std::string& number, real &ret );
bool toBool ( const std::string& str, bool &val );
bool split( const std::string& str, std::string& left, std::string& right, char split = '=' );
char toUpper( const char c );
std::string toUpper( const std::string& str );
bool notDigit( const char s );
int dig2num( const char s );


#endif // UTIL_H
