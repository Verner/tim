#include "test.h"
#include "config.h"
#include "util.h"

#include <fstream>


bool testToUpper() { 
  bool ret = true;

  TESTCOND( toUpper('c') == 'C', "testToUpper(): Failed to convert 'c'." )
  TESTCOND( toUpper('C') == 'C', "testToUpper(): Failed to convert 'C'." )
  TESTCOND( toUpper('a') == 'A', "testToUpper(): Failed to convert 'a'.")
  TESTCOND( toUpper('A') == 'A', "testToUpper(): Failed to convert 'A'." )
  TESTCOND( toUpper('z') == 'Z', "testToUpper(): Failed to convert 'z'." )
  TESTCOND( toUpper('Z') == 'Z', "testToUpper(): Failed to convert 'Z'." )
  TESTCOND( toUpper('1') == '1', "testToUpper(): Failed to convert '1'." )
  
  TESTCOND( toUpper("ahoj1") == "AHOJ1", "testToUpper(): Failed to convert 'ahoj1'." )
  TESTCOND( toUpper("Ahoj1") == "AHOJ1", "testToUpper(): Failed to convert 'Ahoj1'." )
  TESTCOND( toUpper("xAhOj1") == "XAHOJ1", "testToUpper(): Failed to convert 'xAhOj1'." )
  TESTCOND( toUpper("xAhOj1") == "XAHOJ1", "testToUpper(): Failed to convert 'xAhOj1'." )
  
  return ret;
}

bool testSplit() { 
  bool ret = true;
  
  std::string left, right;
  bool conversion;
  
  conversion = split( "a=b", left, right );
  TESTCOND( (left == "a" && right == "b" && conversion), ("testSplit(): Failed 'a=b' with l:" + left + "; r:" + right) );
  
  conversion = split( "abc=befs", left, right );
  TESTCOND( (left == "abc" && right == "befs" && conversion), ("testSplit(): Failed 'abc=befs' with l:" + left + "; r:" + right) );
  
  conversion = split( "ab", left, right );
  TESTCOND( (! conversion), "testSplit(): Failed 'ab'" );
  
  conversion = split( "a=", left, right );
  TESTCOND( (left == "a" && right == "" && conversion), ("testSplit(): Failed 'a=' with l:" + left + "; r:" + right) );
  
  conversion = split( "a,b", left, right, ',' );
  TESTCOND( (left == "a" && right == "b" && conversion), ("testSplit(): Failed 'a,b' with l:" + left + "; r:" + right) );
  
  conversion = split( "a=b", left, right, '=' );
  TESTCOND( (left == "a" && right == "b" && conversion), ("testSplit(): Failed 'a=b' with l:" + left + "; r:" + right) );
  
  return ret;
  
}

bool testToBool() { 
  bool ret = true;
  
  bool val, conversion;
  
  conversion = toBool( "False", val );
  TESTCOND( ( conversion && !val ), "testToBool(): Failed for 'False'" );
  
  conversion = toBool( "FALSE", val );
  TESTCOND( ( conversion && !val ), "testToBool(): Failed for 'FALSE'" );
  
  conversion = toBool( "false", val );
  TESTCOND( ( conversion && !val ), "testToBool(): Failed for 'false'" );
  
  conversion = toBool( "nO", val );
  TESTCOND( ( conversion && !val ), "testToBool(): Failed for 'nO'" );
  
  conversion = toBool( "0", val );
  TESTCOND( ( conversion && !val ), "testToBool(): Failed for '0'" );
  
  conversion = toBool( "true", val );
  TESTCOND( ( conversion && val ), "testToBool(): Failed for 'true'" );
  
  conversion = toBool( "TRUE", val );
  TESTCOND( ( conversion && val ), "testToBool(): Failed for 'TRUE'" );
  
  conversion = toBool( "tRuE", val );
  TESTCOND( ( conversion && val ), "testToBool(): Failed for 'tRuE'" );
  
  conversion = toBool( "Yes", val );
  TESTCOND( ( conversion && val ), "testToBool(): Failed for 'Yes'" );
  
  conversion = toBool( "1", val );
  TESTCOND( ( conversion && val ), "testToBool(): Failed for '1'" );
  
  conversion = toBool( "false ", val );
  TESTCOND( ( ! conversion ), "testToBool(): Failed for 'false '" );
  
  return ret;
}
  
long long readNumberToNextNonDigit( const std::string& number, uint& pos );

bool testToNonDigit() { 
  bool ret = true;
  uint pos = 0;
  
  TESTCOND( readNumberToNextNonDigit("-1.231e10", pos) == -1, "testToNonDigit(): Fail at '-'" );
  pos++;
  TESTCOND( readNumberToNextNonDigit("-1.231e10", pos) == 1 && pos == 2, "testToNonDigit(): Fail at '1.'" );
  pos++;
  TESTCOND( readNumberToNextNonDigit("-1.231e10", pos) == 231 && pos == 6, "testToNonDigit(): Fail at '231e'" );
  pos++;
  TESTCOND( readNumberToNextNonDigit("-1.231e10", pos) == 10 && pos == 9, "testToNonDigit(): Fail at '10'");
  
  return ret;
}  
  
  
bool testToNum() { 
  bool ret = true;  
  real val;

  TESTCOND( toNum( "1.231", val ) && FP_EQ(val, 1.231), "testToNum(): Failed to convert 1.231" );
  TESTCOND( toNum( "-231", val ) && FP_EQ(val, -231), "testToNum(): Failed to convert -231" );
  TESTCOND( toNum( "231", val ) && FP_EQ(val, 231), "testToNum(): Failed to convert 231" );
  TESTCOND( toNum( "1.0231", val ) && FP_EQ(val, 1.0231), "testToNum(): Failed to convert 1.0231" );
  TESTCOND( toNum( "-1.231", val ) && FP_EQ(val, -1.231), "testToNum(): Failed to convert -1.231" );
  TESTCOND( toNum( "1000000000", val ) && FP_EQ(val, 1000000000), "testToNum(): Failed to convert 1000000000" );
  
  // Invalid characters
  TESTCOND( ! toNum( "-1.x231", val ), "testToNum(): Converted an invalid number 1.x231" );
  TESTCOND( ! toNum( "1000000000x", val ), "testToNum(): Converted an invalid number 1000000000x" );
  
  // Scientific Notation
  TESTCOND( toNum("1e-02",val) && FP_EQ(val, 0.01), "testToNum(): Failed to convert 1e-02" )
  TESTCOND( toNum("123.5e-02",val) && FP_EQ(val, 1.235), "testToNum(): Failed to convert 123.5e-02" )
  TESTCOND( toNum("123.5e+02",val) && FP_EQ(val, 12350), "testToNum(): Failed to convert 123.5e+02" )
  TESTCOND( toNum("-123.5e+02",val) && FP_EQ(val, -12350), "testToNum(): Failed to convert -123.5e+02" )
  TESTCOND( toNum("-123.5e-02",val) && FP_EQ(val, -1.235), "testToNum(): Failed to convert -123.5e-02" )
  TESTCOND( ! toNum("123.5e02",val), "testToNum(): Converted an invalid number 123.5e02" )
  
  return ret;
}

bool testConfig() { 
  bool ret;
  std::string fName = "/tmp/test_config";
  std::ofstream OUT( fName.c_str() );
  
  OUT << " # This is a comment " << std::endl;
  OUT << "   allow_down = YES " << std::endl;
  OUT << "   \tdown\t               \t=\t       NO" << std::endl;
  OUT << "   decay_flow_1to2 = 0.5 # This is a real number between 0 and 0.4" << std::endl;
  OUT << " dump_base_name = dump " << std::endl;
  OUT << " ########################" << std::endl;
  OUT << std::endl;
  OUT << " test_val = -235 "<< std::endl;
  OUT.flush();
  OUT.close();
  
  config c;
  c.load( fName );
  
  TESTCOND( (c.haveKey("ALLOW_DOWN") && c.haveKey("decay_flow_1to2") && c.haveKey("DUMP_BASE_NAME") && c.haveKey("TEST_VAL")), "testConfig(): Missing some keys." );
  TESTCOND( (c.getBool("ALLOW_DOWN")), "testConfig(): Failed ALLOW_DOWN" );
  TESTCOND( (! c.getBool("down")), "testConfig(): Failed down" );
  TESTCOND( (FP_EQ(c.getNum("DECAY_FLOW_1TO2"),0.5)), "testConfig(): Failed DECAY_FLOW_1TO2" );
  TESTCOND( (FP_EQ(c.getNum("test_val"), -235)), "testConfig(): Failed test_val" );
  TESTCOND( (c.getStr("DUMP_BASE_NAME") == "dump"), "testConfig(): Failed DUMP_BASE_NAME" );
  
  TESTCOND( (c.getBool("crash",true)), "testConfig(): Failed crash" );
  TESTCOND( (FP_EQ(c.getNum("epsilon",1000),1000)), "testConfig(): Failed epsilon" );
  
  rm( fName );
  
  return ret;
}

int main(int, char **) { 
  bool ret = true;
  
  TEST(testToUpper)
  TEST(testToNum)
  TEST(testSplit)
  TEST(testToBool)
  TEST(testConfig)
  TEST(testToNonDigit)
  
  if ( ! ret ) return 1;
  return 0;
}
