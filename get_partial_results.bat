@echo off
set /p schemaName= Schema:
set /p tableName= Load results into table:


if exist dump_finished del dump_finished
if exist temporary_results del temporary_results

echo 1 > %schemaName%-my_control
echo %schemaName%-temporary_results >> %schemaName%-my_control
echo -1 >> %schemaName%-my_control
echo -1 >> %schemaName%-my_control
echo -1 >> %schemaName%-my_control
copy %schemaName%-my_control %schemaName%-control

:WAIT_RESULTS
if exist %schemaName%-control-dump_finished goto CONTINUE
goto WAIT_RESULTS

:CONTINUE
del %schemaName%-control-dump_finished
del %schemaName%-control

w64-build\tim.exe -p summary -d %schemaName%-temporary_results -o %schemaName%-summary_table



sql\summary_sql.bat %schemaName% %tableName% | psql -U postgres mop

del %schemaName%-summary_table
del %schemaName%-my_control
del %schemaName%-temporary_results