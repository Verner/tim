/***************************************************************
 * initState.cpp
 * @Author:      Jonathan Verner (jonathan.verner@matfyz.cz)
 * @License:     GPL v2.0 or later
 * @Created:     2009-06-21.
 * @Last Change: 2009-06-21.
 * @Revision:    0.0
 * Description:
 * Usage:
 * TODO:
 *CHANGES:
 ***************************************************************/

#include "initState.h"

#include "flow.h"
#include "sourceSinkList.h"
#include "graph.h"
#include "dataSet.h"

#include "util.h"

#include "global_config.h"

#include <iostream>
#include <fstream>

using namespace std;


initState::initState( graph* Graph, int mFlow, real relDec ): 
  G(Graph), minFlow(mFlow), relDecay(relDec), flVector( G->numOfEdges() )
{
#ifndef VERSION_1
  decayMatrix[0] = global_config.getNum(FC1_DECAY_OPTION);
  decayMatrix[1] = global_config.getNum(FC2_DECAY_OPTION);
  decayMatrix[2] = global_config.getNum(FC3_DECAY_OPTION);
  decayMatrix[3] = global_config.getNum(FC4_DECAY_OPTION);
  decayMatrix[4] = global_config.getNum(FC5_DECAY_OPTION);
#endif
}

#ifndef VERSION_1
real initState::decayFClass(real flow, short unsigned int fromClass, short unsigned int toClass) const
{
      return (decayMatrix[(toClass-1)]/decayMatrix[(fromClass-1)])*flow;
}
#endif


bool initState::wantMinimalFlow( edge E, dirT direction, reliability relTreshold ) const {
#ifdef VERSION_1
  node srcNode;
  if ( direction == FORWARD ) srcNode = G->fromNode( E );
  else srcNode = G->toNode( E );
  int fClass = G->getMinFClass( srcNode, direction );
  edgeList importantEdges = G->nbrEdges( srcNode, fClass, direction );
  edgeList allEdges = G->nbrEdges( srcNode, direction );
  if ( importantEdges.size() < 2 ) return false;
  return ( getReliability(E) < relTreshold );
#else
  return ( getReliability(E) < relTreshold );
#endif
}

real initState::calculateLBound( edge E, dirT direction ) const {
#ifndef VERSION_1
  real guesstimate = 0;
  if (E < guesstimate_flow.size()) guesstimate = guesstimate_flow[E];
#endif
  node srcNode;
  if ( direction == FORWARD ) srcNode = G->fromNode( E );
  else srcNode = G->toNode( E );
  int fClass = G->getMinFClass( srcNode, direction );
  edgeList importantEdges = G->nbrEdges( srcNode, fClass, direction );
  real totalFlow = 0;
  for( edgeList::iterator eIt = importantEdges.begin(); eIt != importantEdges.end(); ++eIt )
    totalFlow += flVector.getFlow( *eIt );
  if ( G->getFClass( E, direction ) == fClass ) {
    int sz = importantEdges.size()-1;
#ifdef VERSION_1    
    if ( sz == 0 ) return 0;
    else return totalFlow/sz;
#else
    if ( sz == 0 ) return guesstimate;
    else return MAX<real>(totalFlow/sz, guesstimate);
#endif
  } else {
#ifdef VERSION_1
    return (decayFClass( totalFlow, (short unsigned int) fClass, (short unsigned int) G->getFClass( E, direction ) )/2);
#else
    return MAX<real>((decayFClass( totalFlow, (short unsigned int) fClass, (short unsigned int) G->getFClass( E, direction ) )/2),guesstimate);
#endif
  }
}

void initState::mergeState( const initState &s ) {
  flVector.average( s.flVector );
  //needMinimumForward.insert( needMinimumForward.begin(), s.needMinimumForward.begin(), s.needMinimumForward.end() );
  //needMinimumBackward.insert( needMinimumBackward.begin(), s.needMinimumBackward.begin(), s.needMinimumBackward.end() );
}



void initState::ensureMinimalFlow( reliability relTreshold ) {
  real lBound;
  long step = G->numOfEdges()/25;
  for(edge e = G->numOfEdges()-1; e>=0; --e) {
    if ( wantMinimalFlow(e, FORWARD, relTreshold) ) {
      lBound = calculateLBound( e, FORWARD );
      recursive_ensure_minimum_flow( e, FORWARD, lBound, relTreshold, 60 );
    }
    if ( wantMinimalFlow(e, BACKWARD, relTreshold) ) {
      lBound = calculateLBound( e, BACKWARD );
      recursive_ensure_minimum_flow( e, BACKWARD, lBound, relTreshold, 60 );
    }
    if ( e % step == 0 ) cerr << "*";
  }
  cerr << endl;
  return;
  /*
  for(edgeList::iterator e = needMinimumForward.begin(); e != needMinimumForward.end(); ++e ) {
    lBound = calculateLBound( *e, FORWARD );
    recursive_ensure_minimum_flow( *e, FORWARD, lBound, relTreshold, 60 );
    step++;
    if ( step % 10000 == 0 ) cerr << "*";
  }
  step=0;
  for(edgeList::iterator e = needMinimumBackward.begin(); e != needMinimumBackward.end(); ++e ) {
    lBound = calculateLBound( *e, BACKWARD );
    recursive_ensure_minimum_flow( *e, BACKWARD, lBound, relTreshold, 60 );
    step++;
    if ( step % 10000 == 0 ) cerr << "*";
  }
  cerr << endl;*/
}

void initState::recursive_ensure_minimum_flow( edge E, dirT direction, real lBound, reliability relTreshold, int depth ) {

  if ( FP_SLE( relTreshold, getReliability( E ) ) || FP_LEQ( lBound, minFlow ) || FP_LEQ( lBound, getFlow( E ) ) || depth < 0 ) return;
  depth--;

  /* Update the flow along edge E */
  flVector.setFlow( E, lBound, (reliability) relTreshold );

   /* Initialize local variables */
  int fClass = G->getFClass( E, direction );
  node nextN;
  if ( direction == FORWARD ) nextN = G->toNode( E );
  else nextN = G->fromNode( E );
  edgeList nextEdges = G->nbrEdges( nextN, fClass, direction );

  /* Compute the proportions of various edges */
  list<real> proportionList = getProportions( nextEdges );

  /* Recursively ensure the appropriate proportion of minimal flow 
   * along next edges */
  edgeList::iterator eIt = nextEdges.begin();
  list<real>::iterator propIt = proportionList.begin(); 
  for( ; eIt != nextEdges.end(); ++eIt, ++propIt )
    recursive_ensure_minimum_flow( *eIt, direction, lBound*(*propIt), relTreshold, depth );
}

void initState::recursive_distribute_flow( edge source, ofstream &outSRC, edge E, dirT direction, real flow, reliability rel, int depth, int cross_net ) { 
  
//  outSRC << source << E << direction << flow << rel << depth << endl;
  
  #ifdef DEBUG
    if ( E == 3222759 || E == 3222749 || E == 3222599 || E == 3222444 || E == 3222189 ) { 
      cerr << endl;
      cerr << "Source " << source << " Edge " << E  << " Distribute flow: " << flow << " Reliability: " << rel << " Depth: " << depth <<  endl;
      cerr << "  BEFORE => Flow: " << flVector.getFlow(E) << " Rel: " << flVector.getReliability(E) << endl;
    }
  #endif

  /* Prevent infinite recursion */
  if ( FP_LE( flow, 10 ) || depth < 0 ) return;
  depth--;
  /* Update the flow along edge E */
  flVector.addFlowContribution( E, flow, rel );

  #ifdef DEBUG
    if ( E == 3222759 || E == 3222749 || E == 3222599 || E == 3222444 || E == 3222189 ) { 
      cerr << endl;
      cerr << "  AFTER => Flow: " << flVector.getFlow(E) << " Rel: " << flVector.getReliability(E) << endl;
    }
  #endif  
  
  
  /* Initialize local variables */
  int fClass = G->getFClass( E, direction ); // The fclass of the current edge
  node nextN; // The node where we will move to;
  if ( direction == FORWARD ) nextN = G->toNode( E );
  else nextN = G->fromNode( E );
  edgeList nextEdges = G->nbrEdges( nextN, fClass, direction ); // The edges where we will distribute flow
  if ( nextEdges.size() == 0 ) {
    /* If there are no same-class edges, look at all edges. */
    nextEdges = G->nbrEdges( nextN, direction );
  }

  /* Get the new reliability */
  reliability new_rel = decayReliability( rel, nextEdges, nextN, direction );

  /* Compute the proportions of various edges */
  list<real> proportionList = getProportions( nextEdges );


  edgeList lst, minList;
  lst = G->nbrEdgesGE( nextN, fClass, direction );
  real restFlow = flow;
  if ( cross_net >= 1 ) {
  for( edgeList::iterator e = lst.begin(); e != lst.end(); ++e ) {
    if ( G->isSingle( *e, direction ) ) {
      real distFlow = flow*EXP<real>(0.3, G->getFClass( *e, direction ) - fClass);
      recursive_distribute_flow( source, outSRC,  *e, direction, distFlow, new_rel, depth, cross_net-1);
      restFlow -=distFlow;
    } else minList.push_back( *e );
  }
  /* Adds edges in the direction of distribution (@direction) whose fClass is
   * less important then the fClass of E to the list of sideEdges */
  needMinimalFlow( minList, direction ); // NOTE: Currently does nothing
  } else {
    needMinimalFlow( lst, direction ); // NOTE: Currently does nothing
  }

  /* Adds coincidential edges to the list of sideEdges and 
   * distribute flow along coincidental one-way edges [note that
   * this may lead to inconsistencies if the coincidental edge
   * will be initialized in the same phase --- this is not easily
   * solvable and it is hoped that not many such situations will appear
   * in practise] 
   * NOTE: Currently this does nothing !!!
   */
  lst = G->nbrEdges( nextN, invert( direction ) );
  if ( cross_net >= 1 ) { 
  for( edgeList::iterator e = lst.begin(); e != lst.end(); ++e ) {
    if ( G->getFClass( *e, invert(direction) ) > fClass && G->isSingle( *e , invert(direction) ) ) {
      real distFlow = flow*EXP<real>(0.3, G->getFClass( *e, invert(direction) ) - fClass);
      recursive_distribute_flow(source, outSRC, *e, invert(direction), distFlow, new_rel, depth, cross_net - 1 );
      restFlow+=distFlow;
    } else minList.push_back( *e );
  }
  flow = restFlow;
  needMinimalFlow( minList, invert( direction ) );
  } else needMinimalFlow( lst, invert(direction) );

  /* Distribute the flow along next edges with the new reliability
   * and the appropriate proportion of the flow */
  edgeList::iterator eIt = nextEdges.begin();
  list<real>::iterator propIt = proportionList.begin(); 
  for( ; eIt != nextEdges.end(); ++eIt, ++propIt )
    recursive_distribute_flow(source, outSRC, *eIt, direction, flow*(*propIt), new_rel, depth, cross_net );

}

void initState::distributeFlow( const sourceSinkList *data, dirT direction, int depth ) {
  edge E;
  real fl;
  long num = 0;
  ofstream outSRC("sources");
  for( list<ssink>::const_iterator it = data->begin( direction ); it != data->end( direction ); ++it ) {
    E = it->first;
    fl = ABS<real>(it->second);

#ifdef VERSION_1    
    recursive_distribute_flow(E, outSRC, E, direction, fl, (reliability) 1.0, depth);
#else
    recursive_distribute_flow(E, outSRC, E, direction, fl, (reliability) 1.0, depth, (int) global_config.getNum(CROSS_FCLASS_NETWORK_OPTION));
#endif
    num++;
    if (num % 1000 == 0 ) cerr << "*";
  }
  cerr << endl;
}

void initState::distributeSources(const sourceSinkList* data) {
#ifdef VERSION_1
      distributeFlow( data, FORWARD );
#else
      distributeFlow( data, FORWARD, (int) global_config.getNum(INIT_FLOW_DEPTH_OPTION) );
#endif
}

void initState::distributeSinks(const sourceSinkList* data) {
#ifdef VERSION_1
      distributeFlow( data, BACKWARD );
#else
      distributeFlow( data, BACKWARD, (int) global_config.getNum(INIT_FLOW_DEPTH_OPTION) );
#endif
}



long initState::testMeasuredSS(const sourceSinkList* data, dirT direction, const dataSet &dSet) const {
  long num_of_inconsistencies=0, severe_inconsistencies=0;
  for( list<ssink>::const_iterator it = data->begin( direction ); it != data->end( direction ); ++it ) {
    edge E = it->first;
    real measuredFlow = ABS<real>(it->second);
    real initFlow = flVector.getFlow(E);
    if ( ! FP_EQ( initFlow, measuredFlow ) || ! FP_EQ(flVector.getReliability(E), 1.0) ) {
      if ( ( (MAX<real>(measuredFlow,initFlow)/MIN<real>(measuredFlow,initFlow)) > 2) && 
           (  MAX<real>(measuredFlow,initFlow) > 1000 ) ) {
        severe_inconsistencies++;

#ifdef DEBUG      
        if ( severe_inconsistencies < 100 ) {
          cerr << "Edge: " << E << "; Reliability: " << flVector.getReliability(E) 
                            << "; Init: " << initFlow
                            << "; Measured: " << measuredFlow
                            << endl;
        }
#else
        cerr << dSet[E].cat << ",";
#endif
      }
      num_of_inconsistencies++;
    }
  }
  if ( num_of_inconsistencies > 0 ) cerr << endl;
  return severe_inconsistencies;
}

void initState::load( const dataSet &dSet ) {
  flVector.load( dSet );
#ifndef VERSION_1
  guesstimate_flow.resize( dSet.numOfEdges() );
  for( vector<dataRow>::const_iterator row = dSet.begin(); row != dSet.end(); ++row ) {
    if ( row->hasTreshold() ) guesstimate_flow[row->id] = MAX<real>(row->treshold,0);
    else guesstimate_flow[row->id] = 0;
  }
#endif
}

void initState::save( dataSet &dSet ) const { 
  flVector.save( dSet );
}



