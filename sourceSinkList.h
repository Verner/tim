#ifndef _sourceSinkList_h
#define _sourceSinkList_h

#include <string>
#include <list>

#include "defs.h"

class graph;
class dataSet;

/* Note that ssink.second is < 0 for
 * sinks ! */
typedef std::pair< edge, real > ssink;

class sourceSinkList {
  private:
    std::list<ssink> sources, sinks;

  public:
    void load( const dataSet &dSet );

    std::list<ssink>::const_iterator beginSrc() const { return sources.begin(); };
    std::list<ssink>::const_iterator endSrc() const { return sources.end(); };
    std::list<ssink>::const_iterator beginSnk() const { return sinks.begin(); };
    std::list<ssink>::const_iterator endSnk() const { return sinks.end(); };
    std::list<ssink>::const_iterator begin(dirT direction) const { if ( direction == FORWARD ) return sources.begin(); else return sinks.begin(); };
    std::list<ssink>::const_iterator end(dirT direction) const { if ( direction == FORWARD ) return sources.end(); else return sinks.end(); };
};

#endif // _sourceSinkList_h
