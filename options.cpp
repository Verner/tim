#include "options.h"
#include "util.h"

#include "config.h"

#include <map>
#include <iostream>
#include <assert.h>

using namespace std;

option::operator std::string () const {
  if ( ! parsed ) return "";
  switch( typ ) {
    case option::number:
      return number2Str<real>(parsedNUMValue);
    case option::boolean:
      if ( parsedBOOLValue ) return "true";
      else return "false";
    case option::string:
      return parsedSTRValue;
  }
  return "";
}


void options::saveToConfig(config* cfg) {
  string cfgLine = "";
  for( map<string,option>::const_iterator it = data.begin(); it != data.end(); ++it )
    if ( it->second.parsed )
      cfg->addConfigLine(it->first+"="+(string) it->second);
}


void options::addOption(const std::string &long_name, const std::string &short_name, const option::optionType typ, const std::string &desc, bool required, const std::string default_value) 
{
  option opt( short_name, desc, typ, required, default_value ) ;
  data[long_name]=opt;
}

std::string splice(std::string str, unsigned int start, int end=-1) { 
  std::string ret;
  if ( end < 0 ) end = str.length() + end+1;
  if ( start > str.length() || start > (uint) end ) return "";
  if ( (uint) end > str.length() ) end = str.length();
  for(uint i = start; i < (uint) end; i++) { 
    ret += str[i];
  }
  return ret;
}



bool startsWith( std::string str, std::string with ) { 
  unsigned int len = with.length();
  if ( str.length() < len ) return false;
  
  for(unsigned int i = 0; i < len; i++ )
    if (str[i] != with[i]) return false;
    
  return true;
}


bool options::isOptionCandidate( const std::string &option_string ) { 
  return startsWith(option_string,"--") || startsWith(option_string,"-");
}

std::map<std::string,option>::iterator options::findOption( const std::string &option_string )  {
  if ( startsWith(option_string,"--") ) {
    return data.find( ltrim(option_string,2) );
  } else if ( startsWith( option_string, "-" ) ) {
    std::string shopt = ltrim(option_string,1);
    std::map<std::string,option>::iterator ret=data.begin();
    while( ret != data.end() ) {
      if ( ret->second.short_form == shopt ) return ret;
      ret++;
    }
    return ret;
  } else return data.end();
}

void options::parse(int argc, const char** argv) throw (parseException)
{
  int pos = 1;
  std::string arg, param;
  while( pos < argc ) {
    arg = argv[pos];
    if ( isOptionCandidate( arg ) ) { 
      std::map<std::string,option>::iterator opt = findOption( arg );
      if ( opt != data.end() ) {
	if ( opt->second.typ == option::boolean ) {
                    opt->second.parsedBOOLValue = true;
                    opt->second.parsed = true;
                } else {
                    if ( pos >= argc ) {
                        options::parseException ex("Option " + arg + " requires a parameter.", parseException::missing_param);
                        throw ex;
                    } else {
                        param = argv[++pos];
                        switch (opt->second.typ) {
                        case option::number:
                            if (! toNum( param, opt->second.parsedNUMValue) ) {
                                options::parseException ex("Option " + arg + " requires a number, got:" + param, parseException::wrong_param_type);
                                throw ex;
                            }
                            opt->second.parsed=true;
                            break;
                        case option::string:
                            opt->second.parsedSTRValue = param;
                            opt->second.parsed=true;
                            break;
                        };
                    };
                };
            } else {
                options::parseException ex("Unknown option " + arg, parseException::unknown_option);
                throw ex;
            }
    }
    pos++;
  }
  for( std::map<std::string,option>::iterator opt = data.begin(); opt != data.end(); opt++ )  {
    if ( ! opt->second.parsed ) {
      if ( opt->second.default_value.length() > 0 ) { 
	switch( opt->second.typ ) { 
	  case option::boolean:
	    if ( opt->second.default_value == "true" ) opt->second.parsedBOOLValue=true;
	    else opt->second.parsedBOOLValue=false;
	    break;
	  case option::string:
	    opt->second.parsedSTRValue = opt->second.default_value;
	    break;
	  case option::number:
	    if ( ! toNum(opt->second.default_value, opt->second.parsedNUMValue) ) {
	      options::parseException ex("Option " + arg + " requires a number, got:" + param, parseException::wrong_param_type);
	      throw ex;
	    }
	    break;
	}
	opt->second.parsed = true;
      } else if ( opt->second.required ) { 
	options::parseException ex("Missing required option "+opt->first,parseException::missing_required_option);
	throw ex;
      }
    }
  }
}

std::string options::helpString() const {
  std::string ret = std::string("Usage: ")+"\n";
  for( std::map<std::string,option>::const_iterator opt = data.begin(); opt != data.end(); opt++ )  {
    ret += "--"+opt->first+","+opt->second.short_form+":  "+opt->second.description + "\n";
  }
  return ret;
}

bool options::getBool(const std::string& long_name) const
{
  std::map<std::string,option>::const_iterator opt = data.find( long_name );
  assert( opt != data.end() && opt->second.typ == option::boolean && opt->second.parsed == true );
  return opt->second.parsedBOOLValue;
}

real options::getNum(const std::string& long_name) const
{
  std::map<std::string,option>::const_iterator opt = data.find( long_name );
  assert( opt != data.end() && opt->second.typ == option::number && opt->second.parsed == true );
  return opt->second.parsedNUMValue;
}

std::string options::getStr(const std::string& long_name) const
{
  std::map<std::string,option>::const_iterator opt = data.find( long_name );
  assert( opt != data.end() && opt->second.typ == option::string && opt->second.parsed == true );
  return opt->second.parsedSTRValue;
}

bool options::parsedOption(const std::string& long_name) const
{
  std::map<std::string,option>::const_iterator opt = data.find( long_name );
  assert( opt != data.end() );
  return opt->second.parsed;
}



