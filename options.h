#ifndef _OPTIONS_H
#define _OPTIONS_H

#include "defs.h"

#include <string>
#include <map>

class config;


class option {
public:
  enum optionType { number, string, boolean };
  bool parsed;
  bool required;
  
  std::string short_form;
  std::string default_value;
  std::string description;
  
  std::string parsedSTRValue;
  real parsedNUMValue;
  bool parsedBOOLValue;
  
  enum optionType typ;
 
public:
  option(): parsed(false), required(false), short_form(""),default_value(""),description(""), typ(option::string) {};
  option( const std::string &shopt, const std::string &desc, enum optionType tp, bool req = false, const std::string &defval=""):
    parsed(false), required(req), short_form(shopt), default_value(defval), description(desc), typ(tp) {};
    
  operator std::string() const;
  
};
  

class options {
  
private:
  std::map<std::string, option> data;
  std::map<std::string,option>::iterator findOption( const std::string &option_string );
  static bool isOptionCandidate( const std::string &option_string );
  
protected:
  void saveToConfig( config *cfg );
  
public:
  
  class parseException {
  public:
    enum errTyp { missing_param, wrong_param_type, unknown_option, missing_required_option };  
    parseException( std::string msg, enum errTyp e ): message(msg), err(e) {};
    std::string message;
    enum errTyp err;
    std::string what() { return message; }
  };

  
  options() {};
  void addOption( const std::string &long_name, const std::string &short_name, const enum option::optionType typ, const std::string &desc, bool required = false, const std::string default_value="" );
  std::string helpString() const;
  
  void parse( int argc, const char **argv ) throw (parseException);
  
  bool parsedOption( const std::string &long_name ) const;
  
  bool getBool( const std::string &long_name ) const;
  real getNum( const std::string &long_name ) const;
  std::string getStr( const std::string &long_name ) const;
  
};
#endif //_OPTIONS_H