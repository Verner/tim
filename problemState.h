#ifndef _problemState_H
#define _problemState_H

/***************************************************************
 * problemState.h
 * @Author:      Jonathan Verner (jonathan.verner@matfyz.cz)
 * @License:     GPL v2.0 or later
 * @Created:     2009-05-22.
 * @Last Change: 2009-05-22.
 * @Revision:    0.0
 * Description: The references point to 
 *
 *                 Authors: Bertsekas, Polymenakos, Tseng: 
 *		   Title: AN C-RELAXATION METHOD FOR SEPARABLE CONVEX COST NETWORK FLOW PROBLEMS
 *		   Journal: Siam J. of Optimization
 *		   Date: Apr. 1995
 *
 *              where the algorithm is described and its correctness is proved.
 * Usage:
 * TODO:
 *CHANGES:
 ***************************************************************/


#include <list>
#include <vector>
#include <map>
#include <iosfwd>

#include "graph.h"
#include "util.h"
#include "costFunction.h"
#include "problem.h"

class dataSet;
class problem;


struct problemStats {
  node numOfNodes;
  edge numOfEdges;
  int maxSurplus;
  long long unsigned int totalSurplus;
  double cost;
  long long unsigned int iteration;
  real totalSurplusFraction;
  node numOfSurplusNodes;
  std::vector<real> surplusFractionHist;
  std::vector<int> surplusHist;
  real maxSurplusFraction;
  long int activeNodes;
  long int maxActive;
  int canDown;
  real cumSurplus;
  int totalNodeFlow;
  int maxNodeFlow;
  
  friend std::ostream &operator<<( std::ostream &OUT, const problemStats &stats );
};


/*
 * 
 * Has the following guarantees:
 *   + all internal data (in particular the surplus vector) 
 *   is consistent and uptodate between calling public methods
 *   + except for the setEpsilon method, all public methods
 *   preserve the e-CS conditions
 *   + the price vector is nondecreasing throughout the life
 *   of the class
 */

class problemState { 
	private:
		graph *G;
		
		std::vector<real> flowVector;

		/* Guaranteed to be non-negative, nondecreasing
		 * throughout the life of the object */
		std::vector<real> priceVector;
		
		/* The surplus vector. After the constructor
		 * is called, should be correct between
		 * any function calls to public problemState methods
		 * NOTE: CAN BE NEGATIVE.
		 * The formula for the surplus of node N is given
		 * by (see eq. 2 p. 2):
		 *
		 *  s(N) = \sum_{E\in IN(N)} flow(E) - \sum_{E\in OUT(N)} + supply(N)
		 *
		 */
		
		std::vector<real> surplusVector;
		
		/* Contains positive values for sources,
		 * negative values for sinks */
		std::vector<real> supplyVector;

		std::vector<costFunction> cost;
		
		/* Guaranteed to contain all nodes with positive surplus 
		 * between calls to increaseFlow, decreaseFlow and updateSurplus
		 * under the following condition:
		 *
		 *     If a node is removed from the list by calling selectActiveNode
	         *     and its surplus is not reduced to zero, it must be put back
		 *     on the list via returnActiveNode.
		 *
		 * NOTE: The list may contain nodes which do not have positive surplus
		 * and it may contain a node more than once. However the selectActiveNode method
		 * is guaranteed to return a node with POSITIVE surplus (or -1 if no such
		 * node is in the list).
		 */
		std::list<node> activeNodes;
		bool activeNodesUpToDate;
		// Maximum size of the activeNodes list
		long maxActive;
		long activeNodesSize;
		void tryAddToActive( node N ) { 
		  if ( activeNodesSize > maxActive ) {
		    activeNodesUpToDate=false;
		    return;
		  }
		  try { 
		    activeNodes.push_back( N );
		    activeNodesSize++;
		  } catch (...) {
		    activeNodesUpToDate=false;
		  }
		}

		real epsilon;

		long long unsigned int iteration;
		
		/* Whether to allow a variation on step 3 (see p. 8) of the algorithm
		 * which allows for active nodes with negative surplus */
		bool allow_price_drop;
		static const long C_DOWN_OPERATIONS_1;
		static const real C_DOWN_OPERATIONS_2;
		real cumulativeSurplus;
		std::vector<integer32> upOperations;
		
		/* We must guarantee that each node becomes active with negative surplus
		 * only a finitely many of times (otherwise we might get an infinite loop)
		 * this function tells us whether this limit is reached (false) or not (true) */


  public:
		long uBoundNegativeOperation() const { 
		  return (long) (C_DOWN_OPERATIONS_1 + C_DOWN_OPERATIONS_2*cumulativeSurplus);
		}
 
		bool canActivateNegative( node N ) const { 
		  return ( allow_price_drop && upOperations[N] < uBoundNegativeOperation() );
		}
		
		void setMaxActiveNodes( long mx ) { maxActive = mx; };
		long getMaxActiveNodes() const { return maxActive; };
  private:
		void tryAddNegativeActive( node N ) {
		  if ( canActivateNegative(N) ) tryAddToActive( N );
		}
		void returnActiveNodeWithNegativeSurplus( node N ) {
		  tryAddNegativeActive(N);
		  cumulativeSurplus-=ABS<real>(surplusVector[N]);
		}
		

		  

	protected:

		/* Returns true iff the epsilon-CS conditions are met for the edge E: M --> N, i.e. iff
		 *
		 * dC_E(flow_E) - epsilon <= p(M) - p(N) <= dC_E(flow_E) + epsilon
		 *
		 * (see eq. 4., p. 3.)
		 */
		bool checkECSEdge( edge E ) const { 
		  real dP = getPrice( G->fromNode(E) ) - getPrice( G->toNode(E) );
		  real dlCs = dlC(E), drCs = drC(E);
		  // Should not be needed !?!
		  if ( FP_LEQ( dlCs-epsilon, dP ) && FP_LEQ( dP, drCs + epsilon ) ) return true;
		  #ifdef D_ECS
		  printNode( G->fromNode(E) );
		  printNode( G->toNode(E) );
		  std::cerr<<"checkECSEdge("<<E<<"): dlCs = "<<dlCs<<"; drCs = "<<drCs<<"; dP = "<<dP<<"; epsilon = "<<epsilon <<";\n";
		  std::cerr.flush();
		  #endif
		  return false;
		}

		/* Checks the e-CS conditions for edges originating from N
		 * or terminating at N */
		bool checkECSNode( node N ) const { 
		  edgeList IN = G->inEdges( N ), OUT = G->outEdges( N );
		  for( edgeList::iterator E = IN.begin(); E != IN.end(); ++E )
		    if ( ! checkECSEdge( *E ) ) return false;
		  for( edgeList::iterator E = OUT.begin(); E != OUT.end(); ++E )
		    if ( ! checkECSEdge( *E ) ) return false;
		  return true;
		}

		/* Returns true iff the epsilon-CS conditions (see eq. 4, p. 3)
		 * are met for the flow-price vector (flowVector, priceVector)
		 */
		bool checkECS() const { 
		  for( edge E = G->numOfEdges()-1; E >= 0; --E )
		    if ( ! checkECSEdge( E )  ) return false;
		  return true;
		}


		void initCosts();
		
		/* Updates the surplus vector for node N 
		 * Modifies: surplusVector
		 */
		void updateSurplus( node N ) {
		  bool candidateActiveUP = FP_LEQ(surplusVector[N], 0),
		       candidateActiveDOWN = FP_LEQ(surplusVector[N], 0);
		  surplusVector[N] = calculateNodeSurplus( N );
		  if ( candidateActiveUP && FP_SLE(0,surplusVector[N]) ) tryAddToActive( N );
		  if ( allow_price_drop && candidateActiveDOWN && FP_SLE(surplusVector[N],0) ) tryAddNegativeActive(N);
		}
		
		bool surplusUpToDate( node N ) const { 
		  return FP_EQ( surplusVector[N], calculateNodeSurplus(N), 0.5 );
		}
		
  public:
		
		/*
		 * Initializes the activeNodes list. After
		 * this call the list is guaranteed to contain
		 * all nodes with positive surplus
		 * Modifies: activeNodes
		 */
		void initActiveNodes() { 
		  if ( ! activeNodes.empty() ) activeNodes.clear();
		  activeNodesSize=0;
		  try {
		  for(node N = G->numOfNodes()-1; N>=0; --N) {
		    if (FP_SLE(0,surplusVector[N])) {
		      if ( maxActive > activeNodesSize ) {
			activeNodes.push_back(N);
			activeNodesSize++;
		      } else {
			activeNodesUpToDate=false;
			return;
		      }
		    }
		    if ( allow_price_drop && FP_SLE(surplusVector[N],0) ) {
		      if ( maxActive > activeNodesSize ) {
			activeNodes.push_back(N);
			activeNodesSize++;
		      } else {
			activeNodesUpToDate=false;
			return;
		      }
		    }
		  }
		  activeNodesUpToDate=true;
		  } catch (...) {
		    activeNodesUpToDate=false;
		  }
		}	  

	public:
		problemState(): G(NULL), 
		                activeNodesUpToDate(false), 
		                maxActive(1000000), activeNodesSize(0),
		                epsilon(1000), 
		                iteration(0),
		                allow_price_drop(false), 
		                cumulativeSurplus(0)
                                {}
		
		/* Loads a problem set. If @G is non-null, assumes that
		 * it is the graph corresponding to the given dataset @dSet
		 * otherwise it constructs one from @dSet.
		 *
		 * The function updates the initial flow vector
		 * based on the row->initialized value, and it
		 * initializes the cost functions. For each edge E
		 * its cost function is given by
		 *
		 *  c_E(flow) = factor_E*(flow-minCostFlow_E)^2
		 *
		 * where mincostFlow_E is the initial flow estimate
		 * (computed from the measurements by the initState::distributeSinks/Sources
		 * and initState::ensureMinimalFlow metods) and factor_E
		 * is inversly proportionate to the reliability of the initial flow 
		 * estimate on the given edge. It is currently given by
		 *
		 *   factor_E = MAX( factorFCLASS, factorRELIABILITY )
		 *
		 * where
		 *
		 *   factorFCLASS = 0.01*fClass(E)^2
		 *
		 * and
		 *
		 *   factorRELIABILITY = reliability(E)^2+0.001;
		 *
		 * and minCostFlow_E is the initial guess for the flow_E
		 * (as determined by the measurements and distributeFlow).
		 *
		 * In particular the cost of the initial flow estimate
		 * is zero and increases quadraticaly when departing from
		 * this estimate.
		 */
		void load( const dataSet &dSet, graph *G = NULL );
		void save( dataSet &dSet ) const;
		
		/* Allows checkpointing/restarting the current computation
		 * (useful for long running computations which fail in the middle)
		 */
		void checkPointSave( std::ostream& OUT ) const;
		void checkPointRestart( std::istream& IN );

		/* Status information */
		bigreal getSurplus( node N ) const { return surplusVector[N];};
		bigreal getPrice( node N ) const { return priceVector[N]; };
		long long int getIteration() const { return iteration; };
		/* Computes the total cost for the
		 * current flow distribution */
		bigreal computeTotalCost() const { 
		  bigreal totalCost=0;
		  for(edge E = G->numOfEdges()-1;E>=0;--E)
		    totalCost+=C(E);
		   return totalCost;
		};



		/* SOLVERS FOR EQUATIONS NEEDED BY THE ALGORITHM 
		 * ALL ARE CONST FUNCTIONS
		 */
		
		/* Returns the maximum POSITIVE delta by which flow  
		 * along edge E can be increased or decreased while
		 * maintaining ECS. 
		 * NOTE: This assumes dP >= dC+epsilon/2 or dP <= dC-epsilon/2
		 * USED: During the flow push operation on unblocked arcs (see step 2., p. 8.)
		 */

		bigreal solveDelta( const node N, const edge E ) const { 
		  if ( G->fromNode(E) == N )
		    return cost[E].solveDeltaOUT( getPrice(N), getPrice(G->toNode(E)), flowVector[E] );
		  else
		    return cost[E].solveDeltaIN( getPrice(G->fromNode(E)), getPrice(N), flowVector[E] );
		};
		
		bigreal solveDeltaDOWN( const node N, const edge E ) const {
		  if ( G->fromNode(E) == N )
		    return cost[E].solveDeltaIN( getPrice(N), getPrice(G->toNode(E) ), flowVector[E] );
		  else
		    return cost[E].solveDeltaOUT( getPrice( G->fromNode(E) ), getPrice(N), flowVector[E] );
		}

		/* Returns the maximum POSITIVE delta by which price
		 * of node N may be increased so as to maintain e-CS
		 * along edge E
		 * USED: During the price rise operation on active arcs (see step 3., p. 8.)
		 */
		bigreal solveIncrease( const node N, const edge E ) const { 
		  if ( G->toNode(E) == N ) { 
		    return cost[E].solveIncreaseIN( getPrice(G->fromNode(E)), getPrice(N), flowVector[E], epsilon ); 
		  } else { 
		    return cost[E].solveIncreaseOUT( getPrice(N), getPrice( G->toNode(E) ), flowVector[E], epsilon );
		  }
		};
		
		/* Returns the maximum POSITIVE delta by which price
		 * of node N may be dropped so as to maintain e-CS
		 * along edge E
		 * USED: During the price drop operation on active arcs (see [a variation on] step 3., p. 8.)
		 */
		bigreal solveDecrease( const node N, const edge E ) const { 
		  if ( G->toNode(E) == N ) { 
		    return cost[E].solveDecreaseIN( getPrice(G->fromNode(E)), getPrice(N), flowVector[E], epsilon ); 
		  } else { 
		    return cost[E].solveDecreaseOUT( getPrice(N), getPrice( G->toNode(E) ), flowVector[E], epsilon );
		  }
		};


		/* Cost computing functions */
		
		/* Returns the cost and left and right derivative of the cost respectively of the edge E
		 * at flow(E).
		 */
		bigreal C( edge E ) const { return cost[E].C(flowVector[E]); };
		bigreal dlC( edge E ) const { return cost[E].dlC(flowVector[E]);};
		bigreal drC( edge E ) const { return cost[E].drC(flowVector[E]);};
		
		/* Sets the epsilon (for the e-CS.conditions) 
		 * NOTE: The caller must guarantee that the e-CS is met
		 */
		void setEpsilon( real E ) {epsilon = E;};

		/* Increases the flow along edge by the amount delta. Updates the
		 * surplus vector (for from and to node of E)
		 * accordingly. 
		 * Modifies: flowVector, surplusVector
		 * NOTE: delta must be positive
		 * NOTE: This decrease must not violate the e-CS conditions
		 */
		void increaseFlow( edge E, bigreal delta ) { 
		  #ifdef D_ALG
		  assert( FP_LE(0,delta) );
		  #endif
		  node from = G->fromNode(E),
		       to   = G->toNode(E);
		  flowVector[E]+=delta;
		  surplusVector[from] -=delta;
		  surplusVector[to] +=delta;
		  if ( FP_SLE(0,surplusVector[to]) ) tryAddToActive(to);
		  if ( allow_price_drop && FP_SLE(surplusVector[from],0) ) tryAddNegativeActive(from);
		  #ifdef D_OPT
		  if ( ! surplusUpToDate(G->fromNode(E)) || ! surplusUpToDate(G->toNode(E)) ) {
		    std::cerr << " surplusVector From=" << surplusVector[G->fromNode(E)] << "!=" << calculateNodeSurplus(G->fromNode(E))<<std::endl;
		    printNode(G->fromNode(E));
		    std::cerr << " surplusVector To=" << surplusVector[G->toNode(E)] << "!=" << calculateNodeSurplus(G->toNode(E))<<std::endl;
		    printNode(G->toNode(E));
                  }
		  assert( surplusUpToDate(G->fromNode( E ) ) );
		  assert( surplusUpToDate(G->toNode( E ) ) );
		  #endif
		  #ifdef D_ECS
		  assert( checkECSEdge( E ) );
		  #endif
		};
		
		/* Decreases the flow along edge E by the amount delta. Updates the
		 * surplus vector (for from and to node of E)
		 * accordingly.
		 * Modifies: flowVector, surplusVector
		 * NOTE: delta must be positive
		 * NOTE: This decrease must not violate the e-CS conditions
		 */
		void decreaseFlow( edge E, bigreal delta ) { 
		  #ifdef D_ALG
		  assert( FP_LE(0, delta) );
		  #endif
		  node from = G->fromNode(E),
		       to   = G->toNode(E);
		  flowVector[E]-=delta;
		  surplusVector[from] +=delta;
		  surplusVector[to] -=delta;
		  if ( FP_SLE(0,surplusVector[from]) ) tryAddToActive(from);
		  if ( allow_price_drop && FP_SLE(surplusVector[to],0) ) tryAddNegativeActive(to);
		  #ifdef D_OPT
		  if ( ! surplusUpToDate(G->fromNode(E)) || ! surplusUpToDate(G->toNode(E)) ) {
		    std::cerr << " surplusVector From=" << surplusVector[G->fromNode(E)] << "!=" << calculateNodeSurplus(G->fromNode(E))<<std::endl;
		    printNode(G->fromNode(E));
		    std::cerr << " surplusVector To=" << surplusVector[G->toNode(E)] << "!=" << calculateNodeSurplus(G->toNode(E))<<std::endl;
		    printNode(G->toNode(E));
                  }
		  assert( surplusUpToDate(G->fromNode( E ) ) );
		  assert( surplusUpToDate(G->toNode( E ) ) );
		  #endif
		  #ifdef D_ECS
		  assert( checkECSEdge( E ) );
		  #endif

		};
	
		/* Returns true iff there are nodes which
		 * MIGHT have positive (or negative, when allow_price_drop is true) 
		 * surplus */
		bool haveActiveNodes() { 
		  if ( ! activeNodes.empty() ) return true;
		  if ( activeNodesUpToDate ) return false;
		  initActiveNodes();
		  return ( ! activeNodes.empty() );
		}
		
		/* Returns a node with positive surplus
		 * (if there is such a node) and deletes
		 * it from the activeNodes list. If there
		 * is no node with positive surplus, returns -1.
		 * Modifies: activeNodes
		 *
		 * NOTE: Assumes the activeNodes list is nonempty.
		 *       Always check this by calling haveActiveNodes
		 *       before.
		 *
		 * NOTE: If the surplus of the returned node
		 *       is not decreased to zero or less, be sure
		 *       to return it to the list by calling 
		 *       returnActiveNode.
		 */
		node selectActiveNode() { 
		  assert(! activeNodes.empty() );
		  node ret = activeNodes.front();
		  if ( allow_price_drop ) {
		    while( FP_EQ(surplusVector[ret], 0) && ! activeNodes.empty() ) {
		      activeNodes.pop_front();
		      activeNodesSize--;
		      ret = activeNodes.front();
		    }
		    activeNodes.pop_front();
		    activeNodesSize--;
		    if ( ! FP_EQ(surplusVector[ret],0) ) return ret;
		  } else {
		    while( FP_LE(surplusVector[ret], 0) && ! activeNodes.empty() ) {
		      activeNodes.pop_front();
		      activeNodesSize--;
		      ret = activeNodes.front();
		    }
		    activeNodes.pop_front();
		    activeNodesSize--;
		    if ( FP_SLE(0,surplusVector[ret]) ) return ret;
		  }
		  if ( activeNodesUpToDate ) return -1;
		  initActiveNodes();
		  return selectActiveNode();
		}
		
		/* Checks if a given node has positive surplus
		 * and adds it to the activeNodes list 
		 * Modifies: activeNodes
		 */
		void returnActiveNode(node N) { 
		  if ( FP_SLE(0,surplusVector[N]) ) {
		    tryAddToActive(N);
		    if ( allow_price_drop ) cumulativeSurplus-=ABS<real>( surplusVector[N] );
		  } else if ( allow_price_drop && FP_SLE(surplusVector[N],0) ) returnActiveNodeWithNegativeSurplus(N);
		}
		
		
		/* Increases the price of node N by the amount delta. 
		 * NOTE: delta must be positive
		 * NOTE: This must not invalidate the e-CS conditions
		 * Modifies: priceVector
		 */
		void increasePrice( node N, bigreal delta ) {
		  #ifdef D_ALG
		  assert( allow_price_drop || FP_LEQ(0,delta) );
		  #endif
		  priceVector[N]+=delta;
		  #ifdef D_ECS
		  assert( checkECSNode( N ) );
		  #endif
		};
		
		void incIteration() { iteration++; };
		void printNode( node N ) const;
		friend std::ostream &operator<<( std::ostream &out, const problem &prob );

		/* Calculates the surplus of node N.
		 * The formula for the surplus is:
		 *
		 *  s(N) = \sum_{E\in IN(N)} flow(E) - \sum_{E\in OUT(N)} + supply(N)
		 *
		 * (see eq. 2 p. 2)
		 */
		bigreal calculateNodeSurplus( node N ) const {
		  bigreal surplus=0;
		  edgeList IN = G->inEdges( N ), OUT = G->outEdges( N );

		  // Incoming flow increases surplus 
		  for(edgeList::iterator it = IN.begin(); it != IN.end(); ++it) surplus+=flowVector[*it];
		  
		  // Outgoing flow decreases surplus
		  for(edgeList::iterator it = OUT.begin(); it != OUT.end(); ++it) surplus-=flowVector[*it];
		  
		  // Supply increases/decreases surplus
		  surplus += supplyVector[N];
		  
		  return surplus;
		}
      
		graph* getGraph() { return G; };
		real getEpsilon() const { return epsilon; };
		void allowPriceDrop() { allow_price_drop = true; };
		
		void randomizeActiveNodes( real factor = -1 );
		
		void calculateStats( problemStats& stats, const int histBins = 0 ) const;
		real getNodeFlow(const node N) const;

		void setCumulativeSurplus(long unsigned int arg1) { cumulativeSurplus = arg1; };
		
		bool sanityCheck() const;
};


#endif /* _problemState_H */
