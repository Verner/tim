/***************************************************************
 * costFunction.cpp
 * @Author:      Jonathan Verner (jonathan.verner@matfyz.cz)
 * @License:     GPL v2.0 or later
 * @Created:     2009-05-28.
 * @Last Change: 2009-05-28.
 * @Revision:    0.0
 * Description:
 * Usage:
 * TODO:
 *CHANGES:
 ***************************************************************/
 
#include "costFunction.h"
#include <iostream>

bigreal costFunction::K = 1000000;

using namespace std;

ostream &operator<<(ostream &OUT, const costFunction &c ) {
  OUT << "cost(flow) = " << c.factor << "(flow-" << c.minCostFlow << ")^2 + out_of_bound; ";
  OUT << "bound=["<<c.lBound<<","<<c.uBound<<"], K="<< costFunction::K << endl;
  return OUT;
}
