#ifndef GRAPH_H
#define GRAPH_H

#include <list>
#include <vector>
#include <bitset>
#include <string>
#include <iosfwd>

#include <iostream>

#include "defs.h"
#include "util.h"

class zakazy;
class dataSet;

class graph {
    private:
        std::vector<edgeList> mapIN;
        std::vector<edgeList> mapOUT;
        std::vector<node> from, to;
        std::vector<char> edgefClass;
	std::vector<char> nodeFClassCount;

	void resizeNodes( node maxNode );
	void resizeEdges( edge maxEdge );
	void incNodeFClassCount( node N, int fClass ) { 
  	  assert( 0<fClass && fClass < 6 );
	  nodeFClassCount[N*5+(fClass-1)]++;
	}
	
	real computeProportion( edge E ) const {
	  int total=0;
	  node N = fromNode(E);
	  for(int i=1;i<6;i++)
	    total+=getNodeFClassCount(N,i)*(6-i);
	  return getFClass(E)/total;
	}
	
	real computeProportion( edge E, std::bitset<5> mask ) const {
	  int total=0;
	  node N = fromNode(E);
	  for(int i=1;i<6;i++)
	    if ( mask[i-1] ) total+=getNodeFClassCount(N,i)*(6-i);
	  return getFClass(E)/total;
	}

	/* Holds the fClass of the inEdge connected
	 * to a given link (after applying the 
	 * disableUTurns algorithm) */
	std::vector<int> prevFClass;

        edge edgeSize;
        node nodeSize;

        void addEdge( node From, node To, edge E, int FClass, int prevFClass = -1 );
	
	/* Enforces the zakazy given by z into
	 * by modifying the topology of the graph.
	 * New nodes and edges will be added.
	 * NOTE: node id's WILL change !!!
	 * NOTE: old edge id's WILL REMAIN UNCHANGED */
	void enforceZakazy( zakazy &z );

    public:
        graph();
	void loadZakazy( const std::string& fileName, dataSet* dS );
	
	void load( const dataSet &dSet );
	void save( dataSet &dSet ) const;
        
        bool testDeadEnds( dataSet& dSet ) const;

	/* Returns the number of outgoing edges of class fClass */
	int getNodeFClassCount(node N, int fClass) const {
	  assert( 0<fClass && fClass < 6 );
	  return nodeFClassCount[N*5+(fClass-1)];
	}

	/* Based on direction returns either the number of outgoing edes
	 * (in case direction == OUTGOING ) or the number of incoming edges
	 * of function class fClass. In the second alternative, it 
	 * consults the prevFClass of a link, since that is the one
	 * relevant for links with derived fClass (the ones added by
	 * the disableUTurns algorithm). */
	int getNodeFClassCount(node N, int fClass, dirT direction) const { 
	  if ( direction == OUTGOING ) return getNodeFClassCount( N, fClass );
	  edgeList IN = inEdges( N );
	  int ret = 0;
	  for( edgeList::iterator E = IN.begin(); E != IN.end(); ++E )
	    if ( getFClass( *E, direction ) == fClass ) ret++;
	  return ret;
	}



	/* Returns the class of the edge ( 1--5, 1 highways, 5 local roads ) */
	int getFClass( edge E ) const { return edgefClass[E]; };

	/* Used when the graph was modified using the disable U-turns
	 * algorithm.
	 *
	 * If Edge E goes from the outNode of an edge F_0 to the inNode
	 * of edge F_1, then getFClass( E, BACKWARD ) == getFClass( F_0 )
	 * and getFClass( E, FORWARD ) == getFClass( F_1 ) */
	int getFClass( edge E, dirT direction ) const { 
	  if ( direction == FORWARD ) return edgefClass[E];
	  else return prevFClass[E];
	}
	
        /* Returns the proportion of flow that should
	 * go along edge E. Guaranteed to be non-negative 
	 * NOTE: Not used anywhere, will probably delete */
	real flowPortion( edge E, real flow ) const { 
	  real ret = computeProportion(E)*flow; 
	  assert(ret >=0);
	  return ret;
	};
	
        /* Returns the node from which the edge @E is leaving */
	node fromNode( edge E ) const { return from[E]; };
	
	/* Returns the node into which the edge @E enters */
        node toNode( edge E ) const { return to[E]; };
	
        /* Returns the list of edges entering node @N */
	const edgeList &inEdges( node N ) const { return mapIN[(integer32)N]; };
        
	/* Returns the list of edges leaving node @N */
	const edgeList &outEdges( node N ) const  { return mapOUT[(integer32)N]; };
	
	/* If @direction is FORWARD or OUTGOING returns 
	 * the list of outgoing edges leaving node @N otherwise
	 * returns the list of edges entering node @N */
	const edgeList &nbrEdges( node N, dirT direction ) const { 
	  if ( direction == OUTGOING ) return outEdges(N);
	  else return inEdges(N); 
	}

	/* Returns the list of edges of class @fClass leaving node @N */
	edgeList outEdges( node N, int fClass ) const;
	
	/* Returns the list of edges of class @fclass entering node @N */
	edgeList inEdges( node N, int fClass ) const;
	
	/* If @direction is FORWARD or OUTGOING returns 
	 * the list of edges of class @fclass leaving node @N otherwise
	 * returns the list of edges of class @fclass entering node @N*/
	edgeList nbrEdges( node N, int fClass, dirT direction ) const {
	  if ( direction == OUTGOING ) return outEdges( N, fClass );
	  else return inEdges( N, fClass );
	}
	/* Returns the list of outgoing/incoming edges which have
	 * a less important function class than @fClass 
	 * (i.e. of function class > @fClass ) */
	edgeList nbrEdgesGE( node N, int fClass, dirT direction ) const;

	/* Returns the most important class going through node @N 
	 * in the direction @direction */
	int getMinFClass( node N, dirT direction ) const {
	  int ret = 5;
	  edgeList edges = nbrEdges( N, direction );
	  for( edgeList::iterator E = edges.begin(); E != edges.end(); ++E ) { 
	    ret = MIN<int>( getFClass(*E), ret );
	  }
	  return ret;
	}
	
	/* A heuristic to test whether E is a one-way road */
	bool isSingle( edge E, dirT direction ) const;
	
        edge numOfEdges() const { return edgeSize; };
        node numOfNodes() const { return nodeSize; };
	
	void printGraph();

	void printEdges(node N, bool IN = true );
	
	friend std::ostream &operator<<( std::ostream &OUT, const graph &G );
};












#endif // GRAPH_H
