@echo off
set /p schemaName= Schema:
set /p tableName= Load results into table:

w64-build\tim.exe summary -d data\%schemaName%-results -o %schemaName%-summary_table

rem type sql\db_load_dump.sql | psql -U postgres mop

sql\summary_sql.bat %schemaName% %tableName% | psql -U postgres mop

del %schemaName%-summary_table
del %schemaName%-results