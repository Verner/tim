#include "test.h"
#include "dataSet.h"

#include "util.h"

#include <string>
#include <sstream>

using namespace std;

bool testIORow() {
  bool ret = true;
  string input("3273339 847991246 2 2 2 574548 1465717 -1 -1 -11674 -1 -1 -1 -1 -1\n"),
	 exOut("3273339 847991246 2 2 2 574548 1465717 -1 -1 -1 -1 -1 -1 -1 -1"),
         output("");
  stringstream IN(input), OUT(output);
  dataRow row;
  IN >> row;
  
  TESTCOND( row.id  == 3273339, "testINRow(): Failed to read id." )
  TESTCOND( row.cat == 847991246, "testINRow(): Failed to read cat." )
  TESTCOND( row.fClass == 2, "testINRow(): Failed to read fClass." )
  TESTCOND( ! row.isMeasured(), "testINRow(): Failed to read measured" )
  if ( row.measured < -1 ) { 
    cerr << "testINRow(): Warning, preserving invalid measured value" << endl;
  }
  
  OUT << row;
  TESTCOND( (input == OUT.str() || input == exOut), "testINRow(): IN ("+input+") != OUT ("+OUT.str()+")" )
  
  return ret;
}

bool testIODataSet() {
  bool ret = true;
  string input("3273339 847991246 2 2 2 574548 1465717 -1 -1 -11674 -1 -1 -1 -1 -1"), output("");
  stringstream IN(input), OUT(output);
  dataSet dSet;
  dataRow row;
  IN >> dSet;
  IN >> row;
  
  TESTCOND( dSet.numOfEdges() == 3273339+1, "testIODataSet(): Wrong number of edges." + number2Str<edge>(dSet.numOfEdges()) + " != 3273340 (expected)" )
  TESTCOND( dSet.numOfNodes() == 1465717, "testIODataSet(): Wrong number of nodes." + number2Str<node>(dSet.numOfNodes()) + " != 1465717 (expected)" )
  //TESTCOND( ( dSet[3273339] != row ), "testIODataSet(): Failed to read row correctly." )
  
  return ret;
}

int main(int, char **) { 
  bool ret = true;
  
  TEST(testIORow)
  TEST(testIODataSet)
  
  if ( ! ret ) return 1;
  return 0;
}