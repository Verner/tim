#ifndef _zakazy_H
#define _zakazy_H

#include "defs.h"

#include <map>
#include <set>
#include <string>

class dataSet;

typedef std::set<edge> eset;

class zakazy {
  private:
    std::map<edge,eset> rules;
    dataSet *dS;
  public:
    void load( const std::string &fname, dataSet *dS );
    bool isAllowed( edge From, edge To );
};

#endif // _zakazy_H
