#ifndef _CONFIG_H_
#define _CONFIG_H_

#include "defs.h"

#include <string>
#include <map>
#include <iostream>

#define DATA_FILE_OPTION                "data"
#define OUTPUT_FILE_OPTION              "output"
#define ZAKAZY_FILE_OPTION              "zakazy"

#define CONTROL_FILE_OPTION             "control_file"
#define DUMP_INTERVAL_OPTION            "dump_interval"
#define DUMP_FILE_PREFIX_OPTION         "dump_file_prefix"


#define INIT_FLOW_DEPTH_OPTION          "init_flow_depth"
#define RELIABILITY_TRESHOLD_OPTION     "reliability_treshold"
#define CROSS_FCLASS_NETWORK_OPTION     "cross_fclass_traffic"
#define FC1_DECAY_OPTION                "fc1_decay"
#define FC2_DECAY_OPTION                "fc2_decay"
#define FC3_DECAY_OPTION                "fc3_decay"
#define FC4_DECAY_OPTION                "fc4_decay"
#define FC5_DECAY_OPTION                "fc5_decay"


#define AVOID_ZERO_OPTION               "avoid_zero_factor"
#define EPSILON_OPTION                  "epsilon"
#define ALLOW_DOWN_OPTION               "down"
#define COST_FACTOR_FC1_OPTION          "cost_fc1"
#define COST_FACTOR_FC2_OPTION          "cost_fc2"
#define COST_FACTOR_FC3_OPTION          "cost_fc3"
#define COST_FACTOR_FC4_OPTION          "cost_fc4"
#define COST_FACTOR_FC5_OPTION          "cost_fc5"




class config {
    std::map<std::string, std::string> keys;
    bool parseLine( const std::string line, std::string &key, std::string &val );
    
    void loadDefaults();
  
  public:
    config();
    void load( std::string fname );
    bool addConfigLine( std::string ln );
    
    bool haveKey( std::string key );
    
    real getNum( std::string key ) { 
      return getNum( key, 0 );
    };
    real getNum( std::string key, real default_val );
    
    std::string getStr( std::string key ) { 
      return getStr( key, "" );
    };
    std::string getStr( std::string key, std::string default_val );
    
    bool getBool( std::string key ) { 
      return getBool( key, false );
    };
    bool getBool( std::string key, bool default_val );
  
    friend std::ostream& operator<<( std::ostream& OUT, const config& cfg );
};

#endif // _CONFIG_H_